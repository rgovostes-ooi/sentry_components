/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_BLUEVIEW_H
#define SENTRY_BLUEVIEW_H

#include "ds_base/ds_process.h"
#include "ds_control_msgs/ExternalBottomFollowAlarm.h"
#include "ds_sensor_msgs/ForwardLookingStatus.h"
#include "std_msgs/String.h"

namespace sentry_blueview
{
class SentryBlueview : public ds_base::DsProcess
{
public:
  SentryBlueview(int argc, char* argv[], const std::string& name);
  ~SentryBlueview() override;

  void onBlueviewStatusData(ds_core_msgs::RawData bytes);

  void onBlueviewData(ds_core_msgs::RawData bytes);

  void onJtRosMsg(const std_msgs::String::ConstPtr msg);

protected:
  void setupConnections() override;
  void setupPublishers() override;
  void setupSubscriptions() override;

private:
  // UDP connection to Blueview driver
  boost::shared_ptr<ds_asio::DsConnection> blueview_conn_;

  // UDP connection to Blueview driver for status
  boost::shared_ptr<ds_asio::DsConnection> blueview_status_conn_;

  ros::Publisher alarm_pub_;
  ros::Publisher status_pub_;

  ros::Subscriber jtros_sub_;
};
}

#endif
