/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_abort_supervisor/sentry_abort_supervisor.h"
#include "sentry_abort_supervisor_private.h"

namespace sentry_abort_supervisor
{
SentryAbortSupervisor::SentryAbortSupervisor()
  : AbortSupervisor(), d_ptr_(std::unique_ptr<SentryAbortSupervisorPrivate>(new SentryAbortSupervisorPrivate))
{
}
SentryAbortSupervisor::SentryAbortSupervisor(int argc, char** argv, const std::string& name)
  : AbortSupervisor(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryAbortSupervisorPrivate>(new SentryAbortSupervisorPrivate))
{
}

SentryAbortSupervisor::~SentryAbortSupervisor() = default;

void SentryAbortSupervisor::executeAbort(void)
{
  DS_D(SentryAbortSupervisor);

  sentry_msgs::PWRCmd pwr_cmd;
  // Power off strobe
  pwr_cmd.request.address = d->pwr_add["strobe"];
  pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_OFF;
  serviceRetry(d->pwr_cmd_srv_, pwr_cmd, 10, "Strobe powered off", "Strobe power off failed");

  // Power off lasers
  pwr_cmd.request.address = d->pwr_add["lasers"];
  pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_OFF;
  serviceRetry(d->pwr_cmd_srv_, pwr_cmd, 10, "Lasers powered off", "Lasers power off failed");

  // Power off camera
  pwr_cmd.request.address = d->pwr_add["camera"];
  pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_OFF;
  serviceRetry(d->pwr_cmd_srv_, pwr_cmd, 10, "Camera powered off", "Camera power off failed");

  // Power off edgetech
  pwr_cmd.request.address = d->pwr_add["edgetech"];
  pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_OFF;
  serviceRetry(d->pwr_cmd_srv_, pwr_cmd, 10, "Edgetech powered off", "Edgetech power off failed");

  sentry_msgs::XRCmd xr_cmd;
  // Release descent weight, both ascent weights, and store depth
  xr_cmd.request.command = xr_cmd.request.XR_CMD_DCAM_OPEN;
  serviceRetry(d->xr1_cmd_srv_, xr_cmd, 10, "XR1 SEND DCAM OPEN SUCCESS", "XR1 SEND DCAM OPEN FAILURE");
  serviceRetry(d->xr2_cmd_srv_, xr_cmd, 10, "XR2 SEND DCAM OPEN SUCCESS", "XR2 SEND DCAM OPEN FAILURE");

  d->abort_time_ = ros::Time::now();
  d->abort_depth_ = d->state_.down.value;

  // Fire a oneshot timer after one minute
  d->burnwire_oneshot_tmr_.start();
}

void SentryAbortSupervisor::burnwire(const ros::TimerEvent& event)
{
  DS_D(SentryAbortSupervisor);

  // If the depth did not change much, burn the burnwires
  if ((d->abort_depth_ - d->state_.down.value) < d->ascent_depth_change_)
  {
    sentry_msgs::XRCmd xr_cmd;
    xr_cmd.request.command = xr_cmd.request.XR_CMD_BURNWIRE_ON;
    serviceRetry(d->xr1_cmd_srv_, xr_cmd, 10, "XR1 SEND BURN SUCCESS", "XR1 SEND BURN FAILURE");
    serviceRetry(d->xr2_cmd_srv_, xr_cmd, 10, "XR2 SEND BURN SUCCESS", "XR2 SEND BURN FAILURE");
  }

  // Fire a 10s timer to continuosly open dcams during ascent
  d->ascent_dcams_tmr_.start();
}

void SentryAbortSupervisor::continuouslyOpenDcams(const ros::TimerEvent& event)
{
  DS_D(SentryAbortSupervisor);

  // Try to open dcams every 10 seconds during ascent, just in case
  ros::Time now = ros::Time::now();
  if ((d->state_.down.value < d->joystick_enable_depth_) || ((now - d->abort_time_) > ros::Duration(3600.0)))
  {
    sentry_msgs::XRCmd xr_cmd;
    xr_cmd.request.command = xr_cmd.request.XR_CMD_DCAM_OPEN;
    serviceRetry(d->xr1_cmd_srv_, xr_cmd, 10, "XR1 SEND DCAM OPEN SUCCESS", "XR1 SEND DCAM OPEN FAILURE");
    serviceRetry(d->xr2_cmd_srv_, xr_cmd, 10, "XR2 SEND DCAM OPEN SUCCESS", "XR2 SEND DCAM OPEN FAILURE");
  }
  else
  {
    // END of the descent process: stop the timers
    d->ascent_dcams_tmr_.stop();
    d->burnwire_oneshot_tmr_.stop();
  }
}

template <typename T>
bool SentryAbortSupervisor::serviceRetry(ros::ServiceClient srvClient, T srv, int n_retries, std::string success,
                                         std::string failure)
{
  for (int i = 0; i < n_retries; ++i)
  {
    if (srvClient.waitForExistence(ros::Duration(1.0)))
    {
      if (srvClient.call(srv))
      {
        ROS_INFO_STREAM(success);
        return true;
      }
      else
      {
        ROS_ERROR_STREAM(failure);
      }
    }
    else
    {
      ROS_ERROR_STREAM(failure);
    }
  }

  // Also stop the abort publish timer so xrs will abort if the service call fails more than n times
  stopAbortRefreshTimer();
  return false;
}

void SentryAbortSupervisor::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr msg)
{
  DS_D(SentryAbortSupervisor);

  d->state_ = *msg;
}

void SentryAbortSupervisor::setupParameters()
{
  DS_D(SentryAbortSupervisor);

  abort_supervisor::AbortSupervisor::setupParameters();

  d->ascent_depth_change_ = ros::param::param<double>("~ascent_depth_change", 20.0);

  d->joystick_enable_depth_ = ros::param::param<double>("~joystick_enable_depth", 10.0);

  // The name of the stuff to power on/off should come from param server
  d->pwr_add["edgetech"] = grd_util::get_address("edgetech");
  d->pwr_add["strobe"] = grd_util::get_address("strobe");
  d->pwr_add["lasers"] = grd_util::get_address("lasers");
  d->pwr_add["camera"] = grd_util::get_address("camera");
}

void SentryAbortSupervisor::setupServices()
{
  abort_supervisor::AbortSupervisor::setupServices();

  DS_D(SentryAbortSupervisor);

  auto nh = nodeHandle();
  std::string xr1_service = ros::param::param<std::string>("~xr1_service", "/sentry/sail/grd/xr1/cmd");
  d->xr1_cmd_srv_ = nh.serviceClient<sentry_msgs::XRCmd>(xr1_service);
  std::string xr2_service = ros::param::param<std::string>("~xr2_service", "/sentry/sail/grd/xr2/cmd");
  d->xr2_cmd_srv_ = nh.serviceClient<sentry_msgs::XRCmd>(xr2_service);
  std::string pwr_service = ros::param::param<std::string>("~pwr_service", "/sentry/sail/grd/pwr/cmd");
  d->pwr_cmd_srv_ = nh.serviceClient<sentry_msgs::PWRCmd>(pwr_service);
}

void SentryAbortSupervisor::setupTimers()
{
  abort_supervisor::AbortSupervisor::setupTimers();

  DS_D(SentryAbortSupervisor);

  // Create timers but don't autostart them
  auto nh = nodeHandle();
  // Oneshot timer
  d->burnwire_oneshot_tmr_ =
      nh.createTimer(ros::Duration(60.0), boost::bind(&SentryAbortSupervisor::burnwire, this, _1), true, false);
  // Fire a 10s timer to continuosly open dcams during ascent
  d->ascent_dcams_tmr_ = nh.createTimer(
      ros::Duration(10.0), boost::bind(&SentryAbortSupervisor::continuouslyOpenDcams, this, _1), false, false);
}

void SentryAbortSupervisor::setupSubscriptions()
{
  abort_supervisor::AbortSupervisor::setupSubscriptions();

  DS_D(SentryAbortSupervisor);

  std::string agg_ns = ros::param::param<std::string>("~agg_topic", "0");
  d->agg_sub_ = nodeHandle().subscribe<ds_nav_msgs::AggregatedState>(
      agg_ns, 1, boost::bind(&SentryAbortSupervisor::onAggregatedStateMsg, this, _1));
}
}
