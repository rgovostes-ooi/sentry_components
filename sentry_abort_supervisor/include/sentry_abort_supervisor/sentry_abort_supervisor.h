/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ABORT_SUPERVISOR_H
#define SENTRY_ABORT_SUPERVISOR_H

#include <abort_supervisor/abort_supervisor.h>
// Services
#include "sentry_msgs/PWRCmd.h"
#include "sentry_msgs/XRCmd.h"
#include "ds_nav_msgs/AggregatedState.h"

#include "sail_grd/pwr_lookup.h"

namespace sentry_abort_supervisor
{
struct SentryAbortSupervisorPrivate;
class SentryAbortSupervisor : public abort_supervisor::AbortSupervisor
{
  DS_DECLARE_PRIVATE(SentryAbortSupervisor)

public:
  SentryAbortSupervisor();
  SentryAbortSupervisor(int argc, char* argv[], const std::string& name);
  ~SentryAbortSupervisor() override;
  DS_DISABLE_COPY(SentryAbortSupervisor)

  void burnwire(const ros::TimerEvent& event);

  void continuouslyOpenDcams(const ros::TimerEvent& event);

  template <typename T>
  bool serviceRetry(ros::ServiceClient srvClient, T srv, int n_retries, std::string success, std::string failure);

  void onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr msg);

protected:
  virtual void executeAbort() override;
  void setupServices() override;
  void setupTimers() override;
  void setupParameters() override;
  void setupSubscriptions() override;

private:
  std::unique_ptr<SentryAbortSupervisorPrivate> d_ptr_;
};
}
#endif  // SENTRY_ABORT_SUPERVISOR_H
