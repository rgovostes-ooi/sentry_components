/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _SENTRYSITTER_UDP_BRIDGE_H
#define _SENTRYSITTER_UDP_BRIDGE_H

#include "ds_base/ds_process.h"

namespace ds_hotel_msgs
{
ROS_DECLARE_MESSAGE(HTP)
ROS_DECLARE_MESSAGE(PWR)
ROS_DECLARE_MESSAGE(BatMan)
}

namespace ds_nav_msgs
{
ROS_DECLARE_MESSAGE(AggregatedState)
}

namespace ds_control_msgs
{
ROS_DECLARE_MESSAGE(BottomFollow1D)
ROS_DECLARE_MESSAGE(GoalLegState)
}

namespace ds_core_msgs
{
ROS_DECLARE_MESSAGE(Abort)
}

namespace sentrysitter_udp_bridge
{
struct SentrysitterUdpBridgePrivate;

class SentrysitterUdpBridge : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(SentrysitterUdpBridge)

public:
  SentrysitterUdpBridge();
  SentrysitterUdpBridge(int argc, char* argv[], const std::string& name);
  ~SentrysitterUdpBridge() override;
  DS_DISABLE_COPY(SentrysitterUdpBridge)

protected:
  void setupConnections() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void sentrysitterGuiCallback(const ds_core_msgs::RawData& msg);
  void sentrysitterSdyneCallback(const ds_core_msgs::RawData& msg);

  void navStateCallback(const ds_nav_msgs::AggregatedStateConstPtr& msg);
  void mainHtpCallback(const ds_hotel_msgs::HTPConstPtr& msg);
  void batteryHtpCallback(const ds_hotel_msgs::HTPConstPtr& msg);
  void pwrCallback(const ds_hotel_msgs::PWRConstPtr& msg);
  void bottomFollowerCallback(const ds_control_msgs::BottomFollow1DConstPtr& msg);
  void batteryCallback(const ds_hotel_msgs::BatManConstPtr& msg);
  void legCallback(const ds_control_msgs::GoalLegStateConstPtr& msg);
  void abortCallback(const ds_core_msgs::AbortConstPtr& msg);

private:
  std::unique_ptr<SentrysitterUdpBridgePrivate> d_ptr_;
};
}
#endif  //_SENTRYSITTER_UDP_BRIDGE_H
