/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentrysitter_udp_bridge/sentrysitter_udp_bridge.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_hotel_msgs/HTP.h"
#include "ds_hotel_msgs/PWR.h"
#include "ds_hotel_msgs/BatMan.h"
#include "ds_control_msgs/BottomFollow1D.h"
#include "ds_control_msgs/GoalLegState.h"
#include "ds_core_msgs/Abort.h"
#include "sentry_acomms/SentryVehicleState.h"
#include "sentry_acomms/serialization.h"

#include "std_msgs/String.h"

#include <boost/date_time.hpp>

namespace sentrysitter_udp_bridge
{
struct SentrysitterUdpBridgePrivate
{
  SentrysitterUdpBridgePrivate() : battery_total_percent_(0), battery_total_charge_(0), alt_(0)
  {
  }
  boost::shared_ptr<ds_asio::DsConnection> sentrysitter_gui_;
  boost::shared_ptr<ds_asio::DsConnection> sentrysitter_sdyne_;
  ros::Subscriber nav_sub_;
  ros::Subscriber main_htp_;
  ros::Subscriber bat_htp_;
  ros::Subscriber pwr_;
  ros::Subscriber bottom_follower_;
  ros::Subscriber leg_;
  ros::Subscriber battery_sub_;
  ros::Subscriber abort_;
  ros::Publisher mc_shim_pub_;
  std::vector<float> battery_voltages_;
  double battery_total_charge_;
  double battery_total_percent_;

  ds_nav_msgs::AggregatedState last_nav_;
  sentry_acomms::SentryVehicleState sdq0_;
  double alt_;

  void sendGvxString()
  {
    auto time = ros::Time::now().toBoost();
    auto os = std::ostringstream{};

    auto facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S");
    os.imbue(std::locale(os.getloc(), facet));

    os << std::fixed << std::setprecision(3);
    os << "GVX " << time << " SENT 0 " << last_nav_.heading.value * 180 / M_PI;

    sentrysitter_gui_->send(os.str());
  }

  void sendSdq0()
  {
    auto ss = std::ostringstream{};
    // First up, position w/ no decimal places
    ss << "SMS:XXXX|";
    ss << std::fixed << std::setprecision(0);
    ss << sdq0_.pos_ned.x << "," << sdq0_.pos_ned.y << "," << sdq0_.pos_ned.z << ",";
    // Next, altitude and heading w/ 1 decimal point.
    ss << std::setprecision(1);
    ss << sdq0_.altitude << "," << sdq0_.heading << ",";
    // Velocities get 2 decimals
    ss << std::setprecision(2);
    ss << sdq0_.horz_velocity << "," << sdq0_.vert_velocity << ",";
    // A single digit for the abort flag
    ss << "A" << static_cast<int>(sdq0_.abort_status) << ",";
    // We don't have the 'cfg' parameter any more, this is only here for legacy purposes
    ss << "0,";
    // Battery percent
    ss << std::setprecision(1) << sdq0_.battery_pct << ",";
    // Goals, no decimal places
    ss << std::setprecision(0);
    ss << sdq0_.goal_ned.x << "," << sdq0_.goal_ned.y << "," << sdq0_.goal_ned.z << ",";
    // More unused fields now: battery_time, tx_count, rx_count, dvl_status
    ss << "0,0,0,0,";
    // Still more unused fields:  flourometer_volts, optode_volts, eh_volts
    ss << "0,0,0,";
    // Finally, trackline number
    ss << sdq0_.trackline;

    sentrysitter_sdyne_->send(ss.str());
  }
  void sendAuvString()
  {
    auto time = ros::Time::now().toBoost();
    auto os = std::ostringstream{};

    auto facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S");
    os.imbue(std::locale(os.getloc(), facet));

    os << std::setprecision(1) << std::fixed;
    os << "AUV " << time << " SEN " << last_nav_.easting.value << " " << last_nav_.northing.value << " " << 0.0 << " "
       << last_nav_.heading.value * 180 / M_PI << " " << last_nav_.pitch.value << " " << last_nav_.roll.value << " "
       << last_nav_.down.value << " " << 0.0 << " " << 0.0 << " " << alt_ << " " << last_nav_.surge_u.value << " "
       << last_nav_.sway_v.value << " " << last_nav_.heave_w.value << " " << 0.0 << " " << 0.0 << " " << 0.0;

    sentrysitter_gui_->send(os.str());
  }

  void sendGasString()
  {
    auto time = ros::Time::now().toBoost();
    auto os = std::ostringstream{};
    auto facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S");
    os.imbue(std::locale(os.getloc(), facet));

    os << std::setprecision(1) << std::fixed;
    os << "GAS " << time << " TOTAL " << battery_total_charge_ << " " << battery_total_percent_;
    for (auto i = 0; i < battery_voltages_.size(); ++i)
    {
      os << " BAT" << std::setw(2) << std::setfill('0') << i + 1 << " 0 0 0 " << battery_voltages_.at(i) << " 0 0 0 0 "
                                                                                                            "0 ";
    }

    sentrysitter_gui_->send(os.str());
  }
};

SentrysitterUdpBridge::SentrysitterUdpBridge()
  : ds_base::DsProcess(), d_ptr_(std::unique_ptr<SentrysitterUdpBridgePrivate>(new SentrysitterUdpBridgePrivate))
{
}

SentrysitterUdpBridge::SentrysitterUdpBridge(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<SentrysitterUdpBridgePrivate>(new SentrysitterUdpBridgePrivate))
{
}

SentrysitterUdpBridge::~SentrysitterUdpBridge() = default;

void SentrysitterUdpBridge::setupConnections()
{
  DsProcess::setupConnections();

  auto str = ros::names::resolve(ros::this_node::getName(), std::string{ "sentrysitter_gui" });
  if (!ros::param::has(str))
  {
    ROS_FATAL_STREAM("No connection parameter named '" << str << "'");
    ROS_BREAK();
  }

  DS_D(SentrysitterUdpBridge);
  d->sentrysitter_gui_ =
      addConnection("sentrysitter_gui", boost::bind(&SentrysitterUdpBridge::sentrysitterGuiCallback, this, _1));

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "sentrysitter_sdyne" });
  if (!ros::param::has(str))
  {
    ROS_FATAL_STREAM("No connection parameter named '" << str << "'");
    ROS_BREAK();
  }

  d->sentrysitter_sdyne_ =
      addConnection("sentrysitter_sdyne", boost::bind(&SentrysitterUdpBridge::sentrysitterSdyneCallback, this, _1));
}

void SentrysitterUdpBridge::setupSubscriptions()
{
  DsProcess::setupSubscriptions();

  auto param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "nav_state_topic" });
  auto topic_str = std::string{};

  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  DS_D(SentrysitterUdpBridge);
  auto nh = nodeHandle();
  d->nav_sub_ = nh.subscribe<ds_nav_msgs::AggregatedState>(
      topic_str, 1, boost::bind(&SentrysitterUdpBridge::navStateCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "main_htp_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  d->main_htp_ =
      nh.subscribe<ds_hotel_msgs::HTP>(topic_str, 1, boost::bind(&SentrysitterUdpBridge::mainHtpCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "battery_htp_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  d->bat_htp_ =
      nh.subscribe<ds_hotel_msgs::HTP>(topic_str, 1, boost::bind(&SentrysitterUdpBridge::batteryHtpCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "pwr_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  d->pwr_ = nh.subscribe<ds_hotel_msgs::PWR>(topic_str, 1, boost::bind(&SentrysitterUdpBridge::pwrCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "bottom_follower_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  d->bottom_follower_ = nh.subscribe<ds_control_msgs::BottomFollow1D>(
      topic_str, 1, boost::bind(&SentrysitterUdpBridge::bottomFollowerCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "batman_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }
  d->battery_sub_ =
      nh.subscribe<ds_hotel_msgs::BatMan>(topic_str, 1, boost::bind(&SentrysitterUdpBridge::batteryCallback, this, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "leg_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }

  d->leg_ = nh.subscribe(topic_str, 1, &SentrysitterUdpBridge::legCallback, this);

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "abort_topic" });
  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }

  d->abort_ = nh.subscribe(topic_str, 1, &SentrysitterUdpBridge::abortCallback, this);
}

void SentrysitterUdpBridge::legCallback(const ds_control_msgs::GoalLegStateConstPtr& msg)
{
  DS_D(SentrysitterUdpBridge);
  d->sdq0_.goal_ned.x = static_cast<float>(msg->line_end.x);
  d->sdq0_.goal_ned.y = static_cast<float>(msg->line_end.y);
  d->sdq0_.trackline = static_cast<uint16_t>(msg->leg_number);
  d->sendSdq0();
}

void SentrysitterUdpBridge::sentrysitterSdyneCallback(const ds_core_msgs::RawData& msg)
{
}

void SentrysitterUdpBridge::sentrysitterGuiCallback(const ds_core_msgs::RawData& msg)
{
  auto incoming = std::string{ std::begin(msg.data), std::end(msg.data) };
  if (incoming.empty())
  {
    return;
  }

  const auto start_index = incoming.find("PWR!I");
  if (start_index == incoming.npos)
  {
    ROS_ERROR_STREAM("Could not find PWR!I in " << incoming);
    return;
  }

  uint8_t command = 0;
  uint8_t pwr_switch = 0;

  ROS_ERROR_STREAM("String is: " << incoming.substr(start_index));
  const auto num_parsed = std::sscanf(incoming.substr(start_index).data(), "PWR!I%c%02hhXX", &command, &pwr_switch);

  if (num_parsed < 2)
  {
    ROS_ERROR_STREAM("Invalid pwr switch command, parsed " << num_parsed << " args, expected 2");
    return;
  }

  auto os = std::ostringstream{};
  os << std::setw(2) << std::setfill('0') << std::hex;
  if (command == 'C')
  {
    os << "SPOFF " << static_cast<int>(pwr_switch);
  }
  else if (command == 'S')
  {
    os << "SPON " << static_cast<int>(pwr_switch);
  }

  else
  {
    ROS_ERROR_STREAM("Invalid PWR switch command: " << command);
    return;
  }

  auto jt_msg = std_msgs::String{};
  jt_msg.data = os.str();

  DS_D(SentrysitterUdpBridge);
  d->mc_shim_pub_.publish(jt_msg);
}

void SentrysitterUdpBridge::bottomFollowerCallback(const ds_control_msgs::BottomFollow1DConstPtr& msg)
{
  DS_D(SentrysitterUdpBridge);
  d->alt_ = msg->median_altitude;
  d->sdq0_.goal_ned.z = static_cast<float>(msg->depth_goal);
  d->sdq0_.altitude = static_cast<float>(msg->median_altitude);
  d->sendSdq0();
}

void SentrysitterUdpBridge::navStateCallback(const ds_nav_msgs::AggregatedStateConstPtr& msg)
{
  DS_D(SentrysitterUdpBridge);
  d->last_nav_ = *msg;

  d->sendGvxString();
  d->sendAuvString();

#define FILL(FIELD) msg->FIELD.valid ? static_cast<float>(msg->FIELD.value) : sentry_acomms::SentryVehicleState::INVALID
  d->sdq0_.heading = FILL(heading);
  d->sdq0_.heading *= 180 / M_PI;
  d->sdq0_.pos_ned.x = FILL(easting);
  d->sdq0_.pos_ned.y = FILL(northing);
  d->sdq0_.pos_ned.z = FILL(down);
  d->sdq0_.vert_velocity = FILL(heave_w);
#undef FILL

  if (msg->surge_u.valid && msg->sway_v.valid)
  {
    d->sdq0_.horz_velocity =
        static_cast<float>(std::sqrt(std::pow(msg->surge_u.value, 2) + std::pow(msg->sway_v.value, 2)));
  }
  else
  {
    d->sdq0_.horz_velocity = sentry_acomms::SentryVehicleState::INVALID;
  }

  d->sendSdq0();
}

void SentrysitterUdpBridge::mainHtpCallback(const ds_hotel_msgs::HTPConstPtr& msg)
{
  auto os = std::ostringstream{};
  os << "HTP3 " << std::setprecision(1) << std::fixed << msg->humidity << " " << msg->temperature << " "
     << msg->pressure;
  DS_D(SentrysitterUdpBridge);
  d->sentrysitter_gui_->send(os.str());
}

void SentrysitterUdpBridge::batteryHtpCallback(const ds_hotel_msgs::HTPConstPtr& msg)
{
  auto os = std::ostringstream{};
  os << "HTP2 " << std::setprecision(1) << std::fixed << msg->humidity << " " << msg->temperature << " "
     << msg->pressure;
  DS_D(SentrysitterUdpBridge);
  d->sentrysitter_gui_->send(os.str());
}

void SentrysitterUdpBridge::pwrCallback(const ds_hotel_msgs::PWRConstPtr& msg)
{
  auto os = std::ostringstream{};
  os << std::noboolalpha;
  os << "PWRST";
  const auto end = msg->pwr_cmd.cend();
  for (auto it = msg->pwr_cmd.cbegin(); it != end; ++it)
  {
    for (auto i = 7; i >= 0; --i)
    {
      os << " " << static_cast<int>((*it >> i) & 1);
    }
  }
  DS_D(SentrysitterUdpBridge);
  d->sentrysitter_gui_->send("SWITCH ETH");
  d->sentrysitter_gui_->send(os.str());
  d->sentrysitter_gui_->send("SOURCE 0");
}

void SentrysitterUdpBridge::batteryCallback(const ds_hotel_msgs::BatManConstPtr& msg)
{
  DS_D(SentrysitterUdpBridge);
  d->battery_voltages_ = msg->moduleVolt;
  d->battery_total_charge_ = msg->chargeCoulombs;
  d->battery_total_percent_ = msg->percentFull;
  d->sendGasString();
  d->sdq0_.battery_pct = static_cast<float>(msg->percentFull);
  d->sendSdq0();
}

void SentrysitterUdpBridge::abortCallback(const ds_core_msgs::AbortConstPtr& msg)
{
  DS_D(SentrysitterUdpBridge);
  d->sdq0_.abort_status = msg->abort;
}
void SentrysitterUdpBridge::setupPublishers()
{
  DsProcess::setupPublishers();

  auto param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "mc_jtros_topic" });
  auto topic_str = std::string{};

  if (!ros::param::get(param_str, topic_str))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_str << "'");
    ROS_BREAK();
  }

  DS_D(SentrysitterUdpBridge);
  auto nh = nodeHandle();
  d->mc_shim_pub_ = nh.advertise<std_msgs::String>(topic_str, 1, false);
}
}
