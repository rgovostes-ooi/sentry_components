Author: J Vaccaro
Updated: 2/21/18

XR : DsBusDevice

** include/sail_grd/xr.h
** src/sail_grd/xr.cpp

Using the GRD sail bus, XR drivers communicate with the XR at a specific address.
They regularly query and publish the latest status information on the topic with their node's name.

Launch requires
    - ds_bus

Launch prefers
    - sentry_abort_supervisor

Service servers
    - XRCmd.srv : XR action command
      ~nodename/cmd
      DCAM_OPEN opens both dcams
      DCAM_CLOSE closes both dcams
      BURNWIRE_ON sets both burnwires high
      BURNWIRE_OFF sets both burnwires low

    - DeadhourCmd.srv : set XR long deadman timer to a value 0-255 hours
      ~nodename/deadhour_cmd

Subscriptions
    - Abort.msg :
      ~/abort
      abort = true starts the XR abort process
      abort = false resets the short deadman timer to 255 seconds

Publishers
    - XR.msg
      ~nodename


