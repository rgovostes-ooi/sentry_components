/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/8/18.
//

#ifndef PROJECT_XR_H
#define PROJECT_XR_H

#include "ds_base/ds_bus_device.h"
#include "ds_core_msgs/IoSMcommand.h"
#include "ds_core_msgs/Abort.h"
#include "ds_core_msgs/CriticalProcess.h"
#include "ds_hotel_msgs/XR.h"
#include "sentry_msgs/XRCmd.h"
#include "sentry_msgs/XRCode.h"
#include "sentry_msgs/DeadhourCmd.h"
#include <string>
#include <memory>
#include <ds_base/util.h>

namespace sail_grd
{
class XR : public ds_base::DsBusDevice
{
public:
  explicit XR();
  XR(int argc, char* argv[], const std::string& name);
  ~XR() override;
  DS_DISABLE_COPY(XR)

private:
  /// \brief ros communication channels
  ros::Publisher xr_pub_;
  ros::Subscriber abort_sub_;
  ros::ServiceServer xr_cmd_;
  ros::ServiceServer xr_code_;
  ros::ServiceServer deadhour_cmd_;

  /// \brief msg published on xr_pub_
  ds_hotel_msgs::XR handled;

  /// \brief unique address of XR from parameters
  std::string address_;
  std::string cmdstr_;
  std::string long_cmdstr_;

  /// \brief internal regexs calculated from address_
  boost::regex match_regex;
  boost::regex parse_regex;
  boost::regex deadhour_regex, code_regex, release_regex;

  /// \brief statuses for determining whether to publish/send commands
  bool status_release_received;
  bool xrAbortDetected;

  /// \brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

private:
  /// \brief updates regex values using current address_
  void _update_regex();

  /// \brief abort_sub_ callback which determines whether to abort
  void _abort_reset(ds_core_msgs::Abort msg);

  /// \brief xr_cmd_ service callback which holds/releases dcams and burn/snuffs burnwires
  bool _xr_cmd(const sentry_msgs::XRCmd::Request& req, const sentry_msgs::XRCmd::Response& resp);

  /// \brief xr_code_ service callback that sets a code for this XR
  bool _xr_code(const sentry_msgs::XRCode::Request& req, const sentry_msgs::XRCode::Response& resp);

  /// \brief deadhour_cmd_ service callback which sets the long deadman timer
  bool _deadhr_cmd(const sentry_msgs::DeadhourCmd::Request& req, const sentry_msgs::DeadhourCmd::Response& resp);

  /// \brief sends a command for the sail_bus node to pass along next
  void _send_preempt(const std::string& cmdstr);

protected:
  /// \brief overwritten functions from DsBusDevice
  void setupParameters() override;
  void setupIoSM() override;
  void setupConnections() override;
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  void onCriticalProcessTimer(const ros::TimerEvent& event) override;

public:
  /// \brief unit-tested functions for accepting the latest status information
  bool acceptsMessage(const std::string& msg);
  bool handleMessage(const std::string& msg);

  const ds_hotel_msgs::XR& getHandled() const;
};

}  // NAMESPACE

#endif  // PROJECT_XR_H
