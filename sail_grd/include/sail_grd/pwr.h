/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/8/18.
//

#ifndef PROJECT_PWR_H
#define PROJECT_PWR_H

#include "ds_base/ds_bus_device.h"
#include "ds_core_msgs/IoSMcommand.h"
#include "ds_hotel_msgs/PWR.h"
#include "sentry_msgs/PWRCmd.h"
#include "sentry_msgs/PWRstringCmd.h"
#include "ds_core_msgs/VoidCmd.h"
#include <string>
#include <memory>
#include <ds_base/util.h>
#include <ds_util/ds_util.h>

namespace sail_grd
{
/// \brief structure for tracking pwr device state
struct pwr_device
{
  std::string name;
  int desired_cmd;
  int latest_cmd;
};

class PWR : public ds_base::DsBusDevice
{
public:
  explicit PWR();
  PWR(int argc, char* argv[], const std::string& name);
  ~PWR() override;
  DS_DISABLE_COPY(PWR)

private:
  bool state_maintain_enabled;

  /// \brief ros channels for publishing information and accepting commands
  ros::Publisher pwr_pub_;
  ros::ServiceServer pwr_cmd_;
  ros::ServiceServer pwr_string_cmd_;
  ros::ServiceServer pwr_mission_powerup_cmd_;
  ros::ServiceServer pwr_mission_powerdown_cmd_;

  /// \brief latest information from PWR published on pwr_pub_
  ds_hotel_msgs::PWR handled;

  /// \brief internal state updated from pwr_cmd_
  std::map<int, pwr_device> devices_;

  /// \brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  /// \brief sail_address of PWR from parameters
  std::string address_;

  /// \brief internal regexs calculated using address_
  boost::regex match_regex;
  boost::regex parse_regex;
  boost::regex cmd_on_regex, cmd_off_regex;

  bool polling_on;

private:
  /// \brief command callback for address and power commands, which update the desired state
  bool _pwr_cmd(const sentry_msgs::PWRCmd::Request& req, const sentry_msgs::PWRCmd::Response& resp);

  /// \brief command callback for direct string messages (make it easier for MC)
  bool _pwr_string_cmd(const sentry_msgs::PWRstringCmd::Request& req, const sentry_msgs::PWRstringCmd::Response& resp);

  /// \brief command callback for mission powerup (def'd in yaml)
  bool _pwr_mission_powerup_cmd(const ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& resp);

  /// \brief command callback for mission powerdown (def'd in yaml)
  bool _pwr_mission_powerdown_cmd(const ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& resp);

  /// \brief send a single one-time command to the io state machine
  void _send_preempt(const std::string& cmdstr);

  /// \brief if latest_cmd and desired_cmd don't match, send the desired_cmd
  void _command_desired_state();

  /// \brief update the regexes using the latest address setting
  void _update_regex();

  /// \brief sets the desired command of a single device
  bool _set_dev(int id, int cmd);

  /// \brief sends an on/off command for a single device
  bool _cmd_dev(int id, int cmd);

protected:
  /// \brief overwritten functions from DsBusDevice
  void setupParameters() override;
  void setupIoSM() override;
  void setupConnections() override;
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;

public:
  /// \brief unit-tested functions for accepting new information
  bool acceptsMessage(const std::string& msg);
  bool handleMessage(const std::string& msg);
};

}  // NAMESPACE

#endif  // PROJECT_PWR_H
