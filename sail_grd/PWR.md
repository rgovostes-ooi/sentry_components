Author: J Vaccaro
Updated: 2/21/18

PWR : DsBusDevice

** include/sail_grd/pwr.h
** src/sail_grd/pwr.cpp

Using the GRD sail bus, PWR drivers regularly query and publish
the latest PWR status information on the topic with their node's name.
If a device gets powered on/off by some other means, then the PWR will
send commands to reset its desired state.

They maintain and enforce which instruments should be on or off.

Launch requires
    - ds_bus

Service servers
    - PWRCmd.srv : set a PWR device on or off
      ~nodename/cmd

    - PWRstringCmd.srv : send a custom text command directly to PWR
      ~nodename/string_cmd

Publishers
    - PWR.msg
      ~nodename