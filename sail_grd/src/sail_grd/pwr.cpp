/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/8/18.
//

#include "../../include/sail_grd/pwr.h"
#include "ds_core_msgs/IoCommandList.h"
#include "ds_hotel_msgs/PWRdevice.h"
#include "../../include/sail_grd/pwr_lookup.h"

using namespace sail_grd;

PWR::PWR() : ds_base::DsBusDevice()
{
  polling_on = true;
  address_ = "GRD/PWR";
  handled = ds_hotel_msgs::PWR{};

  _update_regex();
}

PWR::PWR(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  polling_on = true;
  address_ = "GRD/PWR";
  handled = ds_hotel_msgs::PWR{};

  _update_regex();
}

PWR::~PWR() = default;

bool PWR::acceptsMessage(const std::string& msg)
{
  // match if the string starts with
  return (boost::regex_search(msg, match_regex));
}

bool PWR::handleMessage(const std::string& msg)
{
  boost::smatch results;
  if (!boost::regex_search(msg, results, parse_regex))
  {
    ROS_INFO_STREAM("Unable to parse PWR string for sensor " << address_);
    ROS_INFO_STREAM(msg);
    return false;
  }
  for (int i = 0; i < 8; i++)
  {
    handled.pwr_cmd[i] = std::strtoul(results[i + 1].str().c_str(), nullptr, 16);
    ROS_INFO_STREAM("Cmd " << i << ": " << handled.pwr_cmd[i]);
  }
  int cmd_index;
  int shift_by;
  uint32_t shifted;
  for (int i = 0; i < handled.devices.size(); i++)
  {
    cmd_index = 7 - (handled.devices[i].id / 8);
    if (cmd_index < 0)
    {
      cmd_index = 0;
    }
    shift_by = handled.devices[i].id % 8;
    shifted = handled.pwr_cmd[cmd_index] >> shift_by;
    handled.devices[i].is_on = (shifted & 0x01 ? true : false);
    if (handled.devices[i].is_on)
    {
      devices_[handled.devices[i].id].latest_cmd = sentry_msgs::PWRCmd::Request::PWR_CMD_ON;
    }
    else
    {
      devices_[handled.devices[i].id].latest_cmd = sentry_msgs::PWRCmd::Request::PWR_CMD_OFF;
    }
  }
  _command_desired_state();
  return true;
}

void PWR::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 10;
  cmd.timeout_ms = 100;
  cmd.delayAfter_ms = 400;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query our voltages // DON"T QUERY WHEN RUNNIN OLD SENTRYSITTER
  if (polling_on)
  {
    cmd.command = "#" + address_ + "?P";
    sm_cmd.request.commands.push_back(cmd);
  }

  if (IosmCmd().call(sm_cmd))
  {
    ROS_INFO_STREAM("Possibly got functioning I/O state machine");
  }
  else
  {
    ROS_INFO_STREAM("Error calling I/O State Machine config service");
  }
}

void PWR::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();
  address_ = ros::param::param<std::string>("~sail_address", "GRD/PWR");
  state_maintain_enabled = ros::param::param<bool>("~state_maintain", false);
  polling_on = ros::param::param<bool>("~polling_on", true);

  std::string sensor_name;
  std::string i_str;
  for (uint16_t i = 0x00; i <= 0xFF; i++)
  {
    sensor_name = "";
    //        int this_foo = grd_util::return_this(i);
    sensor_name = grd_util::get_name(i);
    if (sensor_name != "")
    {  // If this address has a parameter device
      // create pwr-state tracker
      pwr_device foo;
      foo.name = sensor_name;
      foo.desired_cmd = 0;
      foo.latest_cmd = 0;
      devices_[i] = foo;

      // create pwr-device msg
      ds_hotel_msgs::PWRdevice bar;
      bar.name = sensor_name;
      bar.id = i;
      bar.is_on = true;
      bar.cmd = sentry_msgs::PWRCmd::Request::PWR_CMD_OFF;
      handled.devices.push_back(bar);
      ROS_INFO_STREAM(i_str << ": " << sensor_name);
    }
  }

  _update_regex();
  auto generated_uuid = ds_base::generateUuid("sail_bat_" + address_);
}

void PWR::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();

  auto nh = nodeHandle();
  pwr_pub_ = nh.advertise<ds_hotel_msgs::PWR>(ros::this_node::getName() + "/state", 10);

  pwr_cmd_ = nh.advertiseService<sentry_msgs::PWRCmd::Request, sentry_msgs::PWRCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&PWR::_pwr_cmd, this, _1, _2));
  pwr_string_cmd_ = nh.advertiseService<sentry_msgs::PWRstringCmd::Request, sentry_msgs::PWRstringCmd::Response>(
      ros::this_node::getName() + "/string_cmd", boost::bind(&PWR::_pwr_string_cmd, this, _1, _2));
  pwr_mission_powerup_cmd_ = nh.advertiseService<ds_core_msgs::VoidCmd::Request, ds_core_msgs::VoidCmd::Response>(
      ros::this_node::getName() + "/mission_powerup", boost::bind(&PWR::_pwr_mission_powerup_cmd, this, _1, _2));
  pwr_mission_powerdown_cmd_ = nh.advertiseService<ds_core_msgs::VoidCmd::Request, ds_core_msgs::VoidCmd::Response>(
      ros::this_node::getName() + "/mission_powerdown", boost::bind(&PWR::_pwr_mission_powerdown_cmd, this, _1, _2));
}

void PWR::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());
  if (handleMessage(msg))
  {
    // publish!
    ROS_INFO_STREAM("PWR \"" << address_ << "\"");
    handled.header.stamp = ros::Time::now();

    pwr_pub_.publish(handled);
  }
  else
  {
    ROS_INFO_STREAM("failed to handleMessage");
  }
}

bool PWR::_pwr_cmd(const sentry_msgs::PWRCmd::Request& req, const sentry_msgs::PWRCmd::Response& resp)
{
  if (!_set_dev(req.address, req.command))
  {
    return false;
  }
  _command_desired_state();
  return true;
}

bool PWR::_pwr_string_cmd(const sentry_msgs::PWRstringCmd::Request& req,
                          const sentry_msgs::PWRstringCmd::Response& resp)
{
  // Send the command immediately!
  std::string cmdstr = req.command;
  ROS_INFO_STREAM("PWR STRING COMMAND : " << cmdstr);
  _send_preempt(cmdstr);

  // attempt to interpret the message to update the desired_state
  boost::smatch results;
  int req_address;
  if (boost::regex_search(cmdstr, results, cmd_on_regex))
  {
    ROS_INFO_STREAM("It's a cmd on message!");
    req_address = std::strtoul(results[1].str().c_str(), nullptr, 16);

    if (_set_dev(req_address, sentry_msgs::PWRCmd::Request::PWR_CMD_ON))
    {
      _command_desired_state();  // If there was some bad syntax, then resend
    }
  }
  else if (boost::regex_search(cmdstr, results, cmd_off_regex))
  {
    ROS_INFO_STREAM("It's a cmd off message!");
    req_address = std::strtoul(results[1].str().c_str(), nullptr, 16);

    if (_set_dev(req_address, sentry_msgs::PWRCmd::Request::PWR_CMD_OFF))
    {
      _command_desired_state();  // If there was some bad syntax, then resend
    }
  }
  return true;  // You already sent the exact command, whether or not the desired_state was updated
}

bool PWR::_pwr_mission_powerup_cmd(const ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& resp)
{
  ROS_INFO_STREAM("MISSION POWERUP");
  std::string cmd_str = "#" + address_ + "!S" + grd_util::get_powerup_string() + "X";
  ROS_INFO_STREAM("Cmd sent: " << cmd_str);
  _send_preempt(cmd_str);
  resp.success = true;
  resp.msg = "Sent command " + cmd_str;
  return true;
}

bool PWR::_pwr_mission_powerdown_cmd(const ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& resp)
{
  ROS_INFO_STREAM("MISSION POWERDOWN");
  std::string cmd_str = "#" + address_ + "!C" + grd_util::get_powerdown_string() + "X";
  ROS_INFO_STREAM("Cmd sent: " << cmd_str);
  _send_preempt(cmd_str);
  resp.success = true;
  resp.msg = "Sent command " + cmd_str;
  return true;
}

void PWR::_send_preempt(const std::string& cmdstr)
{
  ds_core_msgs::IoCommandList cmdList;
  ds_core_msgs::IoCommand cmd;
  cmd.command = cmdstr;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 1000;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  cmdList.cmds.push_back(cmd);

  PreemptCmd().publish(cmdList);
}

void PWR::_command_desired_state()
{
  // go through all of the devices
  for (int i = 0; i < handled.devices.size(); i++)
  {
    uint8_t id = handled.devices[i].id;

    if (!state_maintain_enabled || devices_[i].desired_cmd == 0)
    {  // if desired_cmd hasn't been set yet
      devices_[i].desired_cmd = devices_[i].latest_cmd;
    }
    // if desired and latest command don't match, then send the desired command

    if (state_maintain_enabled && devices_[id].desired_cmd != devices_[id].latest_cmd)
    {
      _cmd_dev(id, devices_[id].desired_cmd);
    }
  }
}

void PWR::_update_regex()
{
  match_regex = boost::regex{ "#" + address_ };
  parse_regex = boost::regex{ "#" + address_ + ".+P([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2}) "
                                               "([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2}) ([0-9A-Fa-f]{2})" };
  cmd_on_regex = boost::regex{ "#" + address_ + "IS([0-9A-Fa-f]{2})X" };
  cmd_off_regex = boost::regex{ "#" + address_ + "IC([0-9A-Fa-f]{2})X" };

  handled.devices.resize(devices_.size());
}

bool PWR::_set_dev(int id, int cmd)
{
  if (!state_maintain_enabled)
  {  // If state is not maintained, then we need to send the command here.
    _cmd_dev(id, cmd);
  }
  if (cmd != sentry_msgs::PWRCmd::Request::PWR_CMD_ON && cmd != sentry_msgs::PWRCmd::Request::PWR_CMD_OFF)
  {
    ROS_ERROR_STREAM("INVALID COMMAND: " << cmd);
    return false;
  }
  if (devices_.find(id) != devices_.end())
  {
    devices_[id].desired_cmd = cmd;
    return true;
  }
  ROS_ERROR_STREAM("DEVICE ID NOT FOUND: " << id);
  return false;
}

bool PWR::_cmd_dev(int id, int cmd)
{
  // IMPORTANT:: ONLY CALLED IN _command_desired_state when state_maintain is true
  // otherwise called whenever a command is made
  // WE DON'T WANT TO SEND COMMANDS WITHOUT UPDATING THE STATE FIRST is we are maintaining state

  std::string cmdstr = "#" + address_ + "!";
  std::string cmdsuffix = "X";

  if (cmd == sentry_msgs::PWRCmd::Request::PWR_CMD_OFF)
  {
    cmdstr += "IC";
  }
  else if (cmd == sentry_msgs::PWRCmd::Request::PWR_CMD_ON)
  {
    cmdstr += "IS";
  }
  else
  {
    ROS_ERROR_STREAM("Invalid cmd: " << cmd);
    return false;
  }
  cmdstr += ds_util::int_to_hex(id);
  cmdstr += cmdsuffix;
  _send_preempt(cmdstr);
  return true;
}
