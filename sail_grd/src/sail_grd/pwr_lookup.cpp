/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/6/18.
//

#include "../../include/sail_grd/pwr_lookup.h"

namespace grd_util
{
uint16_t get_address(std::string name, std::string _param_str)
{
  bool found_match = false;
  std::string name_at = "";
  uint16_t address = -1;
  std::string i_str;

  for (uint16_t i = 0; i < 0xFF; i++)
  {
    i_str = ds_util::int_to_hex(i);
    name_at = ros::param::param<std::string>(_param_str + "/x" + i_str + "/name", "");
    if (name_at == name && found_match)
    {
      ROS_ERROR_STREAM("Multiple matches found at " << i << " and " << address);
      return -1;
    }
    else if (name_at == name)
    {
      found_match = true;
      address = i;
    }
  }
  return address;
}

std::vector<uint16_t> get_powerup_addresses(std::string name, std::string _param_str)
{
  std::vector<uint16_t> vec;
  std::string i_str;

  for (uint16_t i = 0; i < 0xFF; i++)
  {
    bool powerup_at = false;
    i_str = ds_util::int_to_hex(i);
    powerup_at = ros::param::param<bool>(_param_str + "/x" + i_str + "/mission_powerup", "");
    if (powerup_at)
      vec.push_back(i);
  }
  return vec;
}

std::vector<uint16_t> get_powerdown_addresses(std::string name, std::string _param_str)
{
  std::vector<uint16_t> vec;
  std::string i_str;

  for (uint16_t i = 0; i < 0xFF; i++)
  {
    bool powerup_at = false;
    i_str = ds_util::int_to_hex(i);
    powerup_at = ros::param::param<bool>(_param_str + "/x" + i_str + "/mission_powerdown", "");
    if (powerup_at)
      vec.push_back(i);
  }
  return vec;
}

std::string get_name(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  std::string name = ros::param::param<std::string>(_param_str + "/x" + i_str + "/name", "");
  return name;
}

bool get_confirm_on(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  bool confirm_on = ros::param::param<bool>(_param_str + "/x" + i_str + "/confirm_on", false);
  return confirm_on;
}

bool get_confirm_off(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  bool confirm_off = ros::param::param<bool>(_param_str + "/x" + i_str + "/confirm_off", false);
  return confirm_off;
}

std::string get_group(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  std::string group = ros::param::param<std::string>(_param_str + "/x" + i_str + "/group", "");
  return group;
}

bool get_mission_poweron(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  bool mission_poweron = ros::param::param<bool>(_param_str + "/x" + i_str + "/mission_powerup", false);
  return mission_poweron;
}

bool get_mission_poweroff(uint16_t address, std::string _param_str)
{
  std::string i_str = ds_util::int_to_hex(address);
  bool mission_poweroff = ros::param::param<bool>(_param_str + "/x" + i_str + "/mission_powerdown", false);
  return mission_poweroff;
}

std::string get_powerup_string(std::string _param_str)
{
  uint16_t cmd_int[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  std::string cmd("");

  std::string i_str("");
  uint16_t j(0);

  for (uint16_t i = 0; i < 0xFF; i++)
  //            Get the poweron bool value
  {
    j = 7 - (i / 8);
    if (get_mission_poweron(i, _param_str))
    {
      uint16_t mask = pow(2, i % 8);
      //                ROS_ERROR_STREAM(mask << " | " << cmd_int[j]);
      cmd_int[j] = cmd_int[j] | mask;
      //                ROS_ERROR_STREAM("        = " << cmd_int[j]);
    }
  }
  for (int k = 0; k < 8; k++)
  //            Convert the one-hot array into a command string
  {
    cmd += ds_util::int_to_hex<uint16_t>(cmd_int[k]);
  }
  return cmd;
}

std::string get_powerdown_string(std::string _param_str)
{
  uint16_t cmd_int[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  std::string cmd("");

  std::string i_str("");
  uint16_t j(0);

  for (uint16_t i = 0; i < 0xFF; i++)
  //            Get the poweron bool value
  {
    j = 7 - (i / 8);
    if (get_mission_poweroff(i, _param_str))
    {
      uint16_t mask = pow(2, i % 8);
      //                ROS_ERROR_STREAM(mask << " | " << cmd_int[j]);
      cmd_int[j] = cmd_int[j] | mask;
      //                ROS_ERROR_STREAM(j << ": " << cmd_int[j]);
    }
  }
  for (int k = 0; k < 8; k++)
  //            Convert the one-hot array into a command string
  {
    cmd += ds_util::int_to_hex<uint16_t>(cmd_int[k]);
  }
  return cmd;
}
}
