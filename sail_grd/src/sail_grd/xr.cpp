/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/8/18.
//

#include "../../include/sail_grd/xr.h"
#include "sentry_msgs/XRCmd.h"
#include "ds_core_msgs/IoCommandList.h"
#include "ds_util/ds_util.h"

using namespace sail_grd;

XR::XR() : ds_base::DsBusDevice()
{
  address_ = "XR1";
  handled = ds_hotel_msgs::XR{};

  _update_regex();
  xrAbortDetected = false;
}

XR::XR(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  address_ = "XR1";
  handled = ds_hotel_msgs::XR{};

  _update_regex();
  xrAbortDetected = false;
}

XR::~XR() = default;

bool XR::acceptsMessage(const std::string& msg)
{
  // match if the string starts with
  return boost::regex_search(msg, match_regex);
}

bool XR::handleMessage(const std::string& msg)
{
  boost::smatch results;
  if (!acceptsMessage(msg))
  {
    ROS_INFO_STREAM("Does not accept message to " << address_);
    return false;
  }
  if (boost::regex_search(msg, results, deadhour_regex))
  {
    ROS_INFO_STREAM("Parsing deadhour XR string for sensor " << address_);
    handled.Dd = std::strtoul(results[1].str().c_str(), nullptr, 16);
    handled.deadhour = handled.Dd / 3600.0;
    return true;
  }
  if (boost::regex_search(msg, results, code_regex))
  {
    ROS_INFO_STREAM("Parsing code XR string for sensor " << address_);
    handled.C = std::strtoul(results[1].str().c_str(), nullptr, 16);
    if (handled.C == 14 || handled.C == 15)
    {
      ROS_FATAL_STREAM("Got XR Abort! XR " << address_ << " reporting itself as dead.");
      xrAbortDetected = true;
      onCriticalProcessTimer(ros::TimerEvent());
    }
    return true;
  }
  if (boost::regex_search(msg, results, release_regex))
  {
    ROS_INFO_STREAM("Parsing release XR string for sensor " << address_);
    handled.V = std::strtoul(results[1].str().c_str(), nullptr, 16);
    handled.valve_current_level = handled.V >> 4;
    handled.valve_run = handled.V & 8;
    handled.valve_open = handled.V & 4;
    handled.valve_battery_set_on = handled.V & 2;
    handled.valve_battery_set_off = handled.V & 1;

    handled.M = std::strtoul(results[2].str().c_str(), nullptr, 16);
    handled.motor_2_drop = handled.M & 32;
    handled.motor_2_hold = handled.M & 16;
    handled.motor_1_drop = handled.M & 2;
    handled.motor_1_hold = handled.M & 1;

    handled.A = std::strtoul(results[3].str().c_str(), nullptr, 16);
    handled.burnwire_2_drive = handled.A & 2;
    handled.burnwire_1_drive = handled.A & 1;

    handled.motor_1_secs = std::strtoul(results[4].str().c_str(), nullptr, 16);
    handled.motor_2_secs = std::strtoul(results[5].str().c_str(), nullptr, 16);

    handled.burnwire_1_secs = std::strtoul(results[6].str().c_str(), nullptr, 16);
    handled.burnwire_2_secs = std::strtoul(results[7].str().c_str(), nullptr, 16);

    handled.short_deadsecs = std::strtoul(results[8].str().c_str(), nullptr, 16);
    handled.short_deadsecs_idle = std::strtoul(results[9].str().c_str(), nullptr, 16);

    handled.deadsecs = std::strtoul(results[10].str().c_str(), nullptr, 16);
    handled.deadhour = handled.deadsecs / 3600.0;

    handled.good = true;
    status_release_received = true;
    return true;
  }
  ROS_ERROR_STREAM("Correct address, but unrecognized data type " << msg);
  return false;
}

void XR::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 10;
  cmd.timeout_ms = 100;
  cmd.delayAfter_ms = 10;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query our voltages
  cmd.command = "#" + address_ + "?D";  // Deadman status
  sm_cmd.request.commands.push_back(cmd);

  cmd.command = "#" + address_ + "?C";  // Code status
  sm_cmd.request.commands.push_back(cmd);

  cmd.command = "#" + address_ + "?S";  // Release status
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().waitForExistence(ros::Duration(10.0)))
  {
    if (IosmCmd().call(sm_cmd))
    {
      // TODO
      ROS_INFO_STREAM("Possibly got functioning I/O state machine");
    }
    else
    {
      ROS_INFO_STREAM("Error calling I/O State Machine config service");
    }
  }
  else
  {
    ROS_FATAL_STREAM("ERROR in XR driver while waiting for GRD SAIL bus node to start");
  }

  // now add some preempt commands
  // for some reason, _send_preempt doesn't work well at startup.
  // So use the service
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_PREEMPT;
  sm_cmd.request.commands.resize(0);

  cmd.command = "#" + address_ + "!TRFF";
  sm_cmd.request.commands.push_back(cmd);

  // turn on the XR batteries at startup
  cmd.command = "#" + address_ + "!B1";
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().call(sm_cmd))
  {
    ROS_INFO_STREAM("Possibly got functioning I/O state machine");
  }
  else
  {
    ROS_FATAL_STREAM("Unable to turn XR battery on at startup...");
  }

  //_send_preempt("#" + address_ + "!TR" + "FF");
  // turn on the XR batteries at startup
  //_send_preempt("#" + address_ + "!B1");
}

void XR::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();

  address_ = ros::param::param<std::string>("~sail_address", "GRD/XR");

  _update_regex();

  auto generated_uuid = ds_base::generateUuid("sail_bat_" + address_);
}

void XR::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();

  // prepare our publishers

  auto nh = nodeHandle();
  xr_pub_ = nh.advertise<ds_hotel_msgs::XR>(ros::this_node::getName() + "/state", 10);
  abort_sub_ = nh.subscribe<ds_core_msgs::Abort>("abort", 1, &XR::_abort_reset, this);

  deadhour_cmd_ = nh.advertiseService<sentry_msgs::DeadhourCmd::Request, sentry_msgs::DeadhourCmd::Response>(
      ros::this_node::getName() + "/deadhour", boost::bind(&XR::_deadhr_cmd, this, _1, _2));

  xr_cmd_ = nh.advertiseService<sentry_msgs::XRCmd::Request, sentry_msgs::XRCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&XR::_xr_cmd, this, _1, _2));

  xr_code_ = nh.advertiseService<sentry_msgs::XRCode::Request, sentry_msgs::XRCode::Response>(
      ros::this_node::getName() + "/set_code", boost::bind(&XR::_xr_code, this, _1, _2));
}

void XR::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());

  if (handleMessage(msg))
  {
    if (status_release_received)
    {
      ROS_INFO_STREAM("XR \"" << address_ << "\"");
      handled.header.stamp = ros::Time::now();

      // most fields populated- publish!
      xr_pub_.publish(handled);
      status_release_received = false;
    }
  }
  else
  {
    ROS_INFO_STREAM("failed to handleMessage");
  }
}

void XR::onCriticalProcessTimer(const ros::TimerEvent& event)
{
  auto msg = criticalProcessMessage();

  // if we've received an abort code, set to -1 to
  // IMMEDIATELY trigger a systemwide abort
  if (xrAbortDetected)
  {
    msg.ttl = -1;
  }
  publishCriticalProcess(msg);
}

void XR::_abort_reset(ds_core_msgs::Abort msg)
{
  ROS_INFO_STREAM("ABORT MSG RECEIVED");

  // ALWAYS send a short deadman reset
  // if we're aborting, the abort manager will handle
  // burn wires, drop motors, etc
  if (msg.ttl < 0)
  {  // If we have stopped the short deadman timer
    return;
  }
  _send_preempt(cmdstr_ + "!TR" + "FF");
}

bool XR::_deadhr_cmd(const sentry_msgs::DeadhourCmd::Request& req, const sentry_msgs::DeadhourCmd::Response& resp)
{
  auto deadsec = static_cast<int32_t>(req.deadhour * 3600);
  if (req.deadhour > 255)
  {
    return false;
//    deadsec = 918000;
  } else if (req.deadhour < 0){
    return false;
//    deadsec = 0;
  }
  ROS_INFO_STREAM("Deadhour "<< req.deadhour << " Deadsecs " << deadsec);
  // Set the simplest version of the deadman command first
  std::string cmdstr_dead = cmdstr_ + "!DEADMAN=" + ds_util::int_to_32_hex(deadsec);
  // If the form is any more complicated than XR1 or XR2, then there is a GRD prefix and we need the longform cmd
  //    if (address_.size() > 3){
  //        cmdstr_dead = long_cmdstr_ + "!DEADMAN=" + ds_util::int_to_32_hex(deadsec) + " D=" +
  //        ds_util::int_to_32_hex(deadsec);
  //    }
  ROS_INFO_STREAM("DEADHOUR COMMAND =" << req.deadhour);
  _send_preempt(cmdstr_dead);
  _send_preempt("Y");
  _send_preempt("#" + address_ + "!TR" + "00");
  return true;
}

bool XR::_xr_cmd(const sentry_msgs::XRCmd::Request& req, const sentry_msgs::XRCmd::Response& resp)
{
  //    std::string cmdstr_ = "#" + address_ ;
  //    std::string longcmd = "#GRD%P%R%RXR2";

  switch (req.command)
  {
    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN:
      _send_preempt(cmdstr_ + "!D1");  // D is for drop
      _send_preempt(cmdstr_ + "!D2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE:
      _send_preempt(cmdstr_ + "!H1");  // H is for hold
      _send_preempt(cmdstr_ + "!H2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON:
      _send_preempt(cmdstr_ + "!N1");  // N is for nichrome burn wires
      _send_preempt(cmdstr_ + "!N2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF:
      _send_preempt(cmdstr_ + "!M60X 0000\r");  // M is for memory address you overwrite to make it happen
      _send_preempt(cmdstr_ + "!M62X 0000\r");
      break;

    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_1:
      _send_preempt(cmdstr_ + "!D1");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE_1:
      _send_preempt(cmdstr_ + "!H1");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_1:
      _send_preempt(cmdstr_ + "!N1");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF_1:
      _send_preempt(cmdstr_ + "!M60X 0000\r");
      break;

    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_2:
      _send_preempt(cmdstr_ + "!D2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE_2:
      _send_preempt(cmdstr_ + "!H2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_2:
      _send_preempt(cmdstr_ + "!N2");
      break;
    case sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF_2:
      _send_preempt(cmdstr_ + "!M62X 0000\r");
      break;

    case sentry_msgs::XRCmd::Request::XR_CMD_BATTERY_ON:
      _send_preempt(cmdstr_ + "!B1");
      break;

    default:
      ROS_ERROR_STREAM("Invalid XR command");
      return false;
  }
  return true;
}

bool XR::_xr_code(const sentry_msgs::XRCode::Request& req, const sentry_msgs::XRCode::Response& resp)
{
  if (req.code > 15 || req.code < 0)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " was asked to set XR code to " << req.code
                                               << " which is outside the valid range of 0-15");
    return false;
  }

  _send_preempt(cmdstr_ + "!C" + ds_util::int_to_hex(req.code));
  return true;
}

void XR::_send_preempt(const std::string& str)
{
  ds_core_msgs::IoCommandList cmdList;
  ds_core_msgs::IoCommand cmd;
  cmd.command = str;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 100;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  cmdList.cmds.push_back(cmd);
  ROS_INFO_STREAM("Sending preempt: " << str);

  PreemptCmd().publish(cmdList);
}

const ds_hotel_msgs::XR& XR::getHandled() const
{
  return handled;
}

void XR::_update_regex()
{
  ROS_INFO_STREAM("update regex start");
  handled.address = address_;

  match_regex = boost::regex{ "#" + address_ };
  parse_regex = boost::regex{ "#" + address_ + "\\?" };
  deadhour_regex = boost::regex{ "#" + address_ + "\\?D ([0-9A-Fa-f]{8})" };
  code_regex = boost::regex{ "#" + address_ + "\\?C\\-([0-9A-Fa-f]{2})" };
  release_regex =
      boost::regex{ "#" + address_ + "\\?S V=([0-9A-Fa-f]{2}) M=([0-9A-Fa-f]{2}) A=([0-9A-Fa-f]{2}) "
                                     "M1=([0-9A-Fa-f]{2}) M2=([0-9A-Fa-f]{2}) N1=([0-9A-Fa-f]{4}) N2=([0-9A-Fa-f]{4}) "
                                     "TR=([0-9A-Fa-f]{2}) TV=([0-9A-Fa-f]{2}) D=([0-9A-Fa-f]{8})" };

  // parse our IDnum.  Assume our addr is either XR# or GRD/XR#
  boost::regex idnumFind{ "([1-2]{1})" };
  boost::smatch idnum_find;
  if (!boost::regex_search(address_, idnum_find, idnumFind))
  {
    ROS_ERROR_STREAM("IDNUM not found!");
  }
  else
  {
    handled.idnum = std::strtoul(idnum_find[1].str().c_str(), nullptr, 10);
  }

  cmdstr_ = "#" + address_;

  boost::regex idnumGRD{ "GRD" };
  boost::smatch idnum_grd;
  if (!boost::regex_search(address_, idnum_grd, idnumGRD))
  {
    // address_ in format XR1
    ROS_DEBUG_STREAM("XR::" << address_);
    ROS_DEBUG_STREAM("Unable to find ID Number in battery string");
    long_cmdstr_ = cmdstr_;
  }
  else
  {
    // address_ i format GRD/XR1
    long_cmdstr_ = "#GRD%P%R%RXR" + std::to_string(handled.idnum);
  }

  ROS_INFO_STREAM("update regex end");
  status_release_received = false;
}
