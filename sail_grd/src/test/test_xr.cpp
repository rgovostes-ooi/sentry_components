/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/8/18.
//

#include "../../include/sail_grd/xr.h"

#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class XRTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(XRTest, AcceptsGood)
{
  std::string testStr("#XR1");
  sail_grd::XR this_xr;
  EXPECT_TRUE(this_xr.acceptsMessage(testStr));
}

TEST_F(XRTest, ParsesH)
{
  std::string testStr("#XR1?");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  EXPECT_FALSE(parse);
}

TEST_F(XRTest, ParsesD)
{
  std::string testStr("#XR1?D FFFA9CF0");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  EXPECT_TRUE(parse);
}

TEST_F(XRTest, ParsesD_full)
{
  std::string testStr("#XR1?D 12345678");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_TRUE(parse);

  const ds_hotel_msgs::XR& result = this_xr.getHandled();
  // not set by this message
  EXPECT_EQ(0, result.V);
  EXPECT_EQ(0, result.M);
  EXPECT_EQ(0, result.A);
  EXPECT_EQ(0, result.motor_1_secs);
  EXPECT_EQ(0, result.motor_2_secs);
  EXPECT_EQ(0, result.burnwire_1_secs);
  EXPECT_EQ(0, result.burnwire_2_secs);
  EXPECT_EQ(0, result.short_deadsecs);
  EXPECT_EQ(0, result.short_deadsecs_idle);
  EXPECT_EQ(0, result.deadsecs);
  EXPECT_FALSE(result.good);

  // not set by this message
  EXPECT_EQ(0, result.C);

  // set by this message
  EXPECT_EQ(0x12345678, result.Dd);
}

TEST_F(XRTest, ParsesD_tricky)
{
  std::string testStr("#XR1?D 123C0158");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_TRUE(parse);

  const ds_hotel_msgs::XR& result = this_xr.getHandled();
  // not set by this message
  EXPECT_EQ(0, result.V);
  EXPECT_EQ(0, result.M);
  EXPECT_EQ(0, result.A);
  EXPECT_EQ(0, result.motor_1_secs);
  EXPECT_EQ(0, result.motor_2_secs);
  EXPECT_EQ(0, result.burnwire_1_secs);
  EXPECT_EQ(0, result.burnwire_2_secs);
  EXPECT_EQ(0, result.short_deadsecs);
  EXPECT_EQ(0, result.short_deadsecs_idle);
  EXPECT_EQ(0, result.deadsecs);
  EXPECT_FALSE(result.good);

  // not set by this message
  EXPECT_EQ(0, result.C);

  // set by this message
  EXPECT_EQ(0x123C0158, result.Dd);
}

TEST_F(XRTest, ParsesC)
{
  std::string testStr("#XR1?C-00");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  EXPECT_TRUE(parse);
}

TEST_F(XRTest, ParsesC_full)
{
  std::string testStr("#XR1?C-15");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_TRUE(parse);

  const ds_hotel_msgs::XR& result = this_xr.getHandled();
  // not set by this message
  EXPECT_EQ(0, result.V);
  EXPECT_EQ(0, result.M);
  EXPECT_EQ(0, result.A);
  EXPECT_EQ(0, result.motor_1_secs);
  EXPECT_EQ(0, result.motor_2_secs);
  EXPECT_EQ(0, result.burnwire_1_secs);
  EXPECT_EQ(0, result.burnwire_2_secs);
  EXPECT_EQ(0, result.short_deadsecs);
  EXPECT_EQ(0, result.short_deadsecs_idle);
  EXPECT_EQ(0, result.deadsecs);
  EXPECT_FALSE(result.good);

  // set by this message
  EXPECT_EQ(0x15, result.C);

  // not set by this message
  EXPECT_EQ(0, result.Dd);
}

TEST_F(XRTest, ParsesS)
{
  std::string testStr("#XR1?S V=00 M=00 A=30 M1=00 M2=00 N1=0000 N2=0000 TR=00 TV=00 D=FFFA9CF0");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  EXPECT_TRUE(parse);
}

TEST_F(XRTest, ParsesS_full)
{
  std::string testStr("#XR1?S V=11 M=22 A=33 M1=44 M2=55 N1=1234 N2=5678 TR=66 TV=77 D=FFFA9CF0");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_TRUE(parse);

  const ds_hotel_msgs::XR& result = this_xr.getHandled();
  EXPECT_EQ(0x11, result.V);
  EXPECT_EQ(0x22, result.M);
  EXPECT_EQ(0x33, result.A);
  EXPECT_EQ(0x44, result.motor_1_secs);
  EXPECT_EQ(0x55, result.motor_2_secs);
  EXPECT_EQ(0x1234, result.burnwire_1_secs);
  EXPECT_EQ(0x5678, result.burnwire_2_secs);
  EXPECT_EQ(0x66, result.short_deadsecs);
  EXPECT_EQ(0x77, result.short_deadsecs_idle);
  EXPECT_EQ(0xFFFA9CF0, result.deadsecs);
  EXPECT_TRUE(result.good);

  // not set by this message
  EXPECT_EQ(0x0, result.C);
  EXPECT_EQ(0, result.Dd);
}

TEST_F(XRTest, ParsesS_noreply)
{
  std::string testStr("#XR1?S#XR2?S V=11 M=22 A=33 M1=44 M2=55 N1=1234 N2=5678 TR=66 TV=77 D=FFFA9CF0");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_FALSE(parse);
}

TEST_F(XRTest, ParsesAbortBug)
{
  std::string testStr("#XR1?S V=11 M=22 A=33 M1=44 M2=55 N1=C015 N2=1234 TR=66 TV=77 D=FFFA9CF0");
  sail_grd::XR this_xr;
  auto parse = this_xr.handleMessage(testStr);
  ASSERT_TRUE(parse);

  const ds_hotel_msgs::XR& result = this_xr.getHandled();
  EXPECT_EQ(0x11, result.V);
  EXPECT_EQ(0x22, result.M);
  EXPECT_EQ(0x33, result.A);
  EXPECT_EQ(0x44, result.motor_1_secs);
  EXPECT_EQ(0x55, result.motor_2_secs);
  EXPECT_EQ(0xC015, result.burnwire_1_secs);
  EXPECT_EQ(0x1234, result.burnwire_2_secs);
  EXPECT_EQ(0x66, result.short_deadsecs);
  EXPECT_EQ(0x77, result.short_deadsecs_idle);
  EXPECT_EQ(0xFFFA9CF0, result.deadsecs);
  EXPECT_TRUE(result.good);

  // not set by this message
  EXPECT_EQ(0x0, result.C);
  EXPECT_EQ(0, result.Dd);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
