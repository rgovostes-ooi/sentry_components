/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/27/18.
//

#include "../../include/sail_sci/a2d2.h"

using namespace sail_sci;

A2D2::A2D2() : ds_base::DsBusDevice()
{
  address_ = "AD3";
  handled = ds_hotel_msgs::A2D2{};

  update_regex();
}

A2D2::A2D2(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  address_ = "AD1";
  handled = ds_hotel_msgs::A2D2{};

  update_regex();
}

A2D2::~A2D2() = default;

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool A2D2::acceptsMessage(const std::string& msg)
{
  return boost::regex_search(msg, match_regex);
}

bool A2D2::handleMessage(const std::string& msg)
{
  boost::smatch results;
  if (!boost::regex_search(msg, results, parse_regex))
  {
    ROS_INFO_STREAM("Unable to parse HTP string for sensor " << address_);
    ROS_INFO_STREAM(msg);
    return false;
  }

  for (int i = 0; i < 4; i++)
  {
    handled.raw[i] = std::strtod(results[i + 1].str().c_str(), nullptr);
  }
  process_raw();
  return true;
}

void A2D2::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 10;
  cmd.timeout_ms = 100;
  cmd.delayAfter_ms = 10;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query our voltages
  cmd.command = "#" + address_ + "D";
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().call(sm_cmd))
  {
    // TODO
    ROS_INFO_STREAM("Possibly got functioning I/O state machine");
  }
  else
  {
    ROS_INFO_STREAM("Error calling I/O State Machine config service");
  }
}

void A2D2::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();

  address_ = ros::param::param<std::string>("~sail_address", "AD0");
  type_ = ros::param::param<std::string>("~a2d2_type", "");

  update_regex();

  auto generated_uuid = ds_base::generateUuid("sail_bat_" + address_);
}

void A2D2::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();

  // prepare our publishers
  a2d2_pub_ = nodeHandle().advertise<ds_hotel_msgs::A2D2>(ros::this_node::getName() + "/a2d2", 10);
}

void A2D2::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());

  if (handleMessage(msg))
  {
    // publish!
    ROS_INFO_STREAM("A2D2 \"" << address_ << "\"");
    handled.header.stamp = ros::Time::now();

    // publish servo states
    a2d2_pub_.publish(handled);
  }
  else
  {
    ROS_INFO_STREAM("failed to handleMessage");
  }
}

void A2D2::update_regex()
{
  match_regex = boost::regex{ "#" + address_ };
  parse_regex = boost::regex{ "#" + address_ + "D ([+-][0-9][.][0-9]{4}) ([+-][0-9][.][0-9]{4}) ([+-][0-9][.][0-9]{4}) "
                                               "([+-][0-9][.][0-9]{4})" };
}

void A2D2::process_raw()
{
  handled.units = "n/a";

  if (type_ == "obs")
  {
    for (int i = 0; i < 4; i++)
    {
      handled.proc[i] = handled.raw[i] + 5.0;
      handled.units = "obs units";
    }
  }
  if (type_ == "orp")
  {
    for (int i = 0; i < 4; i++)
    {
      handled.proc[i] = handled.raw[i] - 5.0;
      handled.units = "orp units";
    }
  }
}
