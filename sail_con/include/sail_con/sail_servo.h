/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/23/18.
//

#ifndef PROJECT_SAIL_SERVO_H
#define PROJECT_SAIL_SERVO_H

#include <ds_base/ds_bus_device.h>

#include <ds_actuator_msgs/ServoCmd.h>
#include <ds_actuator_msgs/ServoState.h>
#include <sentry_msgs/SailServoCmd.h>
#include <sensor_msgs/JointState.h>
#include <ds_core_msgs/Abort.h>

namespace sail_con
{
class SailServo : public ds_base::DsBusDevice
{
public:
  explicit SailServo();
  SailServo(int argc, char* argv[], const std::string& name);
  ~SailServo() override;
  DS_DISABLE_COPY(SailServo)

protected:
  // overrides here

  void setupParameters() override;
  void setupIoSM() override;
  void setupConnections() override;
  void checkProcessStatus(const ros::TimerEvent& event) override;

  /// \brief Stop the servo and have it hold its current position
  void stop();

  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  bool _parseReceivedInner(const ds_core_msgs::RawData& bytes);
  bool _parseQueryReply(const std::string& msg, const ros::Time& stamp);
  ds_core_msgs::Status _checkStatus();

  /// \brief Convert encoder counts to radians
  float counts_to_radians(uint32_t raw);

  /// \brief Convert A2D counts to voltage at the pin
  float a2d_to_Vmeas(uint32_t raw);

  /// \brief Convert A2D counts to voltage measured through a resistor divider
  float a2d_to_voltage(uint32_t raw, float Rupper, float Rlower);

  /// \brief Convert A2D counts to thermistor resistance (needs calibration coefficients!)
  ///
  /// TODO: Make this return degrees
  float a2d_to_therm(uint32_t raw);

  float correct_servo_pos(float raw);
  float uncorrect_servo_pos(float corr);

  ros::Publisher servo_pub_;
  ros::Publisher sentry_servo_pub_;
  ros::Publisher joint_states_pub_;
  ros::Subscriber abort_topic_sub_;
  std::string abort_topic_name_;

  std::string address_;
  std::string speed_;
  std::string joint_name_;

  boost::uuids::uuid uuid_;

  float servo_offset;
  float servo_scale;  // usually +/- for sign

  /// \brief The last time the servo reported its actual position
  ros::Time last_location_time_;

  /// \brief A flag that can be set from the outside
  bool enable;

  /// \brief Servo commanded position subscriber
  ros::Subscriber servo_pos_cmd_;

  /// \brief Servo control command service
  ros::ServiceServer servo_ctrl_cmd_;

  std::vector<uint64_t> sail_cmd_list_;
  uint64_t sail_loc_cmd_id_;

  /// \brief Flag for whether the location data is valid
  bool location_good;

  /// \brief Count of good positions required before accepting commands
  int locations_good_required;

  /// \brief Commanded servo location
  float location_cmd;

  /// \brief Corrected servo location in radians
  float location_rad_corrected;

  /// \brief The raw servo location in radians (no scaling / offset applied)
  float location_rad_raw;

  /// \brief The raw servo location in radians at the last-reported index crossing
  float index_rad_raw;

  /// \brief Thermistor voltage
  float voltage_thermistor;

  /// \brief Measured 48V bus voltages
  float voltage_bus_48v;

  /// \brief Measured 12.5V bus voltage
  float voltage_bus_12p5v;

  /// \brief Measured 6.5V bus voltage
  float voltage_bus_6p5v;

  //------------------------------------------------------------
  // These parameters get loaded from the parameter server

  /// \brief Number of good locations required between indexing and accepting commands
  int GOOD_LOCATION_COMMAND_LOCKOUT_;

  /// \brief Radians per servo count
  float SERVO_COUNTS_TO_RADIANS_;

  /// \brief Upper ADC voltage (voltage when counts=0xff)
  float ADC_V_UPPER_;

  /// \brief Lower ADC voltage (voltage when counts=0x00)
  float ADC_V_LOWER_;

  /// \brief 48V monitor resistor divider, to +48V input
  float RESIS_DIV_48_UPPER_;

  /// \brief 48V monitor resistor divider, to ground
  float RESIS_DIV_48_LOWER_;

  /// \brief 12.5V monitor resistor divider, to +12.5V rail
  float RESIS_DIV_12p5_UPPER_;

  /// \brief 12.5V monitor resistor divider, to ground
  float RESIS_DIV_12p5_LOWER_;

  /// \brief 6.5V monitor resistor divider, to +6.5V rail
  float RESIS_DIV_6p5_UPPER_;

  /// \brief 6.5V monitor resistor divider, to ground
  float RESIS_DIV_6p5_LOWER_;

  /// \brief Thermistor resistor divider resistor, to +V rail
  float RESIS_DIV_THERM_UPPER_;

  /// \brief Thermistor resistor divider upper voltage
  float RESIS_DIV_THERM_VOLT_;

  /// \brief Result from ?M77 query
  uint8_t speed_query_;

  /// \brief Result from ?M0 query
  uint8_t switch_query_;

private:
  void _command_req(const ds_actuator_msgs::ServoCmd& msg);
  bool _ctrl_cmd(const sentry_msgs::SailServoCmd::Request& req, const sentry_msgs::SailServoCmd::Response& resp);
  std::string _make_loc_string(float cmd_radians);
  void _send_preempt(const std::string& cmdstr);
  void _update_loc(const std::string& cmdstr);
  void add_loc_cmd();
  void remove_loc_cmd();
  void _set_enable(const ds_core_msgs::Abort& msg);
  ds_actuator_msgs::ServoState buildServoMsg() const;
  sensor_msgs::JointState buildJointStateMsg() const;
};
}

#endif  // PROJECT_SAIL_SERVO_H
