/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/23/18.
//

#include "sail_con/sail_servo.h"
#include "sail_con/SentryServoState.h"

#include <ds_core_msgs/IoSMcommand.h>
#include <ds_core_msgs/IoCommandList.h>
#include <ds_core_msgs/Abort.h>

using namespace sail_con;

// See EXTENDING.md
// Our constructors use the protected constructor from `DsProcess`, providing our
// own version of the private implementation class.
//
// This newly constructed LambdaPowerSupply::Impl gets implicitly upcast to DsProcess::Impl
// when passed to DsProcess's constructor.
//
// NOTE:  Our public constructors just forward on to our protected versions.  If
// we end up needing to add logic inside the constructors we'll only have to add
// it in two places now (the protected versions) instead of all four.
// Public default constructor:  use our own protected anolog

// Protected 'default' constructor
SailServo::SailServo() : ds_base::DsBusDevice()
{
  enable = false;
  sail_loc_cmd_id_ = 0;
  location_good = false;
  location_rad_corrected = 0;
  locations_good_required = 0;
}

// Protected constructor with arguments for ros::init
SailServo::SailServo(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  enable = false;
  sail_loc_cmd_id_ = 0;
  location_good = false;
  location_rad_corrected = 0;
  locations_good_required = 0;
}

SailServo::~SailServo() = default;

//
// This is how we get access to our new private LambdaPowerSupply::Impl.
// See, in the constructors above, we upcast LambdaPowerSupply::Impl into SensorBase::Impl, where
// it's stored in the SensorBase::impl_ member.
//
// To get the Impl class back *in the propper type* we need to downcast it again before
// working on it, which is why we have the static_cast<>'s here.
//

void SailServo::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();

  address_ = ros::param::param<std::string>("~sail_address", "S1");
  joint_name_ = ros::param::param<std::string>("~urdf_joint_name", "fwd_servo");
  abort_topic_name_ = ros::param::param<std::string>("~abort_topic", "abort");

  //    auto generated_uuid = ds_base::generateUuid("sail_servo_" + d->address_);

  servo_offset = ros::param::param<float>("~offset", 0);
  servo_scale = ros::param::param<float>("~scale", 1.0);  // use to flip sign

  // these are optional parameters.  You almost definitely don't need to change them
  speed_ = ros::param::param<std::string>("~speed", "90");

  SERVO_COUNTS_TO_RADIANS_ = ros::param::param<float>("~radians_per_servo_count", 1.0 / 1163.7);
  ADC_V_UPPER_ = ros::param::param<float>("~adc_upper_v", 5.0);
  ADC_V_LOWER_ = ros::param::param<float>("~adc_lower_v", 0.0);

  RESIS_DIV_THERM_VOLT_ = ros::param::param<float>("~resis_div_therm_volt", 5.0);
  RESIS_DIV_THERM_UPPER_ = ros::param::param<float>("~resis_div_therm_upper", 24.3e3);
  // TODO: Thermistor calibration constants

  RESIS_DIV_48_UPPER_ = ros::param::param<float>("~resis_div_48_upper", 1.0e6);
  RESIS_DIV_48_LOWER_ = ros::param::param<float>("~resis_div_48_lower", 80.6e3);

  RESIS_DIV_12p5_UPPER_ = ros::param::param<float>("~resis_div_12p5_upper", 402e3);
  RESIS_DIV_12p5_LOWER_ = ros::param::param<float>("~resis_div_12p5_lower", 100e3);

  RESIS_DIV_6p5_UPPER_ = ros::param::param<float>("~resis_div_6p5_upper", 49.9e3);
  RESIS_DIV_6p5_LOWER_ = ros::param::param<float>("~resis_div_6p5_lower", 100e3);

  GOOD_LOCATION_COMMAND_LOCKOUT_ = ros::param::param<int>("~command_lockout_count", 10);
}

void SailServo::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 500;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // start with speed.  First command is most likely to get garbled, so... yeah.
  // Query Speed
  cmd.command = "#" + address_ + "?M77 1\r";
  sm_cmd.request.commands.push_back(cmd);

  // query location
  cmd.command = "#" + address_ + "?L";
  sm_cmd.request.commands.push_back(cmd);

  // Query last index crossing
  cmd.command = "#" + address_ + "?I";
  sm_cmd.request.commands.push_back(cmd);

  // query our voltages
  cmd.command = "#" + address_ + "?V";
  sm_cmd.request.commands.push_back(cmd);

  // Query PORTA for centering switch
  cmd.command = "#" + address_ + "?M0 1\r";
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().call(sm_cmd))
  {
    sail_cmd_list_ = sm_cmd.response.retval;
    ROS_INFO_STREAM("Added " << sail_cmd_list_.size() << " initial SAIL commands");
  }
  else
  {
    ROS_ERROR_STREAM("Error calling I/O State Machine config service");
  }

  // set our speed
  _send_preempt("#" + address_ + "!P" + speed_ + ".0000");
}

void SailServo::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();

  // prepare our publishers
  auto nh = nodeHandle();
  servo_pub_ = nh.advertise<ds_actuator_msgs::ServoState>(ros::this_node::getName() + "/state", 10);
  sentry_servo_pub_ = nh.advertise<SentryServoState>(ros::this_node::getName() + "/sentry_servo_state", 10);
  joint_states_pub_ = nh.advertise<sensor_msgs::JointState>("joint_states", 10);

  // prepare to listen for commands
  servo_pos_cmd_ = nh.subscribe(ros::this_node::getName() + "/cmd", 10, &SailServo::_command_req, this);

  // prepare to listen for abort status to properly handle the enable flag
  abort_topic_sub_ = nh.subscribe(abort_topic_name_, 10, &SailServo::_set_enable, this);

  // prepare to a service for indexing, sleeping, etc
  servo_ctrl_cmd_ = nh.advertiseService<sentry_msgs::SailServoCmd::Request, sentry_msgs::SailServoCmd::Response>(
      ros::this_node::getName() + "/ctrl_cmd", boost::bind(&SailServo::_ctrl_cmd, this, _1, _2));
}

void SailServo::checkProcessStatus(const ros::TimerEvent& event)
{
  ds_core_msgs::Status status = _checkStatus();
  publishStatus(status);
}

void SailServo::_set_enable(const ds_core_msgs::Abort& msg)
{
  if (msg.enable && !enable)
  {
    // we're re-enabling, so add a location command and go to 0
    add_loc_cmd();
  }
  enable = msg.enable;
  if (!enable)
  {
    // we're disabling, so IMMEDIATELY stop
    stop();
  }
}

ds_core_msgs::Status SailServo::_checkStatus()
{
  ros::Time now = ros::Time::now();

  ds_core_msgs::Status status;
  status.descriptive_name = descriptiveName();
  status.ds_header.io_time = now;

  ros::Duration location_age = now - last_location_time_;

  if (MessageTimeout() > ros::Duration(0) && location_age > MessageTimeout())
  {
    // A timeout is set and we haven't heard from the servo in a while.  Not good.
    status.status = status.STATUS_ERROR;
  }
  else if (enable && location_good && sail_loc_cmd_id_ != 0)
  {
    // servo is enabled and we have good location data;
    // and are sending commands to the servo!
    // ready to accept new commands!
    status.status = status.STATUS_GOOD;
  }
  else
  {
    // servo is talking, but either it's disabled or needs indexing.  So status=WARN
    status.status = status.STATUS_WARN;
  }

  return status;
}

void SailServo::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  if (_parseReceivedInner(bytes))
  {
    // publish!
    //        ROS_ERROR_STREAM("SERVO \"" <<d->address_ <<"\""
    //                                    << " LOC[" << (d->location_good ? "GOOD" : "BAD")
    //                                    <<"]: " <<d->location_rad_raw*180.0/M_PI
    //                                    <<" IDX: " <<d->location_rad_raw*180.0/M_PI);

    // publish servo states
    const auto servo_msg = buildServoMsg();
    auto sentry_servo_msg = SentryServoState{};

    sentry_servo_msg.servo_state = servo_msg;
    sentry_servo_msg.mem_speed = speed_query_;
    sentry_servo_msg.mem_switch = switch_query_;

    servo_pub_.publish(servo_msg);
    sentry_servo_pub_.publish(sentry_servo_msg);

    // only publish joint_states if info is good
    if (location_good)
    {
      joint_states_pub_.publish(buildJointStateMsg());
    }
  }
}

bool SailServo::_parseReceivedInner(const ds_core_msgs::RawData& bytes)
{
  std::string recv_msg(bytes.data.begin(), bytes.data.end());

  // Find the start of the string
  // we're looking for the first ETX after any #, and the last # before that ETX
  size_t hash_idx = recv_msg.find('#');
  size_t etx_idx = recv_msg.find('\x03', hash_idx);

  // Also check backwards for the last # before the ETX.  That lets us avoid any junk
  // from another sail device that isn't talking.
  size_t hash_idx_rfind = recv_msg.rfind('#', etx_idx);
  hash_idx = hash_idx_rfind;

  if (hash_idx == std::string::npos || etx_idx == std::string::npos)
  {
    ROS_ERROR_STREAM("Unable to find start & end of message! Message=\"" << recv_msg << "\"");
    return false;
  }

  if (address_ != recv_msg.substr(hash_idx + 1, address_.length()))
  {
    ROS_ERROR_STREAM("Address mismatch.  \"" << address_ << "\" != \""
                                             << recv_msg.substr(hash_idx + 1, address_.length()) << "\"");
    return false;
  }

  size_t payload_idx = hash_idx + 1 + address_.length();
  if (etx_idx - payload_idx < 4)
  {
    ROS_ERROR_STREAM("Payload too short to include prompt, aborting parse of message=\"" << recv_msg << "\"");
    return false;
  }
  std::string payload = recv_msg.substr(payload_idx, etx_idx - payload_idx - 3);
  if (recv_msg.substr(etx_idx - 3, 2) != "\r\n")
  {
    ROS_ERROR_STREAM("Terminator not correct, aborting parse of message=\"" << recv_msg << "\"");
    return false;
  }

  char prompt = recv_msg[etx_idx - 1];

  if (payload.empty())
  {
    ROS_ERROR_STREAM("Empty SAIL servo payload string!");
    return false;
  }

  // check to see if the prompt says we're invalid (we'll check valid later..."
  if ((prompt == '*' || prompt == 'c'))
  {
    if (location_good)
    {
      remove_loc_cmd();
    }
    location_good = false;
  }

  bool ret = false;
  if (payload[0] == '?')
  {
    try
    {
      ret = _parseQueryReply(payload, bytes.ds_header.io_time);
    }
    catch (const std::exception& e)
    {
      ROS_ERROR_STREAM("Unable to parse message payload \"" << payload << "\" because of exception \"" << e.what()
                                                            << "\"");
    }
    catch (...)
    {
      ROS_ERROR_STREAM("Unable to parse message payload \"" << payload << "\" because of exception\"Unknown "
                                                                          "Exception\"");
    }

    // if this is a query command, and the servo reports it's indexed, then
    // possibly add a location
    if (ret && payload[1] == 'L' && prompt == ':')
    {
      // we need noticable delay between when the servo is done indexing and
      // when we flag the location as good.  This allows the deck team time to notice
      // that the servo has stopped during indexing.  It also prevents
      // the 1-2 bad locations reported RIGHT after indexing from being used to
      // set the initial position.  For that reason, we use a counter to ignore the
      // first GOOD_LOCATION_COMMAND_LOCKOUT_ good locations
      if (locations_good_required > 0)
      {
        locations_good_required--;
        ROS_ERROR_STREAM("SERVO " << address_ << " locations_good_required=" << locations_good_required);
      }
      else
      {
        if (!location_good)
        {
          add_loc_cmd();
        }
        location_good = true;
      }
    }
  }

  return ret;
}

bool SailServo::_parseQueryReply(const std::string& payload, const ros::Time& stamp)
{
  if (payload.length() < 2)
  {
    ROS_ERROR_STREAM("SAIL servo query payload too short, aborting");
    return false;
  }

  size_t idx;
  std::string msg;
  msg = payload.substr(2, std::string::npos);
  if (msg.empty())
  {
    ROS_ERROR_STREAM("Unable to parse empty message...");
    return false;
  }

  switch (payload[1])
  {
    case 'L':
      location_rad_raw = counts_to_radians(std::stoul(msg, &idx, 16));
      location_rad_corrected = correct_servo_pos(location_rad_raw);
      ROS_INFO_STREAM("Location radians: " << location_rad_raw << " -> " << location_rad_corrected);
      last_location_time_ = stamp;
      return true;
    // no break needed

    case 'I':
      index_rad_raw = counts_to_radians(stoul(msg, &idx, 16));
      ROS_INFO_STREAM("Index radians: " << index_rad_raw);
      break;

    case 'V':
      int32_t raw;
      int32_t raw1, raw2, raw3, raw4;

      // first field
      raw = stoul(msg, &idx, 16);
      msg = msg.substr(idx, std::string::npos);
      raw1 = raw;
      voltage_thermistor = a2d_to_therm(raw);  // custom function

      // second field
      raw = stoul(msg, &idx, 16);
      msg = msg.substr(idx, std::string::npos);
      raw2 = raw;
      voltage_bus_48v = a2d_to_voltage(raw, RESIS_DIV_48_UPPER_, RESIS_DIV_48_LOWER_);

      // third field
      raw = stoul(msg, &idx, 16);
      msg = msg.substr(idx, std::string::npos);
      raw3 = raw;
      idx++;
      voltage_bus_12p5v = a2d_to_voltage(raw, RESIS_DIV_12p5_UPPER_, RESIS_DIV_12p5_LOWER_);

      // fourth field
      raw = stoul(msg, &idx, 16);
      raw4 = raw;
      voltage_bus_6p5v = a2d_to_voltage(raw, RESIS_DIV_6p5_UPPER_, RESIS_DIV_6p5_LOWER_);

      ROS_INFO_STREAM("Voltages: " << raw1 << " " << raw2 << " " << raw3 << " " << raw4);
      ROS_INFO_STREAM("Voltages: Therm: " << voltage_thermistor << " 48v: " << voltage_bus_48v
                                          << " 12.5v: " << voltage_bus_12p5v << " 6.5v: " << voltage_bus_6p5v);
      break;

    case 'M':
    {
      const auto addr = std::stoul(msg, &idx, 16);
      if (addr == 0x0)
      {
        uint8_t value;
        if (std::sscanf(msg.data(), "0 1\r\r\n0000 %hhX", &value) != 1)
        {
          ROS_WARN_STREAM("Unable to parse switch memory query");
          return false;
        }

        switch_query_ = value;
        return true;
      }

      if (addr == 0x77)
      {
        uint8_t value;
        if (std::sscanf(msg.data(), "77 1\r\r\n%*X %hhX", &value) != 1)
        {
          ROS_WARN_STREAM("Unable to parse speed memory query");
          return false;
        }

        speed_query_ = value;
        return true;
      }
      ROS_INFO_STREAM("Unhandled ?M response: '" << msg << "'");
      break;
    }

    default:
      ROS_ERROR_STREAM("Unknown query response for SAIL SERVO, payload=\"" << msg << "\"");
      return false;
  }

  return false;
}

float SailServo::counts_to_radians(uint32_t raw)
{
  int32_t signed_raw = raw;
  if (signed_raw > 0x7fff)
  {
    signed_raw -= 0xfffe;
  }

  return static_cast<float>(signed_raw) * SERVO_COUNTS_TO_RADIANS_;
}

float SailServo::a2d_to_Vmeas(uint32_t raw)
{
  if (raw > 0xff)
  {
    return ADC_V_UPPER_;
  }
  else
  {
    return (ADC_V_UPPER_ - ADC_V_LOWER_) * static_cast<float>(raw) / 255.0f + ADC_V_LOWER_;
  }
}

float SailServo::a2d_to_voltage(uint32_t raw, float Rupper, float Rlower)
{
  // This assumes the circuit looks like:
  //
  //               /----<Vin
  //               |
  //               <
  //               >  Rupper
  //               <
  //               |
  //   Vmeas>------+
  //               |
  //               <
  //               >  Rlower
  //               <
  //               |
  //              ---
  //               -
  //              GND
  //
  // Given that:
  // Vmeas = Vin * Rlower / (Rupper + Rlower);
  // Rearrange to find:
  // Vin = Vmeas * (Rupper + Rlower) / Rlower
  //
  float Vmeas = a2d_to_Vmeas(raw);

  return Vmeas * (Rupper + Rlower) / Rlower;
}

float SailServo::a2d_to_therm(uint32_t raw)
{
  float Vmeas = a2d_to_Vmeas(raw);

  if (raw == 0xff)
  {
    return std::numeric_limits<float>::infinity();
  }

  float Rtherm = Vmeas * RESIS_DIV_THERM_UPPER_ / (RESIS_DIV_THERM_UPPER_ - Vmeas);

  // TODO: Convert Rtherm to a real temperature

  return Rtherm;
}

float SailServo::correct_servo_pos(float raw)
{
  return servo_scale * (raw - servo_offset);
}

float SailServo::uncorrect_servo_pos(float corr)
{
  return (corr / servo_scale) + servo_offset;
}

//------------------------------------------------------------
// Private inner stuff
bool SailServo::_ctrl_cmd(const sentry_msgs::SailServoCmd::Request& req,
                          const sentry_msgs::SailServoCmd::Response& resp)
{
  if (!enable)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " got a command, but it is not enabled!");
    return false;
  }

  std::string cmdstr = "#" + address_;
  switch (req.command)
  {
    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_INDEX:
      cmdstr += "I";
      locations_good_required = GOOD_LOCATION_COMMAND_LOCKOUT_;
      location_good = false;
      remove_loc_cmd();
      break;

    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_BRAKE:
      cmdstr += "B";
      break;

    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_COAST:
      cmdstr += "C";
      // otherwise this overides the coast command and keeps things stopped
      remove_loc_cmd();
      break;

    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_SLEEP:
      cmdstr += "Z";
      location_good = false;
      remove_loc_cmd();
      // TODO: Kill all our commands, add a flag for up&down to re-enable
      break;

    default:
      ROS_ERROR_STREAM("Request for unknown servo command " << req.command);
      return false;
  }

  _send_preempt(cmdstr);
  return true;
}

void SailServo::_command_req(const ds_actuator_msgs::ServoCmd& msg)
{
  if (!enable)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " got a command, but it is not enabled!");
    return;
  }
  if (!location_good)
  {
    ROS_WARN_STREAM("Servo joint name=\"" << joint_name_ << "\" needs indexing");
    return;
  }
  location_cmd = msg.cmd_radians;

  _update_loc("#" + address_ + "!L" + _make_loc_string(location_cmd) + "\r");
}

std::string SailServo::_make_loc_string(float cmd_radians)
{
  float cmd_counts = uncorrect_servo_pos(cmd_radians) / SERVO_COUNTS_TO_RADIANS_;
  int16_t toSend = roundf(cmd_counts);

  char buf[5];
  std::snprintf(buf, 5, "%04hX", toSend);

  return std::string(buf, 4);
}

void SailServo::stop()
{
  // There are two versions of stop;
  // this one sets the position to the last reported value and keeps it there
  // char buf[5];
  // int16_t toSend = roundf(location_rad_raw / SERVO_COUNTS_TO_RADIANS_);
  //
  // std::snprintf(buf, 5, "%04hX", toSend);
  // std::string cmd = "#" + address_ + "!L" + std::string(buf, 4) + "\r";
  //
  //_update_loc(cmd);

  // This version sets the servo to "coast" mode so it retains indexing
  // but won't actually actuate the motor
  remove_loc_cmd();
  _send_preempt("#" + address_ + "C");
}

void SailServo::_send_preempt(const std::string& cmdstr)
{
  ds_core_msgs::IoCommandList cmdList;
  ds_core_msgs::IoCommand cmd;
  cmd.command = cmdstr;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 500;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  cmdList.cmds.push_back(cmd);

  PreemptCmd().publish(cmdList);
}

void SailServo::_update_loc(const std::string& cmdstr)
{
  if (sail_loc_cmd_id_ == 0)
  {
    ROS_ERROR_STREAM("ERROR: No servo location SAIL command! Servo location not set.");
    return;
  }

  ds_core_msgs::IoCommand cmd;
  cmd.command = cmdstr;
  cmd.id = sail_loc_cmd_id_;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 500;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  ds_core_msgs::IoCommandList cmdlist;
  cmdlist.cmds.push_back(cmd);

  UpdateCmd().publish(cmdlist);
}

void SailServo::add_loc_cmd()
{
  ROS_INFO_STREAM("Adding location command...");
  if (sail_loc_cmd_id_ != 0)
  {
    ROS_WARN_STREAM("Already have a servo location command, but asked to add one.  IGNORING!");
    return;
  }

  // set to current location
  location_cmd = location_rad_corrected;
  // Re-send our speed command, just to be sure
  _send_preempt("#" + address_ + "!P" + speed_ + "." + _make_loc_string(location_cmd));

  // ALWAYS tell it to go to 0
  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 500;
  cmd.delayAfter_ms = 0;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;
  cmd.command = "#" + address_ + "!L" + _make_loc_string(location_cmd) + "\r";
  sm_cmd.request.commands.push_back(cmd);
  if (IosmCmd().call(sm_cmd))
  {
    sail_cmd_list_.push_back(sm_cmd.response.retval[0]);
    sail_loc_cmd_id_ = sm_cmd.response.retval[0];
    ROS_INFO_STREAM("Successfully added SAIL command for location id=" << sail_loc_cmd_id_);
  }
  else
  {
    ROS_ERROR_STREAM("Error calling I/O State Machine config service");
  }
  publishStatus(_checkStatus());
}

void SailServo::remove_loc_cmd()
{
  if (sail_loc_cmd_id_ == 0)
  {
    // no command to remove!
    return;
  }
  ROS_INFO_STREAM("Removing location command...");
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_REMOVE_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.id = sail_loc_cmd_id_;
  sm_cmd.request.commands.push_back(cmd);
  if (IosmCmd().call(sm_cmd))
  {
    ROS_INFO_STREAM("Successfully removed SAIL command ID=" << sail_loc_cmd_id_);
    sail_loc_cmd_id_ = 0;
  }
  else
  {
    ROS_ERROR_STREAM("Error calling I/O State Machine config service");
  }
  publishStatus(_checkStatus());
}

ds_actuator_msgs::ServoState SailServo::buildServoMsg() const
{
  ds_actuator_msgs::ServoState ret;

  ret.header.stamp = ros::Time::now();
  ret.ds_header.io_time = ret.header.stamp;
  std::copy(std::begin(uuid_), std::end(uuid_), std::begin(ret.ds_header.source_uuid));

  ret.cmd_radians = location_cmd;
  ret.servo_name = joint_name_;
  ret.enable = enable;
  ret.actual_radians = location_rad_corrected;

  return ret;
}
sensor_msgs::JointState SailServo::buildJointStateMsg() const
{
  sensor_msgs::JointState ret;

  ret.header.stamp = ros::Time::now();

  ret.name.push_back(joint_name_);
  ret.position.push_back(location_rad_corrected);

  return ret;
}
