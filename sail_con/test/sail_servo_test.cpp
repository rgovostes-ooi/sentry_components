/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/29/18.
//

#include <gtest/gtest.h>

#include <sail_con/sail_servo.h>

// dummy test values
const float NODATA = -9999.9;

const float S_SCALE=3.0;
const float S_OFFSET=4.0;
const float S2RAD=2.3;

class SailServoUnderTest : public sail_con::SailServo {
 public:

  virtual void _init_test() {
    // DO NOT call superclass version (FAILS to set us up correctly!)(!)
    SERVO_COUNTS_TO_RADIANS_ = S2RAD; // DUMMY value
    servo_offset = 4;
    servo_scale = 3.0;
    address_ = "S1";
    joint_name_ = "servo_test";

    location_good = false;
    location_cmd = NODATA;
    location_rad_corrected = NODATA;
    location_rad_raw = NODATA;
    index_rad_raw = NODATA;

    voltage_thermistor = NODATA;
    voltage_bus_48v = NODATA;
    voltage_bus_12p5v = NODATA;
    voltage_bus_6p5v = NODATA;
  }

  bool _parseReceivedInner(const ds_core_msgs::RawData &bytes) {
    return sail_con::SailServo::_parseReceivedInner(bytes);
  }

  bool locationGood() const {
    return location_good;
  }

  float locationCmd() const {
    return location_cmd;
  }

  float locationRadCorrected() const {
    return location_rad_corrected;
  }

  float locationRadRaw() const {
    return location_rad_raw;
  }

  float indexRadRaw() const {
    return index_rad_raw;
  }
};

class sail_servo_test : public ::testing::Test {

 protected:
  virtual void SetUp() {

    // initialize ROS
    ros::Time::init();

    // run our variable setup
    underTest._init_test();

  }

  // Helper function to build a raw data message from a string
  ds_core_msgs::RawData buildRawMsg(const std::string& msg) {
    ds_core_msgs::RawData toSend;
    toSend.header.stamp = ros::Time::now();
    toSend.ds_header.io_time = toSend.header.stamp;
    toSend.data_direction = ds_core_msgs::RawData::DATA_IN;
    toSend.data = std::vector<uint8_t>(msg.begin(), msg.end());
    return toSend;
  }

  // a list of data fired from callbacks
  SailServoUnderTest underTest;

  //virtual void TearDown() {}
};

TEST_F(sail_servo_test, parse_location_empty) {
  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r\n*\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(0, underTest.locationRadRaw());
  EXPECT_EQ((-S_OFFSET * S_SCALE), underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, parse_location_positive) {
  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L000A\r\n*\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.locationCmd());
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(S2RAD * 10, underTest.locationRadRaw());
  EXPECT_EQ((S2RAD * 10 - S_OFFSET) * S_SCALE, underTest.locationRadCorrected());
  EXPECT_FALSE(underTest.locationGood());
}

TEST_F(sail_servo_test, parse_location_negative) {
  ds_core_msgs::RawData bytes = buildRawMsg("#S1?LFFF4\r\n*\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.locationCmd());
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(S2RAD*-10, underTest.locationRadRaw());
  EXPECT_EQ((S2RAD*-10 - S_OFFSET)*S_SCALE, underTest.locationRadCorrected());
  EXPECT_FALSE(underTest.locationGood());
}

TEST_F(sail_servo_test, reject_location_cmd) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1!L0000\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_noterm) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1!L0000\r\n*");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_nonewline) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_prompt) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r\n\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_nocr) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_diff_addr) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S2?L0000\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_payload) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_junk) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?LGM!\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, fuzz) {

  ds_core_msgs::RawData bytes = buildRawMsg("DFHGHUIOHA\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, good_index) {

  EXPECT_TRUE(!underTest.locationGood());

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r\n:\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(0, underTest.locationRadRaw());
  EXPECT_EQ((-S_OFFSET * S_SCALE), underTest.locationRadCorrected());
  EXPECT_TRUE(underTest.locationGood());
}

TEST_F(sail_servo_test, long_command) {

  EXPECT_TRUE(!underTest.locationGood());

  ds_core_msgs::RawData bytes = buildRawMsg("#S2?L#S2?I#S2?V#S2?M0 1\r#S2?M77 1\r#S1?L0000\r\n:\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(0, underTest.locationRadRaw());
  EXPECT_EQ((-S_OFFSET * S_SCALE), underTest.locationRadCorrected());
  EXPECT_TRUE(underTest.locationGood());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}