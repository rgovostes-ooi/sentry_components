/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// Created by llindzey on 2 October 2018

#ifndef SENTRY_COMPONENTS_DECKBOX_DECKBOX_H
#define SENTRY_COMPONENTS_DECKBOX_DECKBOX_H

// QUESTION(LEL): ds_asio gets included by ds_process, so this isn't required
//     for compilation. However, is it good form to explicitly include it,
//     since we directly use it?
#include <ds_asio/ds_asio.h>
#include <ds_base/ds_process.h>
#include <ds_core_msgs/RawData.h>
#include <sentry_msgs/BurnwiresMeasurement.h>
#include <sentry_msgs/DeckboxRelayCmd.h>
#include <sentry_msgs/DeckboxStatus.h>
#include <sentry_msgs/SampleBurnwiresCmd.h>

#include <regex>

namespace deckbox {

class Deckbox : public ds_base::DsProcess {
public:
  Deckbox();
  Deckbox(int argc, char *argv[], const std::string &name);
  ~Deckbox() override; // QUESTION(LEL): Why is the destructor overridden?

private:
  void setupConnections() override;
  void setupParameters() override;
  void setupPublishers() override;
  void setupServices() override;

  bool ethernetRelayCallback(const sentry_msgs::DeckboxRelayCmd::Request &req,
                             sentry_msgs::DeckboxRelayCmd::Response &resp);
  bool powerRelayCallback(const sentry_msgs::DeckboxRelayCmd::Request &req,
                          sentry_msgs::DeckboxRelayCmd::Response &resp);
  bool
  sampleBurnwiresCallback(const sentry_msgs::SampleBurnwiresCmd::Request &req,
                          sentry_msgs::SampleBurnwiresCmd::Response &resp);

  void statusConnectionCallback(const ds_core_msgs::RawData &bytes);
  void burnwiresConnectionCallback(const ds_core_msgs::RawData &bytes);
  void powerConnectionCallback(const ds_core_msgs::RawData &bytes);
  void ethernetConnectionCallback(const ds_core_msgs::RawData &bytes);

  ros::Publisher status_publisher_;
  ros::Publisher burnwire_publisher_;

  ros::ServiceServer burnwire_server_;
  ros::ServiceServer power_server_;
  ros::ServiceServer ethernet_server_;

  // TODO(LEL): I think this was only a shared_ptr due to the pImpl idiom,
  //     and we shouldn't need it here, right? The object should be fine...
  boost::shared_ptr<ds_asio::IoSM> iosm_;

  // Timeouts for how long IOSM will wait for a UDP reply
  float power_timeout_;
  float ethernet_timeout_;
  float burnwire_timeout_;
  float status_timeout_;

  // Rate at which to poll the deckbox for status updates
  float hz_;
};

} // namespace deckbox

#endif // SENTRY_COMPONENTS_DECKBOX_DECKBOX_H
