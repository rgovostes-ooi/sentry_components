#! /usr/bin/env python2.7

# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


# Mock deckbox, providing an interface that will make it possible to test
# integration with SentrySitter and the deckbox node.

# Created by llindzey on 2 October 2018.

import argparse
import numpy.random
import socket
import time

class MockDeckbox(object):
    def __init__(self, rx_port, tx_port):
        self.rx_port = rx_port
        self.tx_port = tx_port

        # Whether the power relay is currently on or off (on -> relay closed).
        self.power_on = False
        # Whether the ethernet relay is currently on or off (on -> relay closed).
        # Sean says that the deckbox defaults to ethernet connected.
        self.ethernet_on = True

        # Deckbox reports ambient temperature in C.
        # Will be updated in random walk with every status call
        self.temperature = 20.0

        self.deckbox_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.deckbox_server.bind(('', self.rx_port))

    def run(self):
        while True:
            message, rx_address = self.deckbox_server.recvfrom(1024)
            message = message.strip()
            print "got %s from %r!" % (message, rx_address)
            tx_address = (rx_address[0], self.tx_port)
            print "and will reply to %r" % (tx_address,)
            if message == "#POWER ON":
                time.sleep(2.0) # 2 sec delay for "soft start"
                self.power_on = True
                reply_message = "\n\n!POWER ON\r"
                self.deckbox_server.sendto(reply_message, tx_address)
            elif message == "#POWER OFF":
                self.power_on = False
                reply_message = "\n\n!POWER OFF\r"
                self.deckbox_server.sendto(reply_message, tx_address)
            elif message == "#POWER STATE":
                if self.power_on:
                    reply_message = "\n\n!ON\r"
                else:
                    reply_message = "\n\n!OFF\r"
                self.deckbox_server.sendto(reply_message, tx_address)
            elif message == "#CONNECT ETH":
                self.ethernet_on = True
                reply_message = "\n\n!CONNECT ETH\r"
                self.deckbox_server.sendto(reply_message, tx_address)
            elif message == "#DISCONNECT ETH":
                self.ethernet_on = False
                reply_message = "\n\n!DISCONNECT ETH\r"
                self.deckbox_server.sendto(reply_message, tx_address)

            elif message == "#SAMPLE BURNWIRES":
                time.sleep(4.0) # 4 sec delay to actually take readings

                # These hard-coded parameters are assuming the burnwires are
                # burning. No good way for this mock to know whether they're
                # active or snuffed.
                v_mean = 5 # mean voltage (V)
                v_stddev = 1.0
                v1 = numpy.random.normal(v_mean, v_stddev)
                v2 = numpy.random.normal(v_mean, v_stddev)
                v3 = numpy.random.normal(v_mean, v_stddev)

                a_mean = 120 # mean current (mA)
                a_stddev = 20
                a1 = numpy.random.normal(a_mean, a_stddev)
                a2 = numpy.random.normal(a_mean, a_stddev)
                a3 = numpy.random.normal(a_mean, a_stddev)

                reply_message = ("\n\n!BW %0.1f %0.3f %0.1f %0.3f %0.1f %0.3f\r"
                                 % (v1, a1, v2, a2, v3, a3))
                self.deckbox_server.sendto(reply_message, tx_address)

            elif message == "#STATUS":
                temperature_step = 0.1
                self.temperature += temperature_step * numpy.random.choice([-1, 1])
                reply_message = "\n\n!STATUS {} {} {}\r".format(
                    int(self.power_on), int(self.ethernet_on), round(self.temperature, 1))
                self.deckbox_server.sendto(reply_message, tx_address)

            else:
                print "Unrecognized message:", message


def main(args):
    mock_deckbox = MockDeckbox(args.rx_port, args.tx_port)
    mock_deckbox.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("rx_port", type=int, help="port server is listening on")
    parser.add_argument("tx_port", type=int, help="port server will reply to")

    args = parser.parse_args()
    main(args)
