/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// Created by llindzey on 2 October 2018

#include <deckbox/deckbox.h>
// Node for communicating with the deckbox over UDP.
// NOTE: This doesn't prevent the user from doing something stupid,
//       like requesting 20 burnwire samples in quick succession.
//       The requests would just accumulate in ds_asio's preempt buffer,
//       and run in sequence.

using namespace deckbox;

Deckbox::Deckbox() : ds_base::DsProcess() {
  // do nothing
}

Deckbox::Deckbox(int argc, char *argv[], const std::string &name)
    : ds_base::DsProcess(argc, argv, name) {
  // do nothing
}

Deckbox::~Deckbox() = default;

void Deckbox::setupConnections() {
  ds_base::DsProcess::setupConnections();

  // The 2nd argument here needs to match the ns in the launch file.
  iosm_ = addIoSM("statemachine", "instrument");

  auto callback = boost::bind(&Deckbox::statusConnectionCallback, this, _1);
  iosm_->addRegularCommand(ds_asio::IoCommand("#STATUS\r\n", status_timeout_, false, callback));

  float timeout_seconds = 1.0 / hz_;
  iosm_->addRegularCommand(ds_asio::IoCommand(timeout_seconds));
}

void Deckbox::setupParameters() {
  ds_base::DsProcess::setupParameters();

  power_timeout_ = ros::param::param<float>("~power_timeout", 5.0);
  ethernet_timeout_ = ros::param::param<float>("~ethernet_timeout", 3.0);
  burnwire_timeout_ = ros::param::param<float>("~burnwire_timeout", 7.0);
  status_timeout_ = ros::param::param<float>("~status_timeout", 1.0);
  hz_ = ros::param::param<float>("~hz", 0.5);

}

void Deckbox::setupPublishers() {
  ds_base::DsProcess::setupPublishers(); // status & critical process publishers

  auto nh = nodeHandle();
  // Can't use "status" as the topic because that's where ds_core_msgs::Status
  // is published. (I don't understand why that isn't under the instrument ns.)
  std::string status_topic = ros::this_node::getName() + "/state";
  status_publisher_ =
      nh.advertise<sentry_msgs::DeckboxStatus>(status_topic, 10);

  std::string burnwire_topic = ros::this_node::getName() + "/burnwires";
  burnwire_publisher_ =
      nh.advertise<sentry_msgs::BurnwiresMeasurement>(burnwire_topic, 10);
}

void Deckbox::setupServices() {
  ds_base::DsProcess::setupServices();

  auto nh = nodeHandle();
  std::string node_name = ros::this_node::getName();
  std::string burnwire_service = node_name + "/sample_burnwires";
  burnwire_server_ =
      nh.advertiseService<sentry_msgs::SampleBurnwiresCmd::Request,
                          sentry_msgs::SampleBurnwiresCmd::Response>(
          burnwire_service,
          boost::bind(&Deckbox::sampleBurnwiresCallback, this, _1, _2));

  std::string power_service = node_name + "/set_power";
  power_server_ = nh.advertiseService<sentry_msgs::DeckboxRelayCmd::Request,
                                      sentry_msgs::DeckboxRelayCmd::Response>(
      power_service, boost::bind(&Deckbox::powerRelayCallback, this, _1, _2));

  std::string ethernet_service = node_name + "/set_ethernet";
  ethernet_server_ =
      nh.advertiseService<sentry_msgs::DeckboxRelayCmd::Request,
                          sentry_msgs::DeckboxRelayCmd::Response>(
          ethernet_service,
          boost::bind(&Deckbox::ethernetRelayCallback, this, _1, _2));
}

bool Deckbox::sampleBurnwiresCallback(
    const sentry_msgs::SampleBurnwiresCmd::Request &req,
    sentry_msgs::SampleBurnwiresCmd::Response &resp) {

  auto callback = boost::bind(&Deckbox::burnwiresConnectionCallback, this, _1);
  auto sample_command =
      ds_asio::IoCommand("#SAMPLE BURNWIRES\r\n", burnwire_timeout_, false, callback);
  // TODO(LEL): It looks like this doesn't actually do anything? I've emailed Ian...
  sample_command.setWarnOnTimeout(true);
  iosm_->addPreemptCommand(sample_command);

  // This doesn't try to reject commands at all.
  // It might be nice to filter based on the length of the preempt queue in
  // ds_asio, but that's not currently visible.
  resp.accepted = true;
  return true;
}

bool Deckbox::powerRelayCallback(
    const sentry_msgs::DeckboxRelayCmd::Request &req,
    sentry_msgs::DeckboxRelayCmd::Response &resp) {
  auto callback = boost::bind(&Deckbox::powerConnectionCallback, this, _1);
  std::string command_string;
  if (req.command == req.ON) {
    command_string = "#POWER ON\r\n";
  } else {
    command_string = "#POWER OFF\r\n";
  }
  auto power_command = ds_asio::IoCommand(command_string, power_timeout_, false, callback);
  power_command.setWarnOnTimeout(true);
  iosm_->addPreemptCommand(power_command);

  resp.accepted = true;
  return true;
}

bool Deckbox::ethernetRelayCallback(
    const sentry_msgs::DeckboxRelayCmd::Request &req,
    sentry_msgs::DeckboxRelayCmd::Response &resp) {
  auto callback = boost::bind(&Deckbox::ethernetConnectionCallback, this, _1);
  std::string command_string;
  if (req.command == req.ON) {
    command_string = "#CONNECT ETH\r\n";
  } else {
    command_string = "#DISCONNECT ETH\r\n";
  }
  auto ethernet_command = ds_asio::IoCommand(command_string, ethernet_timeout_, false, callback);
  ethernet_command.setWarnOnTimeout(true);
  iosm_->addPreemptCommand(ethernet_command);

  resp.accepted = true;
  return true;
}

void Deckbox::statusConnectionCallback(const ds_core_msgs::RawData &bytes) {
  std::string status_message(reinterpret_cast<const char *>(bytes.data.data()),
                             bytes.data.size() - 1);
  // Format of returned status will be: `!STATUS 1 0 24.9`, where the arguments
  // are: bool(power on), bool(ethernet on), float (temperature).
  std::regex status_regex{"!STATUS\\s([0,1])\\s([0,1])\\s(-?[0-9]*\\.?[0-9]*)"};
  std::smatch status_match;
  if (!std::regex_search(status_message, status_match, status_regex)) {
    ROS_ERROR_STREAM("Deckbox status regex didn't match: " << status_message);
    return;
  } else {
    ROS_INFO_STREAM("Received deckbox status response: " << status_message);
  }

  sentry_msgs::DeckboxStatus deckbox_status;
  deckbox_status.ds_header = bytes.ds_header;
  deckbox_status.header.stamp = bytes.ds_header.io_time;
  deckbox_status.power_on = bool(std::stoi(status_match[1]));
  deckbox_status.ethernet_on = bool(std::stoi(status_match[2]));
  deckbox_status.temperature = std::stof(status_match[3]);
  status_publisher_.publish(deckbox_status);
}

void Deckbox::burnwiresConnectionCallback(const ds_core_msgs::RawData &bytes) {
  std::string measurement(reinterpret_cast<const char *>(bytes.data.data()),
                          bytes.data.size() - 1);
  // Format of measurement is: `!BW 0.0 0.000 0.0 0.000 0.0 0.000`, where the
  // fields are: port_v, port_a, desc_v, desc_a, stbd_v, stbd_a
  std::regex burnwires_regex{"!BW\\s(-?[0-9]*\\.?[0-9]*)\\s(-?[0-9]*\\.?[0-9]*)"
                             "\\s(-?[0-9]*\\.?[0-9]*)\\s(-?[0-9]*\\.?[0-9]*)"
                             "\\s(-?[0-9]*\\.?[0-9]*)\\s(-?[0-9]*\\.?[0-9]*)"};
  std::smatch burnwires_match;
  if (!std::regex_search(measurement, burnwires_match, burnwires_regex)) {
    ROS_ERROR_STREAM("Burnwire regex didn't match: " << measurement);
    return;
  } else {
    ROS_INFO_STREAM("Received burnwire response: " << measurement);
  }

  sentry_msgs::BurnwiresMeasurement burnwire_message;
  burnwire_message.ds_header = bytes.ds_header;
  burnwire_message.header.stamp = bytes.ds_header.io_time;
  burnwire_message.port_voltage = std::stof(burnwires_match[1]);
  burnwire_message.port_current = std::stof(burnwires_match[2]);
  burnwire_message.descent_voltage = std::stof(burnwires_match[3]);
  burnwire_message.descent_current = std::stof(burnwires_match[4]);
  burnwire_message.starboard_voltage = std::stof(burnwires_match[5]);
  burnwire_message.starboard_current = std::stof(burnwires_match[6]);
  burnwire_publisher_.publish(burnwire_message);
}

void Deckbox::powerConnectionCallback(const ds_core_msgs::RawData &bytes) {
  // do nothing -- the feedback that we'd publish for this will be in the following
  // status message.
  std::string power_response(reinterpret_cast<const char *>(bytes.data.data()),
			     bytes.data.size() - 1);
  ROS_INFO_STREAM("Received power response: " << power_response);
}

void Deckbox::ethernetConnectionCallback(const ds_core_msgs::RawData &bytes) {
  // do nothing -- the feedback that we'd publish for this will be in the following
  // status message.
  std::string ethernet_response(reinterpret_cast<const char *>(bytes.data.data()),
				bytes.data.size() - 1);
  ROS_INFO_STREAM("Received ethernet response: " << ethernet_response);

}
