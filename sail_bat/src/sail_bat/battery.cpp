/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/24/18.
//

#include "../../include/sail_bat/battery.h"
#include <ds_core_msgs/IoCommandList.h>
//#include <algorithm>

using namespace sail_bat;

Battery::Battery() : ds_base::DsBusDevice()
{
  address_ = "B2";
  state = ds_hotel_msgs::Battery{};
  bat = sensor_msgs::BatteryState{};

  state.chargeCoulombs = 0;
  state.percentFull = 0;
  state.repeatingCommandId = 0;

  regex_set();

  // don't balance unless directed to by the charger
  state.balanceEnable = false;
  state.balanceGain = 18;           // 18;
  state.balanceOnsetVoltage = 4.0;  // 10000;
  state.balanceMaxCurrent = 0.7;    // 0.5;
  state.balanceCommandId = 0;
}

Battery::Battery(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  // do nothing
  address_ = "B2";
  state = ds_hotel_msgs::Battery{};
  bat = sensor_msgs::BatteryState{};

  state.chargeCoulombs = 0;
  state.percentFull = 0;
  state.repeatingCommandId = 0;

  regex_set();

  ROS_INFO_STREAM("Battery::" << address_ << " ID: " << state.idnum);

  // don't balance unless directed to by the charger
  state.balanceEnable = false;
  state.balanceGain = 18;           // 18;
  state.balanceOnsetVoltage = 4.0;  // 10000;
  state.balanceMaxCurrent = 0.7;    // 0.5;
  state.balanceCommandId = 0;
}

Battery::Battery(std::string address) : ds_base::DsBusDevice()
{
  address_ = address;
  state = ds_hotel_msgs::Battery{};
  bat = sensor_msgs::BatteryState{};

  state.chargeCoulombs = 0;
  state.percentFull = 0;
  state.repeatingCommandId = 0;

  regex_set();

  // don't balance unless directed to by the charger
  state.balanceEnable = false;
  state.balanceGain = 18;           // 18;
  state.balanceOnsetVoltage = 4.0;  // 10000;
  state.balanceMaxCurrent = 0.7;    // 0.5;
  state.balanceCommandId = 0;
}

Battery::~Battery() = default;

void Battery::regex_set()
{
  accept_regex = boost::regex{ "!" + address_ + " " + address_ + " ([0-9A-Fa-f ]+)\n\r\x03" };
  start_regex = boost::regex{ "!" + address_ + " " + address_ + "(?= \\d)" };
  // end_regex = boost::regex { "(?<= )([0-9A-Fa-f]{2})\\s+(?![0-9A-Fa-f])"};
  end_regex = boost::regex{ "(?<= )([0-9A-Fa-f]{2})\\s?\n\r\x03" };  // This was changed, but without a reliably
                                                                     // different behavior

  // parse our IDnum.  Assume our addr is either BX or BATX
  boost::regex idnumSearcher{ "(B|BAT)(\\d{1,2})" };
  boost::smatch idnum_results;
  if (!boost::regex_search(address_, idnum_results, idnumSearcher))
  {
    ROS_DEBUG_STREAM("Battery::" << address_);
    ROS_DEBUG_STREAM("Unable to find ID Number in battery string");
    state.idnum = -1;
  }

  state.idnum = stol(idnum_results[2].str());

  ROS_INFO_STREAM("Battery::" << address_ << " ID: " << state.idnum);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool Battery::acceptsMessage(const std::string& msg)
{
  // match if the string starts with
  return boost::regex_search(msg, accept_regex);
}

#define NUM_VOLTAGES 14
#define NUM_TEMPERATURES 56

// These are things that are actually errors--
// OVERTEMP, UNDERVOLTAGE, OVERVOLTAGE, SAFETY_PROBLEM_CODE_SET
const uint32_t BAT_STRING_ERROR_MASK = 0x3e;            // don't worry about non-error conditions
const uint32_t BAT_REPORTED_ERROR_MASK = 0x3c;          // Errors reported by the battery itself
const uint32_t BAT_STRING_VALID_DISCHARGE_MASK = 0xfe;  // don't worry about non-error conditions
const uint32_t BAT_STRING_VALID_CHARGE_MASK = 0x37;     // don't worry about non-error conditions-- or undervolt

const std::vector<std::string> Battery::SAFETY_CODES = {
  "Normal", "OverVoltage", "UnderVoltage", "CellTemp", "SwitchTemp", "Timeout", "StateOfCharge",
};

// commands
const std::map<std::string, std::string> Battery::commandTable = { { "SwitchOff", "X" },
                                                                   { "Discharge", "D" },
                                                                   { "Charge", "C" },
                                                                   { "Reboot", "R" },
                                                                   { "ResetError", "r" } };

bool Battery::handleMessage(const std::string& msg)
{
  // now find the start of the string (yeah, whatever)

  boost::smatch start_results;
  if (!boost::regex_search(msg, start_results, start_regex))
  {
    ROS_ERROR_STREAM("Battery::" << address_ << "Unable to find start of battery status string (long-form)");
    return false;
  }
  ROS_ERROR_STREAM("Start results position" << start_results.position());

  // now find the end of the string
  boost::smatch end_results;
  std::string msg_substr = msg.substr(start_results.position());
  ROS_ERROR_STREAM(msg_substr);
  if (!boost::regex_search(msg_substr, end_results, end_regex))
  {
    ROS_ERROR_STREAM("Battery::" << address_ << "Unable to find end of battery status string (long-form)");
    return false;
  }

  if (end_results.position() < 400)
  {
    ROS_ERROR_STREAM("Matched string length " << end_results.position() << " is invalid because too short!");
    return false;
  }

  uint8_t accum = 0;
  uint8_t latest_c;
  for (int i = start_results.position(); i < end_results.position() + start_results.position(); i++)
  {
    latest_c = static_cast<uint8_t>(msg[i]);
    accum += latest_c;
  }
  uint16_t accum_16 = accum;  // Prints as an int, not a char

  std::string checksumStr = end_results[1];  // This has failed us sometimes
  std::string end_substr = msg.substr(start_results.position() + end_results.position(), 2);  // We are using this
  ROS_ERROR_STREAM("Checksum string: " << checksumStr.c_str() << " end substr: "
                                       << end_substr);  //<<" res0: "<<checksumStr_0<<" res2: "<<checksumStr_2);

  // Changed due to a Bat1 parsing error
  unsigned int expected = strtoul(end_substr.c_str(), NULL, 16);
  ROS_ERROR_STREAM("Got: " << accum_16 << " Expected: " << expected);

  if (expected != accum)
  {
    ROS_INFO_STREAM("Battery::" << address_ << "Battery status string checksum mismatch!");
    ROS_INFO_STREAM("Battery::" << address_ << "Expected: " << expected << ", got: " << accum_16);
    ROS_INFO_STREAM("Battery::" << address_ << "Checksum taken over: "
                                << msg.substr(start_results.position(), end_results.position()));
    ROS_INFO_STREAM("Battery::" << address_ << "Checksum of entire message (len=" << msg.length() << ") \"" << msg
                                << "\"");
    return false;
  }
  size_t len = end_results.position();

  // strip out non-printable things
  std::string msgFilt(msg.substr(start_results.position(), len));
  for (std::string::iterator iter = msgFilt.begin(); iter != msgFilt.end();)
  {
    if (!(std::isalnum(*iter) || std::isspace(*iter) || std::ispunct(*iter)))
    {
      iter = msgFilt.erase(iter);
    }
    else
    {
      iter++;
    }
  }  // for each character

  std::stringstream tokens(msgFilt);

  // If the string starts with "!", skip the first token
  tokens >> address_;
  if (address_[0] == '#')
  {
    tokens >> address_;
  }
  if (address_ == "L**")
  {
    tokens >> address_;
  }
  if (address_[0] == '!')
  {
    tokens >> address_;
  }

  // read voltages
  state.voltages.resize(NUM_VOLTAGES);
  bat.cell_voltage.resize(NUM_VOLTAGES);

  state.maxCellVoltage = 0;
  state.minCellVoltage = 100;
  int32_t foo;
  float float_foo;
  for (size_t i = 0; i < NUM_VOLTAGES; i++)
  {
    tokens >> foo;
    float_foo = static_cast<float>(foo) * 0.001;  // mV to V
    state.voltages[i] = float_foo;
    bat.cell_voltage[i] = float_foo;
    if (float_foo > state.maxCellVoltage)
    {
      state.maxCellVoltage = float_foo;
    }
    if (float_foo < state.minCellVoltage)
    {
      state.minCellVoltage = float_foo;
    }
  }
  tokens >> foo;
  float_foo = static_cast<float>(foo) * 0.001;  // mV to V
  state.totalVoltage = float_foo;
  bat.voltage = float_foo;

  // read temperatures
  state.maxCellTemp = 0;
  state.minCellTemp = 100;

  state.temperatures.resize(NUM_TEMPERATURES);
  for (size_t i = 0; i < NUM_TEMPERATURES; i++)
  {
    tokens >> state.temperatures[i];
    if (state.temperatures[i] > state.maxCellTemp)
    {
      state.maxCellTemp = state.temperatures[i];
    }
    if (state.temperatures[i] < state.minCellTemp)
    {
      state.minCellTemp = state.temperatures[i];
    }
    //        ROS_INFO_STREAM("Num temps"<< i << " value="<<state.temperatures[i]);
  }
  // switch card temperature
  tokens >> state.switchTemp;
  state.switchTemp = state.switchTemp;

  // read battery charge count stuff
  std::string raw;
  tokens >> raw;
  state.countFull = std::strtoul(raw.c_str(), nullptr, 16);
  tokens >> raw;
  state.countCapacity = std::strtoul(raw.c_str(), nullptr, 16);
  tokens >> raw;
  state.countFreerun = std::strtoul(raw.c_str(), nullptr, 16);

  // parse battery status
  tokens >> raw;
  state.discharging = (raw[0] != '0');
  state.charging = (raw[1] != '0');
  if (state.discharging)
  {
    bat.power_supply_status = 2;
  }
  else if (state.charging)
  {
    bat.power_supply_status = 1;
  }

  tokens >> state.safetyCode;
  if (state.safetyCode < 0 || state.safetyCode >= static_cast<int>(SAFETY_CODES.size()))
  {
    state.safetyString = "UNKNOWN";
  }
  else
  {
    state.safetyString = SAFETY_CODES[state.safetyCode];
  }
  tokens >> state.problemLocation;
  tokens >> state.numScans;
  tokens >> state.overflowCount;
  tokens >> state.timeoutCount;
  tokens >> state.timeoutLocation;
  tokens >> state.uptime;
  tokens >> state.interruptProblemLocation;
  tokens >> state.fixCount;

  // We have some data! Now lets get some meaning from it
  // (conversions, error status checking, that kinda stuff)
  //    lastMsgTime = ros::Time::now();

  ROS_INFO_STREAM("Battery::" << address_ << " Got msg:");
  ROS_INFO_STREAM(msg);

  // Handle connected vs. not
  state.errorStatus &= (~BATT_NOT_CONNECTED);  // Clear the NOT_CONNECTED flag
  state.errorStatus &= (~BATT_OFFLINE);        // The battery WAS offline, but we just heard from it.  So its good now.

  updateStatus();
  updateBalance();  // (possibly) update the balancing shunts

  return true;
}

void Battery::updateStatus()
{
  // Let's describe our logic behind battery status monitoring
  //
  // 1). Some error states come directly from the battery itself
  // 2). Error states LATCH.  NO error is cleared until we get a "NORMAL"
  //         status message from the batteries.
  // 3). BATT_SAFETY_PROBLEM_CODE_SET means that the battery module's internal
  //         monitoring microcontroller detected an error.  It is NOT enough
  //         to determine if the battery has an error
  // 4). Some additional checks can introduce error states even without this flag set.
  //         That's why you should use this->hasError() (see its bitmasking implementation)
  //
  // IV, 9 Nov 2016
  if ((state.errorStatus & BATT_NOT_CONNECTED) != 0)
  {
    // BATT_NOT_CONNECTED means there's no battery here.  Abort and
    // move on
    return;
  }

  //    if (secondsSinceLastMessage() > config.REPORT_TIMEOUT()) {
  //        errorStatus |= BATT_OFFLINE;
  //    } else {
  //        errorStatus &= (~BATT_OFFLINE);
  //    }

  switch (state.safetyCode)
  {
    case 0:  // Normal
      // Got NORMAL status from battery, clear battery-reported error flags
      state.errorStatus &= (~BAT_REPORTED_ERROR_MASK);
      break;
    case 1:  // OverVoltage
      state.errorStatus |= (BATT_OVERVOLTAGE | BATT_SAFETY_PROBLEM_CODE_SET);
      break;
    case 2:  // UnderVoltage
      state.errorStatus |= (BATT_UNDERVOLTAGE | BATT_SAFETY_PROBLEM_CODE_SET);
      break;
    case 3:  // CellTemp
    case 4:  // SwitchTemp
      state.errorStatus |= (BATT_OVERTEMP | BATT_SAFETY_PROBLEM_CODE_SET);
      break;
    case 5:  // Timeout
    case 6:  // StateOfCharge
      state.errorStatus |= BATT_SAFETY_PROBLEM_CODE_SET;
      break;
    default:
      // this shouldn't happen, so just flag an error and move on.
      state.errorStatus |= BATT_SAFETY_PROBLEM_CODE_SET;
      ROS_ERROR_STREAM("Battery::" << address_ << " Unknown battery safety code (?!?!)");
  }

  // manually check for under voltage
  for (size_t i = 0; i < state.voltages.size(); i++)
  {
    if (static_cast<double>(state.voltages[i]) * 0.001 < config.cellUnderVoltage)
    {
      state.errorStatus |= BATT_UNDERVOLTAGE;
    }
  }

  // Ok, now do the switches
  if (!state.discharging)
  {
    state.errorStatus |= BATT_DISCHARGE_SWITCH_OFF;
  }
  else
  {
    state.errorStatus &= (~BATT_DISCHARGE_SWITCH_OFF);
  }
  if (state.charging)
  {
    state.errorStatus |= BATT_CHARGE_SWITCH_ON;
  }
  else
  {
    state.errorStatus &= (~BATT_CHARGE_SWITCH_ON);
  }

  if (state.balanceEnable)
  {
    state.errorStatus |= BATT_BALANCING;
  }
  else
  {
    state.errorStatus &= (~BATT_BALANCING);
  }

  // Finally, let's handle coulomb counts

  // Battery reports fuel gauge as counts when state, counts right now, and capacity (measured in counts).
  // HOWEVER, the counter can roll over.  Capacity is usually good, but its perfectly valid for freerun > state
  // Fortunately, we can handle this by cleverly casting between signed and unsigned types.
  //
  // First, note that ALL the count variables are UNSIGNED.  That's VERY important! If freerun rolls over, it
  // rolls over to what would be a negative number if everything was signed.  But by keeping things
  // unsigned, the processor handles rollover correctly (yes, its a hack, but it works.  So go with it)
  //
  // Next, we compute how much of the battery has been expended, but we need it to be SIGNED!
  // (yes, we can have negative battery expenditure if we charge past state)
  // casting avoids having to manually condition on std::numeric_limits/2 kind of stuff
  state.countUsed = static_cast<int32_t>(state.countFull - state.countFreerun);

  // next, compute number of counts available.  Again, can be negative if we've discharged past
  // 0 fuel.
  state.countAvailable = static_cast<int32_t>(state.countCapacity) - state.countUsed;

  // Next, convert capacity and available charge to coulombs.  Just use doubles.
  state.capacityCoulombs = config.countToCoulomb * static_cast<double>(state.countCapacity);
  state.capacityAh = state.capacityCoulombs / 3600.0;
  bat.capacity = state.capacityAh;

  state.chargeCoulombs = config.countToCoulomb * static_cast<double>(state.countAvailable);
  state.chargeAh = state.chargeCoulombs / 3600.0;
  bat.charge = state.chargeAh;

  // Finally, if we have a reading on capacity, do the percentage calculation
  if (state.capacityCoulombs > 0)
  {  // dividing by zero often offends
    state.percentFull = 100.0 * state.chargeCoulombs / state.capacityCoulombs;
  }
  else
  {
    state.percentFull = 0.0;
  }
  bat.percentage = state.percentFull;

  bat.present = true;
  bat.location = std::to_string(state.idnum);
}

void Battery::updateBalance()
{
  if (!state.balanceEnable)
  {
    return;
  }

  if (!state.charging)
  {
    ROS_DEBUG_STREAM("Battery::" << address_ << " not in charge mode but had balancer enabled");

    zeroShunts();
    state.balanceEnable = false;
  }

  // find min/max voltage
  int hi = state.voltages[0];
  int lo = state.voltages[0];
  for (size_t i = 1; i < state.voltages.size(); i++)
  {
    hi = (hi > state.voltages[i] ? hi : state.voltages[i]);
    lo = (lo < state.voltages[i] ? lo : state.voltages[i]);
  }

  // If the lowest cell voltage is near the shutoff limit (safety) then zero the shunts and turn off the balance
  if (lo < config.cellShutoffBalance)
  {
    state.balanceEnable = false;
    zeroShunts();
    ROS_ERROR_STREAM("Balance cell voltage: " << lo << " below limit: " << config.cellShutoffBalance
                                              << " so shut off balance on Module " << state.idnum);
  }

  std::string shuntStr;
  shuntStr += "#" + address_ + " B ";
  bool doneBalancing = true;
  for (size_t i = 0; i < state.voltages.size(); i++)
  {
    float curr = static_cast<float>(state.voltages[i] - lo) * state.balanceGain;
    int level = static_cast<int>(round(curr * 0.06));  // rounds same way as multicharger
    if (level < 0)
    {
      level = 0;
    }
    if (level > 9)
    {
      level = 9;
    }
    shuntStr += std::to_string(level);
    if (level > 0)
    {
      doneBalancing = false;
    }
  }
  shuntStr += " ";

  _send_preempt(shuntStr);
  ROS_INFO_STREAM("Battery::" << address_ << " Balancer setting shunts: " << shuntStr);

  if (doneBalancing)
  {
    state.balanceEnable = false;
    ROS_INFO_STREAM("Battery::" << address_ << " Balancer reports done");
  }
}

void Battery::zeroShunts()
{
  std::string cmd = "#" + address_ + " B 00000000000000 ";
  _send_preempt(cmd);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

//------------------------------------------------------------
// Private inner stuff
bool Battery::_bat_cmd(const sentry_msgs::BatteryCmd::Request& req, const sentry_msgs::BatteryCmd::Response& resp)
{
  std::string cmdstr = "#" + address_ + " ";

  switch (req.command)
  {
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_OFF:
      if (state.balanceEnable)
      {
        zeroShunts();
      }
      cmdstr += "X ";
      break;

    case sentry_msgs::BatteryCmd::Request::BAT_CMD_CHARGE:
      cmdstr += "C ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_DISCHARGE:
      cmdstr += "D ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_RESET_FLAGS:
      cmdstr += "r ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_REBOOT:
      cmdstr += "R ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_RECORD_FULL:
      cmdstr += "F ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_RECORD_EMPTY:
      cmdstr += "E ";
      break;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_ZERO_SHUNTS:
      zeroShunts();
      return true;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_ENABLE_BALANCE:
      state.balanceEnable = true;
      return true;
    case sentry_msgs::BatteryCmd::Request::BAT_CMD_DISABLE_BALANCE:
      state.balanceEnable = false;
      zeroShunts();
      return true;
    default:
      ROS_ERROR_STREAM("Request for unknown bat command " << req.command);
      return false;
  }

  _send_preempt(cmdstr);
  return true;
}

void Battery::_send_preempt(const std::string& cmdstr)
{
  ds_core_msgs::IoCommandList cmdList;
  ds_core_msgs::IoCommand cmd;

  cmd.delayBefore_ms = 0;
  cmd.timeout_ms = 1000;
  cmd.delayAfter_ms = 0;
  if (cmdstr.find_first_of("C") < cmdstr.size())
    cmd.delayAfter_ms = 50;  // Case: charge
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  cmd.command = with_checksum_text(cmdstr);

  cmdList.cmds.push_back(cmd);

  PreemptCmd().publish(cmdList);
}

void Battery::_send_wakeup_cmd()
{
  // TODO: CONFIRM THAT IT WORKS ON VEHICLE
  // We are concerned that the batteries are spitting junk on startup.
  // In order to let things cool off, we are sending a param dump with a
  // nice long timeout first.

  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_PREEMPT;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 100;
  cmd.timeout_ms = 10000;
  cmd.delayAfter_ms = 100;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query state status
  std::string command = "#" + address_ + " p ";
  cmd.command = "###" + with_checksum_text(command);
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().waitForExistence(ros::Duration(1.0)))
  {
    if (IosmCmd().call(sm_cmd))
    {
      ROS_INFO_STREAM("Wakup command success");
    }
    else
    {
      ROS_ERROR_STREAM("Wakeup command failure");
    }
  }
  else
  {
    ROS_ERROR_STREAM("Wakeup command failure");
  }
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

void Battery::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  _send_wakeup_cmd();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 100;
  cmd.timeout_ms = 3000;
  cmd.delayAfter_ms = 100;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query state status
  std::string command = "#" + address_ + " L ";
  cmd.command = with_checksum_text(command);
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().call(sm_cmd))
  {
    ROS_INFO_STREAM("Possibly got functioning I/O state machine");
  }
  else
  {
    ROS_INFO_STREAM("Error calling I/O State Machine config service");
  }
}

void Battery::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();

  address_ = ros::param::param<std::string>("~sail_address", "B3");
  regex_set();
}

void Battery::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();
  // prepare our publishers
  auto nh = nodeHandle();
  bat_state_pub_ = nh.advertise<ds_hotel_msgs::Battery>(ros::this_node::getName() + "/state", 10);
  bat_pub_ = nh.advertise<sensor_msgs::BatteryState>("bat", 10);

  // prepare to a service for indexing, sleeping, etc
  bat_ctrl_cmd_ = nh.advertiseService<sentry_msgs::BatteryCmd::Request, sentry_msgs::BatteryCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&Battery::_bat_cmd, this, _1, _2));
}

void Battery::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());
  if (handleMessage(msg))
  {
    // publish!
    ROS_INFO_STREAM("BAT \"" << address_ << "\"");
    state.header = bytes.header;
    bat.header = state.header;
    // publish battery states
    bat_pub_.publish(bat);
    bat_state_pub_.publish(state);
  }
  else
  {
    ROS_INFO_STREAM("failed to handleMessage \n");
  }
}

std::string Battery::with_checksum_text(std::string cmd)
{
  uint8_t accum = 0;
  const char* cmd_char = cmd.c_str();
  for (int i = 0; i < strlen(cmd_char); i++)
  {
    accum += cmd_char[i];
  }
  return cmd + ds_util::int_to_hex<uint16_t>(accum) + "\r";
}
