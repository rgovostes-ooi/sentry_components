/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/24/18.
//

#include "../../include/sail_bat/balancer.h"

using namespace sail_bat;

Balancer::Balancer()
{
  is_balancing = false;
}

Balancer::Balancer(int argc, char** argv, const std::string& name) : ds_base::DsProcess(argc, argv, name)
{
  is_balancing = false;
}

Balancer::~Balancer() = default;

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
void Balancer::setupParameters()
{
  ds_base::DsProcess::setupParameters();

  bat_path = ros::param::param<std::string>("~bat_path", "bat6");
}

void Balancer::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  auto nh = nodeHandle();
  bat_sub = nh.subscribe(bat_path + "/state", 1, &Balancer::sub_callback, this);
  bat_client = nh.serviceClient<sentry_msgs::BatteryCmd>(bat_path + "/cmd");
  balance_server = nh.advertiseService<sentry_msgs::PowerCmd::Request, sentry_msgs::PowerCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&Balancer::cmd_callback, this, _1, _2));
  timer = nh.createTimer(ros::Duration(30), &Balancer::timer_callback, this);
}

bool Balancer::cmd_callback(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::PowerCmd::Request::POWER_CMD_ON:
      balance_on();
      break;
    case sentry_msgs::PowerCmd::Request::POWER_CMD_OFF:
      balance_off();
      break;
    default:
      ROS_ERROR_STREAM("Unknown balancer command received: " << req.command);
      return false;
  }
  return true;
}

void Balancer::timer_callback(const ros::TimerEvent&)
{
  if (!is_balancing)
  {
    return;
  }
  balance_on();
}

void Balancer::sub_callback(ds_hotel_msgs::Battery msg)
{
  //    if (msg.balanceEnable && three_strikes>3 && is_balancing){
  //        balance_off();
  //    }
}

void Balancer::balance_on()
{
  is_balancing = true;

  auto charge_cmd = sentry_msgs::BatteryCmd{};
  charge_cmd.request.command = sentry_msgs::BatteryCmd::Request::BAT_CMD_CHARGE;
  if (bat_client.call(charge_cmd))
  {
    ROS_INFO_STREAM("Battery charge command sent");
  }
  else
  {
    ROS_ERROR_STREAM("Battery charge command failed");
  }

  auto balance_cmd = sentry_msgs::BatteryCmd{};
  balance_cmd.request.command = sentry_msgs::BatteryCmd::Request::BAT_CMD_ENABLE_BALANCE;
  if (bat_client.call(balance_cmd))
  {
    ROS_INFO_STREAM("Battery balance enable command sent");
  }
  else
  {
    ROS_ERROR_STREAM("Battery balance enable command failed");
  }
}

void Balancer::balance_off()
{
  is_balancing = false;

  auto charge_cmd = sentry_msgs::BatteryCmd{};
  charge_cmd.request.command = sentry_msgs::BatteryCmd::Request::BAT_CMD_OFF;
  if (bat_client.call(charge_cmd))
  {
    ROS_INFO_STREAM("Battery off command sent");
  }
  else
  {
    ROS_ERROR_STREAM("Battery off command failed");
  }

  auto balance_cmd = sentry_msgs::BatteryCmd{};
  balance_cmd.request.command = sentry_msgs::BatteryCmd::Request::BAT_CMD_DISABLE_BALANCE;
  if (bat_client.call(balance_cmd))
  {
    ROS_INFO_STREAM("Battery balance disable command sent");
  }
  else
  {
    ROS_ERROR_STREAM("Battery balance disable command failed");
  }
}
