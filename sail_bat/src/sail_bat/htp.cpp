/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/23/18.
//

#include "../../include/sail_bat/htp.h"

#include <ds_core_msgs/IoSMcommand.h>
#include <ds_core_msgs/IoCommandList.h>

using namespace sail_bat;

HTP::HTP() : ds_base::DsBusDevice()
{
  // do nothing
  address_ = "HTP2";
  handled = ds_hotel_msgs::HTP{};

  update_regex();
}

HTP::HTP(int argc, char* argv[], const std::string& name) : ds_base::DsBusDevice(argc, argv, name)
{
  // do nothing
  address_ = "HTP2";
  handled = ds_hotel_msgs::HTP{};

  update_regex();
}

HTP::~HTP() = default;

bool HTP::acceptsMessage(const std::string& msg)
{
  // match if the string starts with
  return boost::regex_search(msg, match_regex);
}

bool HTP::handleMessage(const std::string& msg)
{
  boost::smatch results;
  if (!boost::regex_search(msg, results, parse_regex))
  {
    ROS_INFO_STREAM("Unable to parse HTP string for sensor " << address_);
    ROS_INFO_STREAM(msg);
    return false;
  }

  unsigned long raw_h = std::strtoul(results[1].str().c_str(), nullptr, 16);
  double proc_h = computeHumidity(raw_h);
  handled.humidity = proc_h;

  unsigned long raw_t = std::strtoul(results[2].str().c_str(), nullptr, 16);
  double proc_t = computeTemperature(raw_t);
  handled.temperature = proc_t;

  unsigned long raw_p = std::strtoul(results[3].str().c_str(), nullptr, 16);
  double proc_p = computePressure(raw_p);
  handled.pressure = proc_p;

  return true;
}

double HTP::computeHumidity(unsigned long raw)
{
  // from compute_humidity in sentry_hotel_thread.cpp, 8 Nov 2016
  // Yes, I really found these calibration constants in the code
  double Vout = 5.0 * static_cast<double>(raw) / 255.0;
  return ((Vout - 0.875) / 0.0317);
}

const static double AT = 9.397284160441968e-04;
const static double BT = 2.205165651179403e-04;
const static double CT = 1.287378917072399e-07;
double HTP::computeTemperature(unsigned long raw)
{
  // from compute_temperature in sentry_hotel_thread.cpp
  // Again, still got those calibration constants from teh codez
  double R, RT;
  double result = 0;
  if (raw > 0)
  {
    R = 49900.0 / (255.0 / static_cast<double>(raw) - 1.0);
    RT = AT + BT * log(R) + CT * pow(log(R), 3.0);
    if (RT != 0.0)
      result = (1.0 / RT) - 273.0;
  }
  return result;
}

// calibration constants for pressure sensor.
// Looks like M_KPA is a slope and B_KPA is an intercept (both in kPa)
const static double M_KPA = 0.8734;
const static double B_KPA = -85.4303;
double HTP::computePressure(unsigned long raw)
{
  // from compute_pressure in sentry_hotel_thread.cpp
  // Again, still got those calibration constants from teh codez
  double p_kpa = M_KPA * static_cast<double>(raw) + B_KPA;

  return p_kpa / 6.89476;
}

void HTP::setupIoSM()
{
  ds_base::DsBusDevice::setupIoSM();

  // Prepare our I/O state machine command
  ds_core_msgs::IoSMcommand sm_cmd;
  sm_cmd.request.iosm_command = ds_core_msgs::IoSMcommand::Request::IOSM_ADD_REGULAR;

  ds_core_msgs::IoCommand cmd;
  cmd.delayBefore_ms = 100;
  cmd.timeout_ms = 3000;
  cmd.delayAfter_ms = 100;
  cmd.forceNext = false;
  cmd.timeoutWarn = true;
  cmd.emitOnMatch = true;

  // query our voltages
  cmd.command = "#" + address_ + "?V";
  sm_cmd.request.commands.push_back(cmd);

  if (IosmCmd().call(sm_cmd))
  {
    // TODO
    ROS_INFO_STREAM("Possibly got functioning I/O state machine");
  }
  else
  {
    ROS_INFO_STREAM("Error calling I/O State Machine config service");
  }
}

void HTP::setupParameters()
{
  ds_base::DsBusDevice::setupParameters();

  address_ = ros::param::param<std::string>("~sail_address", "HTP6");

  update_regex();

  auto generated_uuid = ds_base::generateUuid("sail_bat_" + address_);
}

void HTP::setupConnections()
{
  ds_base::DsBusDevice::setupConnections();

  // prepare our publishers
  htp_pub_ = nodeHandle().advertise<ds_hotel_msgs::HTP>(ros::this_node::getName() + "/htp", 10);
}

void HTP::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());

  if (handleMessage(msg))
  {
    // publish!
    ROS_INFO_STREAM("HTP \"" << address_ << "\"");
    handled.header.stamp = ros::Time::now();

    // publish servo states
    htp_pub_.publish(handled);
  }
  else
  {
    ROS_INFO_STREAM("failed to handleMessage");
  }
}

double HTP::getHumidity()
{
  return handled.humidity;
}

double HTP::getTemperature()
{
  return handled.temperature;
}

double HTP::getPressure()
{
  return handled.pressure;
}

void HTP::update_regex()
{
  match_regex = boost::regex{ "#" + address_ };
  parse_regex = boost::regex{ "#" + address_ + "\\?V.H=([0-9A-Fa-f]{2}) T=([0-9A-Fa-f]{2}) P=([0-9A-Fa-f]{2})" };
}
