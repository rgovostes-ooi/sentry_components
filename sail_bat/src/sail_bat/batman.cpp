/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/1/18.
//

#include <sentry_msgs/BatteryCmd.h>
#include <ds_hotel_msgs/PowerSupplyCommand.h>
#include "../../include/sail_bat/batman.h"

using namespace sail_bat;

BatMan::BatMan() : DsProcess()
{
  ChargeConfig config();
  multi = nullptr;
}

BatMan::BatMan(int argc, char** argv, const std::string& name) : DsProcess(argc, argv, name)
{
  ChargeConfig config();
  multi = nullptr;
}

BatMan::~BatMan()
{
  safeBats();
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatMan::startCharge()
{
  stopCharge();

  ROS_INFO_STREAM("CHARGE STARTED");
  multi = new MultichargeSequence(bats, chgs, multi_timer);
}

void BatMan::stopCharge()
{
  ROS_INFO_STREAM("CHARGE STOPPED");
  if (multi != nullptr)
  {
    ROS_INFO_STREAM("multi existed");
    delete multi;
    multi_timer.stop();
    multi = nullptr;
  }

  batsOff();
  for (int i = 0; i < chgs.size(); i++)
  {
    chgs[i]->setOff();
  }
}

void BatMan::batsOn()
{
  ROS_INFO_STREAM("BATS ON");
  for (int i = 0; i < bats.size(); i++)
  {
    bats[i]->setOn();
  }
}

void BatMan::batsOff()
{
  ROS_INFO_STREAM("BATS OFF");
  for (int i = 0; i < bats.size(); i++)
  {
    bats[i]->setOff();
  }
}

void BatMan::shoreOn()
{
  ROS_INFO_STREAM("SHORE ON");
  shore->setVC(61, 8.0);  // TODO: Real Values
}

void BatMan::shoreOff()
{
  ROS_INFO_STREAM("SHORE OFF");
  shore->setOff();
}

void BatMan::safeBats()
{
  ROS_INFO_STREAM("SAFE BATS");
  stopCharge();
}

void BatMan::pubLatest(const ros::TimerEvent&)
{
  auto now = ds_hotel_msgs::BatMan{};

  // Add timestamp.
  now.header.stamp = ros::Time::now();
  now.ds_header.io_time = now.header.stamp;

  now.moduleVolt.resize(num_bats);
  now.moduleAh.resize(num_bats);
  now.modulePercent.resize(num_bats);
  now.num_bats = 0;
  now.maxModuleVolt = 0;
  now.minModuleVolt = 100;
  now.maxCellVolt = 0;
  now.minCellVolt = 100;
  now.maxSwitchTemp = 0;
  now.minSwitchTemp = 100;
  now.maxCellTemp = 0;
  now.minCellTemp = 100;

  for (int i = 0; i < num_bats; i++)
  {
    auto bat = bats[i]->get_latest();
    now.moduleVolt[i] = bat.totalVoltage;
    now.moduleAh[i] = bat.chargeAh;
    now.modulePercent[i] = bat.percentFull;

    if (bat.idnum == 0)
    {
      // Battery hasn't published yet, so skip to the next battery
      continue;
    }
    now.num_bats += 1;
    if (bat.switchTemp < now.minSwitchTemp)
    {
      now.minSwitchTemp = bat.switchTemp;
    }
    if (bat.switchTemp > now.maxSwitchTemp)
    {
      now.maxSwitchTemp = bat.switchTemp;
    }
    if (bat.minCellTemp < now.minCellTemp)
    {
      now.minCellTemp = bat.minCellTemp;
    }
    if (bat.maxCellTemp > now.maxCellTemp)
    {
      now.maxCellTemp = bat.maxCellTemp;
    }
    if (bat.totalVoltage > now.maxModuleVolt)
    {
      now.maxModuleVolt = bat.totalVoltage;
    }
    if (bat.totalVoltage < now.minModuleVolt)
    {
      now.minModuleVolt = bat.totalVoltage;
    }
    if (bat.maxCellVoltage > now.maxCellVolt)
    {
      now.maxCellVolt = bat.maxCellVoltage;
    }
    if (bat.minCellVoltage < now.minCellVolt)
    {
      now.minCellVolt = bat.minCellVoltage;
    }
    if (bat.percentFull > -10 && bat.percentFull < 110)
    {
      if (bat.discharging)
      {
        now.capacityCoulombs += bat.capacityCoulombs;
        now.capacityAh += bat.capacityAh;
        now.chargeCoulombs += bat.chargeCoulombs;
        now.chargeAh += bat.chargeAh;
      }
    }
    else
    {
      ROS_ERROR_STREAM("Bat " << bat.idnum << " has an out-of-range percent");
    }
  }
  now.percentFull = 100.0 * now.chargeAh / now.capacityAh;
  latest = now;
  batman_pub_.publish(latest);
}

void BatMan::stop()
{
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatMan::setupParameters()
{
  num_bats = 6;
  num_chgs = 5;

  auto batman_prefix = ros::this_node::getNamespace() + "/";
  auto bat_prefix = ros::param::param<std::string>("~bat_path", batman_prefix);
  bats.resize(num_bats);
  for (int i = 0; i < num_bats; i++)
  {
    bats[i] = new Bat(bat_prefix + "bat" + std::to_string(i + 1), i + 1);
  }

  auto chg_prefix = ros::param::param<std::string>("~chg_path", batman_prefix);
  //    auto chg_prefix = ros::this_node::getNamespace();
  chgs.resize(num_chgs);
  for (int i = 0; i < num_chgs; i++)
  {
    chgs[i] = new Pwr(chg_prefix + "chg" + std::to_string(i + 1), i + 1);
  }

  auto shore_addr = ros::param::param<std::string>("~shore_address", batman_prefix + "shore");
  shore = new Pwr(shore_addr, 0);
}

void BatMan::setupConnections()
{
  ROS_INFO_STREAM("Setup connections");

  bat_clients_.resize(num_bats);
  bat_subs_.resize(num_bats);
  chg_clients_.resize(num_chgs);
  chg_subs_.resize(num_chgs);

  auto nh = nodeHandle();
  for (int i = 0; i < num_bats; i++)
  {
    ROS_INFO_STREAM(bats[i]->get_addr());
    bat_clients_[i] = nh.serviceClient<sentry_msgs::BatteryCmd>(bats[i]->get_addr() + "/cmd");
    bat_subs_[i] = nh.subscribe(bats[i]->get_addr() + "/state", 1, &Bat::updateLatest, bats[i]);
    bats[i]->set_client(bat_clients_[i]);
    if (i < num_chgs)
    {
      chg_clients_[i] = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(chgs[i]->get_addr() + "/cmd");
      chg_subs_[i] = nh.subscribe(chgs[i]->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, chgs[i]);
      chgs[i]->set_client(chg_clients_[i]);
    }
  }

  shore_client_ = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(shore->get_addr() + "/cmd");
  shore_sub_ = nh.subscribe(shore->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, shore);
  shore->set_client(shore_client_);

  chg_cmd_ = nh.advertiseService<sentry_msgs::ChargeCmd::Request, sentry_msgs::ChargeCmd::Response>(
      ros::this_node::getName() + "/chg_cmd", boost::bind(&BatMan::_chg_cmd, this, _1, _2));

  bat_pwr_cmd_ = nh.advertiseService<sentry_msgs::PowerCmd::Request, sentry_msgs::PowerCmd::Response>(
      ros::this_node::getName() + "/bat_pwr_cmd", boost::bind(&BatMan::_bat_pwr_cmd, this, _1, _2));

  shore_pwr_cmd_ = nh.advertiseService<sentry_msgs::PowerCmd::Request, sentry_msgs::PowerCmd::Response>(
      ros::this_node::getName() + "/shore_pwr_cmd", boost::bind(&BatMan::_shore_pwr_cmd, this, _1, _2));

  batman_pub_ = nh.advertise<ds_hotel_msgs::BatMan>(ros::this_node::getName() + "/state", 10);

  multi_timer = nh.createTimer(ros::Duration(1), &BatMan::tick, this);
  multi_timer.stop();

  pub_timer = nh.createTimer(ros::Duration(3), &BatMan::pubLatest, this);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool BatMan::_chg_cmd(const sentry_msgs::ChargeCmd::Request& req, const sentry_msgs::ChargeCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::ChargeCmd::Request::CHARGE_CMD_OFF:
      stopCharge();
      break;
    case sentry_msgs::ChargeCmd::Request::CHARGE_CMD_CHARGE:
      startCharge();
      break;
    default:
      ROS_ERROR_STREAM("Request for unknown charge command " << req.command);
      return false;
  }
  return true;
}

bool BatMan::_bat_pwr_cmd(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::PowerCmd::Request::POWER_CMD_OFF:
      batsOff();
      break;
    case sentry_msgs::PowerCmd::Request::POWER_CMD_ON:
      batsOn();
      break;
    default:
      ROS_ERROR_STREAM("Request for unknown battery power command " << req.command);
      return false;
  }
  return true;
}

bool BatMan::_shore_pwr_cmd(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::PowerCmd::Request::POWER_CMD_OFF:
      shoreOff();
      break;
    case sentry_msgs::PowerCmd::Request::POWER_CMD_ON:
      shoreOn();
      break;
    default:
      ROS_ERROR_STREAM("Request for unknown shore power command " << req.command);
      return false;
  }
  return true;
}

void BatMan::tick(const ros::TimerEvent&)
{
  ROS_INFO_STREAM("TICK FUNCTION CALLED OR SOMETHING");
  if (multi != nullptr)
  {
    multi->tick();
  }
}

///////////////////////////////////////////////////////////////////////////////
// MultichargeSequence
///////////////////////////////////////////////////////////////////////////////

MultichargeSequence::MultichargeSequence(std::vector<Bat*> _batteries, std::vector<Pwr*> _chargers, ros::Timer _timer)
  : batteries(_batteries), chargers(_chargers), timer(_timer), config()
{
  ROS_INFO_STREAM("MULTI initialize");
  //    battery = batteries[0];
  num_bats = batteries.size();
  starting = false;
  timer.setPeriod(ros::Duration(30));
  timer.start();
  //    updateTimer = nh->createTimer(ros::Duration(10), &MultichargeSequence::tick, this);

  //    updateTimer.expires_from_now(boost::posix_time::seconds(10));
  //    updateTimer.async_wait(boost::bind(&MultichargeSequence::tick, this));

  startCharge();
}

MultichargeSequence::~MultichargeSequence()
{
  stopCharge();
}

void MultichargeSequence::startCharge()
{
  ROS_INFO_STREAM("CHARGE start");

  //    bottle = _bottle;
  starting = true;
  batteryId = 0;
  chargerId = 0;

  // Turn all the batteries off
  for (int i = 0; i < num_bats; i++)
  {
    batteries[i]->setOff();
  }

  //     Turn on all the chargers REALLY LOW
  for (auto chg = chargers.begin(); chg != chargers.end(); chg++)
  {
    (*chg)->setVC(config.getTARGET_V(), config.getINITIAL_C());
  }

  // we're going to start turning the batteries on slowly one by one
  // checking to see if current is being drawn.  That way we don't
  // have to worry about batteries / chargers getting cross-wired,
  // which would be a major safety issue!

  batteries[batteryId]->setCharge();
}

void MultichargeSequence::startupTick()
{
  ROS_INFO_STREAM("TICK start");
  // check to see if one of the chargers is ready to go
  bool found = false;

  if (batteries[batteryId]->get_is_charging())
  {
    ROS_INFO_STREAM("BatMan knows the battery is charging");
    for (auto chg = chargers.begin(); chg != chargers.end();)
    {
      if ((*chg)->get_meas_amps() > config.getINITIAL_C() / 2.0)
      {
        // We found the connected charger!
        ROS_DEBUG_STREAM("Starting charge sequence " << (*chg)->get_id() << " --> " << batteries[batteryId]->get_id());
        ROS_ERROR_STREAM("Starting charge sequence " << (*chg)->get_id() << " --> " << batteries[batteryId]->get_id());

        chargeSeqs.push_back(new ChargeSequence(config, (*chg), batteries[batteryId]));
        found = true;
        auto eraseMe = chg;
        chg++;
        chargers.erase(eraseMe);
        break;
      }
      chg++;
    }
    if (!found)
    {
      ROS_INFO_STREAM("Could not find charger for battery " << batteries[batteryId]->get_id());
      batteries[batteryId]->setOff();
    }
  }

  batteryId++;
  if (batteryId >= num_bats || chargers.empty())
  {
    // we're out of batteries or chargers
    starting = false;
    // turn off any remaining chargers
    for (auto chg = chargers.begin(); chg != chargers.end(); chg++)
    {
      (*chg)->setOff();
    }
    chargers.clear();
    //        if (chargeSeqs.size() == 0) {
    //            stopCharge();
    //            return;
    //        }
    ROS_INFO_STREAM("Charging " << chargeSeqs.size() << " battery/charger pairs!");
    return;
  }

  // turn on the next battery
  std::stringstream batname;
  batname << "B" << batteryId;

  //    auto battery = batteries[batteryId];
  if (batteries.size() < batteryId)
  {
    ROS_ERROR_STREAM("No battery " << batteryId << " exists");
  }
  else
  {
    batteries[batteryId]->setCharge();
    //        bottle->sendCommand(bat->getQuery());
  }
  // we'll see which charger is on next tick
}

void MultichargeSequence::tick()
{
  timer.setPeriod(ros::Duration(40));
  //    updateTimer.setPeriod(ros::Duration(40));
  ROS_INFO_STREAM("TICK: " << batteryId);

  if (starting)
  {
    startupTick();
  }
  for (auto chg_sq = chargeSeqs.begin(); chg_sq != chargeSeqs.end();)
  {
    (*chg_sq)->tick();
    if ((*chg_sq)->isDone())
    {
      ROS_INFO_STREAM("Removing battery charge sequence");
      // erasing makes the iterator invalid, so create a temporary
      // This also makes the loop iterator ugly.  But ugly beats segfault.
      auto eraseMe = chg_sq;
      chg_sq++;
      chargeSeqs.erase(eraseMe);
    }
    else
    {
      chg_sq++;
    }
  }
}

void MultichargeSequence::stopCharge()
{
  ROS_INFO_STREAM("MULTI stop");
  chargeSeqs.resize(0);  // will call destructors
}

///////////////////////////////////////////////////////////////////////////////
// ChargeSequence
///////////////////////////////////////////////////////////////////////////////
ChargeSequence::ChargeSequence(const ChargeConfig& _c, Pwr* _charger, Bat* _battery)
  : config(_c), battery(_battery), charger(_charger)
{
  ROS_INFO_STREAM("Created new chargesequence!");
  // Do nothing (initializer list did it all)
  batteryCommsTimeout = config.getBatteryTimeout();
  chargerCommsTimeout = config.getChargerTimeout();

  current = config.getINITIAL_C();
  voltage = config.getTARGET_V();
  terminalVmax = config.getN_CELLS() * config.getTARGET_CELL_V() + config.getDIODE_V();

  state = ChargeState::RAMP;

  // set the balancer config
  //    battery->setBalanceGain(config.getBalanceGain());
  //    battery->setBalanceOnsetVoltage(config.getBalanceOnsetVoltage());
  //    battery->setBalanceMaxCurrent(config.getBalanceMaxCurrent());
  balancerState = BALANCER_NOT_STARTED;
  startCharge();
}

ChargeSequence::~ChargeSequence()
{
  stopCharge();
}

void ChargeSequence::tick()
{
  if (charge_notOk())
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " is not OK, aborting");
    stopCharge();
    return;
  }

  ROS_INFO_STREAM("Sending Battery charge again");
  battery->setCharge();

  switch (state)
  {
    case DONE:
      ROS_INFO_STREAM("Case done");
      // do nothing
      break;

    case RAMP:
      ROS_INFO_STREAM("Case ramp");
      tick_ramp();
      break;

    case CHARGE:
      ROS_INFO_STREAM("Case charge");
      tick_charge();
      break;

    case BALANCE:
      ROS_INFO_STREAM("Case balance");
      tick_balance();
      break;

    default:
      ROS_ERROR_STREAM("Battery " << battery->get_id() << " charge sequence has an unknown state, aborting charge");
      stopCharge();
  }
}

void ChargeSequence::tick_ramp()
{
  ROS_ERROR_STREAM("TICK RAMP");

  // This would only happen if we're topping off a battery
  if (charger->get_is_const_voltage())
  {
    ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                       << " reached constant-voltage during ramp.  Proceeding directly to charge");
    voltage = terminalVmax + current * config.getCABLE_R();
    state = ChargeState::CHARGE;
    charger->setVC(voltage, current);
    return;
  }

  // The normal case (constant current!)
  if (current + config.getRAMP_INCR_C() < config.getMAX_C())
  {
    current += config.getRAMP_INCR_C();
  }
  else
  {
    current = config.getMAX_C();
    state = ChargeState::CHARGE;

    ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                       << " has finished RAMP V=" << voltage << "V C=" << current << "A");
  }

  ROS_INFO_STREAM("updating VC");
  voltage = terminalVmax + current * config.getCABLE_R();
  charger->setVC(voltage, current);
}

void ChargeSequence::tick_charge()
{
  ROS_ERROR_STREAM("TICK CHARGE");
  // Also use measured current-- if there's no cable resistance,
  // Daniel's current-pulsing method won't work.  So fall back to standard CC/CV charging
  if (current <= config.getSHUTOFF_C() || charger->get_meas_amps() < config.getSHUTOFF_C())
  {
    // Current has trickled off
    state = ChargeState::BALANCE;

    ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                       << " has finished charging, trickle-charging until balance complete");

    charger->setVC(terminalVmax, current);
    return;
  }

  ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                     << ": terminalVmax: " << terminalVmax << " cableR: " << config.getCABLE_R()
                                     << " current: " << current << " MC: " << charger->get_meas_amps());
  if (charger->get_meas_volts() > terminalVmax + (current - config.getINCR_C()) * config.getCABLE_R())
  {
    // Next step is either another step down, or the nearest reasonable step
    current -= config.getINCR_C();
    voltage = terminalVmax + current * config.getCABLE_R();
    ROS_INFO_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                       << " decrementing current to " << current << "A, V=" << voltage << "V");
    charger->setVC(voltage, current);
  }

  check_balancer();
}

void ChargeSequence::tick_balance()
{
  ROS_ERROR_STREAM("TICK BALANCE");

  // check balancer-- if its done (TRUE), then stop the charge
  if (check_balancer())
  {
    stopCharge();
    ROS_DEBUG_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                        << " balancing reports finished, ending charge normally!");

    ROS_DEBUG_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id() << " done, setting FULL!");
    battery->setFull();
  }
}

bool ChargeSequence::check_balancer()
{
  switch (balancerState)
  {
    case BALANCER_NOT_STARTED:
      if (charger->get_meas_amps() < config.getBalanceMaxCurrent())
      {
        battery->setEnableBalance();
        ROS_ERROR_STREAM("BALANCER_STARTED");
        balancerState = BALANCER_STARTED;
        balanceStartTime = ros::WallTime::now();
        balanceEndTime = ros::WallTime(balanceStartTime.toSec() + 3600);
        ROS_DEBUG_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id() << " enabling "
                                                                                                    "BALANCE!");
      }
      break;

    case BALANCER_STARTED:
      // TODO: shut off after an hour
      if (ros::WallTime::now().toSec() > balanceEndTime.toSec() || !battery->get_balance_enable())
      {
        ROS_ERROR_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id() << " BALANCE "
                                                                                                    "complete!");
        ROS_ERROR_STREAM("BALANCER_DONE");
        balancerState = BALANCER_DONE;
        return true;
      }
      if (charger->get_meas_amps() > config.getBalanceMaxCurrent())
      {
        battery->setDisableBalance();
        ROS_ERROR_STREAM("BALANCER_NOT_STARTED");
        balancerState = BALANCER_NOT_STARTED;
        ROS_ERROR_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id()
                                            << " disabling BALANCE due to high current");
      }
      break;

    case BALANCER_DONE:
      // we're done, don't do anything
      return true;

    default:
      ROS_ERROR_STREAM("Charge Sequence " << charger->get_id() << " --> " << battery->get_id() << " Unknown balancer "
                                                                                                  "status");

  }  // balancer state
  return false;
}

bool ChargeSequence::charge_notOk()
{
  // Check if battery is OK
  if (battery->get_errors())
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " is not OK, aborting");
    return true;
  }
  // Check if we heard from the battery recently enough
  if (battery->get_time_since() > batteryCommsTimeout)
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " last comm " << battery->get_time_since()
                                << " seconds ago, aborting charge");
    return true;
  }

  // Check if power supply is OK
  if (charger->get_errors())
  {
    ROS_ERROR_STREAM("Power supply " << charger->get_id() << " not OK, aborting charge");
    return true;
  }

  // Check if we heard from power supply recently enough
  if (charger->get_time_since() > chargerCommsTimeout)
  {
    ROS_ERROR_STREAM("Power supply " << charger->get_id() << " lost comm " << charger->get_time_since()
                                     << " seconds ago, aborting charge");
    return true;
  }
  ROS_DEBUG_STREAM("Charge status check on battery " << battery->get_id() << " OK, continuing charge");

  return false;
}

void ChargeSequence::stopCharge()
{
  ROS_INFO_STREAM("STOP CHARGE");
  battery->setOff();
  charger->setOff();
  state = DONE;
}

void ChargeSequence::startCharge()
{
  ROS_INFO_STREAM("START NEW CHARGE SEQUENCE");
  battery->setCharge();
  charger->setVC(voltage, current);
}
