/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/6/18.
//

#include "../../include/sail_bat/batmanobject.h"

using namespace sail_bat;

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXX BATMANOBJECT                                                                       XXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

BatManObject::BatManObject(std::string _addr, int _id) : addr(_addr), id(_id)
{
  ROS_INFO_STREAM("NEW BATMAN OBJECT ID=" << id);
  //    setupConnections();
  ros::Time::init();
  last_time_arrived = ros::Time::now();
}

BatManObject::~BatManObject() = default;

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXX BAT                                                                                XXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Bat::Bat(std::string _addr, int _id) : BatManObject(_addr, _id), latest(), cmd()
{
  ROS_INFO_STREAM("IT's a battery! " << addr);
}

Bat::~Bat() = default;

void Bat::updateLatest(ds_hotel_msgs::Battery in)
{
  //    ROS_INFO_STREAM("Latest battery just received: "<< in.idnum);
  latest = in;
  last_time_arrived = latest.header.stamp;
}

void Bat::setOn()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " ON");
  cmd.request.command = cmd.request.BAT_CMD_DISCHARGE;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

void Bat::setOff()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " OFF");
  cmd.request.command = cmd.request.BAT_CMD_OFF;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

void Bat::setCharge()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " CHARGE");
  cmd.request.command = cmd.request.BAT_CMD_CHARGE;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

void Bat::setFull()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " TO FULL");
  cmd.request.command = cmd.request.BAT_CMD_RECORD_FULL;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

void Bat::setEnableBalance()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " ENABLE BALANCE");
  cmd.request.command = cmd.request.BAT_CMD_ENABLE_BALANCE;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

void Bat::setDisableBalance()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING BAT " << id << " DISABLE BALANCE");
  cmd.request.command = cmd.request.BAT_CMD_DISABLE_BALANCE;

  if (client.call(cmd))
  {
    ROS_INFO_STREAM("BAT " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("BAT " << id << " FAILURE");
  }
}

bool Bat::get_errors()
{
  if (latest.safetyCode)
  {
    ROS_ERROR_STREAM("BAT " << id << " HAS ERROR");
    return true;
  }
  return false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXX PWR                                                                                XXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Pwr::Pwr(std::string _addr, int _id) : BatManObject(_addr, _id), latest(), cmd()
{
  ROS_INFO_STREAM("IT's a power supply! " << addr);
}

Pwr::~Pwr() = default;

void Pwr::updateLatest(ds_hotel_msgs::PowerSupply in)
{
  //    ROS_INFO_STREAM("Latest power just received" << id);
  latest = in;
  last_time_arrived = latest.header.stamp;
}

void Pwr::setVC(double V, double C)
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING POWER SUPPLY " << id << " TO V=" << V << " C=" << C);
  cmd.request.prog_volts = V;
  cmd.request.prog_amps = C;
  cmd.request.output_enable = true;
  cmd.request.frontpanel_locked = true;
  if (client.call(cmd))
  {
    ROS_INFO_STREAM("PWR " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("PWR " << id << " FAILURE");
  }
}

void Pwr::setOff()
{
  if (!client_set)
    return;
  ROS_INFO_STREAM("SETTING POWER SUPPLY " << id << " OFF");
  cmd.request.prog_volts = 0;
  cmd.request.prog_amps = 0;
  cmd.request.output_enable = false;
  cmd.request.frontpanel_locked = false;
  if (client.call(cmd))
  {
    ROS_INFO_STREAM("PWR " << id << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("PWR " << id << " FAILURE");
  }
}

bool Pwr::get_errors()
{
  if (!latest.status_good)
  {
    return true;
  }
  return false;
}

///////////////////////////////////////////////////////////////////////////////
// ChargeConfig
///////////////////////////////////////////////////////////////////////////////
ChargeConfig::ChargeConfig()
{
  auto prefix = ros::param::param<std::string>("~bat_path", "");

  double cellMax;
  N_CELLS = ros::param::param<int>("~CHARGE/N_CELLS", 14);
  INCR_C = ros::param::param<double>("~CHARGE/CURRENT_INCREMENT", 0.10);
  INITIAL_C = ros::param::param<double>("~CHARGE/INITIAL_C", 0.25);
  RAMP_INCR_C = ros::param::param<double>("~CHARGE/RAMP_INCREMENT", 1.0);
  MAX_C = ros::param::param<double>("~CHARGE/CURRENT_MAX", 8.0);           // TODO: I changed this from 9.0
  SHUTOFF_C = ros::param::param<double>("~CHARGE/SHUTOFF_CURRENT", 0.38);  // TODO: changed from  0.25, change back
  cellMax = ros::param::param<double>("~CHARGE/MAX_CELL_VOLTS", 4.2);
  MAX_V = cellMax * N_CELLS;
  TARGET_CELL_V = ros::param::param<double>("~CHARGE/TARGET_CELL_VOLTS", 4.125);  // TODO: I changed this from 4.125
  CABLE_R = ros::param::param<double>("~CHARGE/CABLE_R_OHMS", 0.4);               // TODO: changed from 0.4
  DIODE_V = ros::param::param<double>("~CHARGE/CABLE_DIODE_V", 0.5);

  batteryTimeout = ros::param::param<double>("~CHARGE/BATTERY_TIMEOUT", 45);
  chargerTimeout = ros::param::param<double>("~CHARGE/CHARGER_TIMEOUT", 45);

  balanceGain = ros::param::param<double>("~CHARGE/BALANCE_GAIN", 18);
  balanceOnsetVoltage =
      ros::param::param<double>("~CHARGE/BALANCE_ONSET_CELL_VOLTS", 4.0);  // TODO: I changed this from 4.0
  balanceMaxCurrent = ros::param::param<double>("~CHARGE/BALANCE_MAX_CURRENT", 0.5);
}
