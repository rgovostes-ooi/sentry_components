/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/23/18.
// Copied/adjusted from ivaugn's DslBattery_Test.cpp
//

#include "../../include/sail_bat/htp.h"

#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class HTPTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(HTPTest, AcceptsGood)
{
  std::string testStr("#HTP2?VLH=6E T=58 P=90\r\n\3");
  sail_bat::HTP this_htp;
  EXPECT_TRUE(this_htp.acceptsMessage(testStr));
}

TEST_F(HTPTest, ParsesGood)
{
  std::string testStr("#HTP2?VLH=6E T=58 P=90\r\n\3");
  sail_bat::HTP this_htp;
  auto parse = this_htp.handleMessage(testStr);
  EXPECT_TRUE(parse);
  // I'm checking against the existing logged strings, which
  // have cruddy resolution
  EXPECT_NEAR(40.0, this_htp.getHumidity(), 0.5);
  EXPECT_NEAR(28.2, this_htp.getTemperature(), 0.05);
  EXPECT_NEAR(5.9, this_htp.getPressure(), 0.05);
}

TEST_F(HTPTest, AcceptsGood_3)
{
  std::string testStr("#HTP2?V H=61 T=57 P=F7 P4=7A P3=7A P1=7A");
  sail_bat::HTP this_htp;
  EXPECT_TRUE(this_htp.acceptsMessage(testStr));
}

TEST_F(HTPTest, ParsesGood_3)
{
  std::string testStr("#HTP2?V H=61 T=57 P=F7 P4=7A P3=7A P1=7A");
  sail_bat::HTP this_htp;
  auto parse = this_htp.handleMessage(testStr);
  EXPECT_TRUE(parse);
  // I'm checking against the existing logged strings, which
  // have cruddy resolution
  EXPECT_NEAR(32.4, this_htp.getHumidity(), 0.5);
  EXPECT_NEAR(28.65, this_htp.getTemperature(), 0.05);
  EXPECT_NEAR(18.9, this_htp.getPressure(), 0.05);
}

TEST_F(HTPTest, AcceptsMultipleChar)
{
  std::string testStr("#HTP2?V H=61 H=61 T=57 P=F7 P4=7A P3=7A P1=7A");
  sail_bat::HTP this_htp;
  EXPECT_TRUE(this_htp.acceptsMessage(testStr));
}

TEST_F(HTPTest, FailsParsingMultiHTP)
{
  std::string testStr("#HTP2?V H=23 H=61 T=57 P=F7 P4=7A P3=7A P1=7A");
  sail_bat::HTP this_htp;
  auto parse = this_htp.handleMessage(testStr);
  EXPECT_FALSE(parse);
  // I'm checking against the existing logged strings, which
  // have cruddy resolution
}

TEST_F(HTPTest, FindCorrect)
{
  std::string testStr("#HTP2?V H=61 T=57 P=F7 P4=7A P3=7A P1=7A #HTP2?V H=42 T=38 P=A8 P4=5B P3=7B P1=8B");
  sail_bat::HTP this_htp;
  auto parse = this_htp.handleMessage(testStr);
  EXPECT_TRUE(parse);
  // I'm checking against the existing logged strings, which
  // have cruddy resolution
  EXPECT_NEAR(32.4, this_htp.getHumidity(), 0.5);
  EXPECT_NEAR(28.65, this_htp.getTemperature(), 0.05);
  EXPECT_NEAR(18.9, this_htp.getPressure(), 0.05);
}

TEST_F(HTPTest, Cutoff)
{
  std::string testStr("#HTP2?V H=61 T=57 P=#HTP2?V H=42 T=38 P=A8 P4=5B P3=7B P1=8B");
  sail_bat::HTP this_htp;
  auto parse = this_htp.handleMessage(testStr);
  EXPECT_TRUE(parse);
  // I'm checking against the existing logged strings, which
  // have cruddy resolution
  EXPECT_NEAR(13.2, this_htp.getHumidity(), 0.5);
  EXPECT_NEAR(43.7, this_htp.getTemperature(), 0.05);
  EXPECT_NEAR(8.9, this_htp.getPressure(), 0.05);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
