/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/24/18.
// Copied/adjusted from ivaugn's DslBattery_Test.cpp
//

#include "../../include/sail_bat/battery.h"
#include <ds_util/ds_util.h>
#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class BatTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static bool confirm_checksum_match(std::string test_str, uint16_t expected);
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

///////////////////////////////////////////////////////////////////////////////
// BatteryModule
///////////////////////////////////////////////////////////////////////////////

TEST_F(BatTest, AcceptsGood_2)
{
  sail_bat::Battery this_bat;
  std::string testStr;
  testStr = ("!B2 B2 3214 3082 2978 3018 2990 3072 3016 2982 3046 3032 3075 3021 2995 2996 44840 0018 0018 0018 0018 "
             "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 "
             "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 "
             "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0010CE05 00132C85 FFF2DDAE 00 2 14 64956 0 1 1 "
             "4806027 00 0  21 \n\r\03");
  EXPECT_TRUE(this_bat.acceptsMessage(testStr));
  testStr = ("!B2 B2 3654 3654 3649 3649 3649 3649 3649 3649 3649 3649 3649 3649 3649 3649 50660 0017 0016 0017 0017 "
             "0017 0017 0017 0016 0016 0016 0016 0015 0017 0016 0016 0015 0016 0015 0017 0017 0016 0015 0016 0015 0017 "
             "0016 0017 0017 0017 0016 0016 0015 0016 0016 0015 0015 0017 0017 0015 0015 0017 0016 0016 0015 0016 0015 "
             "0017 0017 0016 0016 0016 0016 0017 0017 0017 0017 0019 0022A1F0 00204EE2 00141405 00 0 11 18806 0 109 13 "
             "22980252 00 0  5C \n\r\03");
  EXPECT_TRUE(this_bat.acceptsMessage(testStr));
}

// TEST_F(BatTest, AcceptsValidOldStyle) {
//    sail_bat::Battery this_bat;
//    ASSERT_TRUE(this_bat.handleMessage("B2 4073 4073 4073 4073 4073 4073 4073 4073 4073 4073 4073 4073 4073 4073 57020
//    0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028
//    0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028
//    0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0028 0021D1FE 001FA03D 00209A29 10 0 01 10516 0 0 0 14990627 00
//    0  6E \n\r\03"));
//}

TEST_F(BatTest, RejectsBadChecksum)
{
  sail_bat::Battery this_bat;
  ASSERT_FALSE(this_bat.handleMessage("!B2 B2 3214 3082 2978 3018 2990 3072 3016 2982 3046 3032 3075 3021 2995 2996 "
                                      "44840 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 "
                                      "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 "
                                      "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 "
                                      "0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0018 0010CE05 00132C85 "
                                      "FFF2DDAE 00 2 14 64956 0 1 1 4806027 00 0  25 \n\r\03"));
}

TEST_F(BatTest, ParsesCounts)
{
  sail_bat::Battery this_bat("B1");
  ASSERT_TRUE(this_bat.handleMessage("#B1 L 22\r!B1 B1 3864 3864 3869 3864 3869 3869 3869 3864 3869 3864 3869 3864 "
                                     "3864 3864 54220 0009 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 0008 "
                                     "0008 0010 0010 0010 0010 0008 0008 0009 0009 0010 0010 0009 0009 0011 0011 0008 "
                                     "0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0011 0009 0009 0010 "
                                     "0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 FFFFBC2E 002007B7 "
                                     "FFF80905 10 0 01 21077 0 114 1 30706851 00 0  69\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat.get_cell_v(), 3.864);
  EXPECT_EQ(this_bat.get_cell_t(), 9);
  EXPECT_FLOAT_EQ(this_bat.get_v(), 54.220);
  EXPECT_EQ(this_bat.get_t(), 11);
  // EXPECT_EQ(this_bat.get_countCapacity(), 2072637);
  // EXPECT_EQ(this_bat.get_countUsed(), 79829);
  // EXPECT_EQ(this_bat.get_countAvailable(), 1992808);
}

bool BatTest::confirm_checksum_match(std::string test_str, uint16_t expected)
{
  std::string expected_str = ds_util::int_to_hex<uint16_t>(expected);
  std::string result_str = sail_bat::Battery::with_checksum_text(test_str);
  int result_int = (int)strtol(result_str.substr(result_str.size() - 3).c_str(), NULL, 16);
  ROS_ERROR_STREAM("Expected: " << expected << " Got: " << result_int);
  return (expected == result_int);
  //  EXPECT_EQ((int)strtol(expected_str.c_str(), NULL, 16), (int)strtol(result.substr(result.size()-3).c_str(), NULL,
  //  16));
}

TEST_F(BatTest, CheckBadParseB1)
{
  // What I know: The checksum was taken over too long of a range. Therefoe it failed. Why did it fail this time as
  // opposed to other times?
  ROS_INFO_STREAM("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  ROS_INFO_STREAM("  Check Bad Parse B1  ");
  ROS_INFO_STREAM("-----------------------------------------------------------------------------");
  sail_bat::Battery this_bat("B1");
  // this one works
  EXPECT_TRUE(this_bat.handleMessage("!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                     "57980 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 FFFF726F 10 "
                                     "0 01 28456 0 114 1 31007146 00 0  DD\n\r\x03"));

  // this is also good
  sail_bat::Battery this_bat_1("B1");
  EXPECT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                       "4125 4125 57980 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "FFFF9FF0 002007B7 FFFF70F0 10 0 01 28528 0 114 1 31007423 00 0  D3\n\r\x03"));

  // TODO: used to fail. Now can pass. Why did it fail?
  EXPECT_TRUE(this_bat.handleMessage("#B1 L 22\r!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                     "4125 4125 57980 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 "
                                     "FFFF726F 10 0 01 28456 0 114 1 31007146 00 0  DD\n\r\x03"));
  EXPECT_TRUE(this_bat.handleMessage("#B6 L 27\r!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                     "4125 4125 57980 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                     "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 "
                                     "FFFF726F 10 0 01 28456 0 114 1 31007146 00 0  DD\n\r\x03"));

  // fails because incorrect range
  EXPECT_FALSE(this_bat.handleMessage(
      "B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 57980 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 FFFF726F 10 0 01 28456 0 114 1 31007146 00 0  DD\n\r\x03"));

  // this one works
  EXPECT_TRUE(BatTest::confirm_checksum_match(
      "!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 57980 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 FFFF726F 10 0 01 28456 0 114 1 31007146 00 0  ",
      0xDD));
  // this does not
  EXPECT_FALSE(BatTest::confirm_checksum_match(
      "B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 57980 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 FFFF9FF0 002007B7 FFFF726F 10 0 01 28456 0 114 1 31007146 00 0  ",
      0xDD));
}

TEST_F(BatTest, KnownGoodParse_allBs)
{
  ROS_INFO_STREAM("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  ROS_INFO_STREAM("  Check all Bs  ");
  ROS_INFO_STREAM("-----------------------------------------------------------------------------");

  sail_bat::Battery this_bat_1("B1");
  EXPECT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                       "4125 4125 57980 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "FFFF9FF0 002007B7 FFFF70F0 10 0 01 28528 0 114 1 31007423 00 0  D3\n\r\x03"));

  sail_bat::Battery this_bat_2("B2");
  EXPECT_TRUE(this_bat_2.handleMessage("#B2 L 23\r!B2 B2 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                       "4125 4125 57840 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "00217137 00200A3D 00214825 10 0 11 19914 0 0 0 31031059 00 0  9C\n\r\x03"));

  sail_bat::Battery this_bat_3("B3");
  EXPECT_TRUE(this_bat_3.handleMessage("#B3 L 24\r!B3 B3 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4132 4125 "
                                       "4125 4125 58050 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "001CA06B 002032EF 001C7CC6 10 0 01 4472 0 75 13 31237424 00 0  33\n\r\x03"));

  sail_bat::Battery this_bat_4("B4");
  EXPECT_TRUE(this_bat_4.handleMessage("#B4 L 25\r!B4 B4 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 "
                                       "4125 4125 57710 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
                                       "002C1F89 001FBF16 002BF3DF 10 0 01 54933 0 0 0 1923289 00 0  FC\n\r\x03"));

  sail_bat::Battery this_bat_5("B5");
  EXPECT_TRUE(this_bat_5.handleMessage(
      "#B6 L 27\r#B5 L 26\r!B5 B5 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 4125 57500 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 "
      "0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 0023 001BECA2 002043FA 001BBE2B 10 0 14 39998 1 0 0 "
      "432368 00 0  C8\n\r\x03"));
}

TEST_F(BatTest, CommandWithChecksum)
{
  sail_bat::Battery this_bat;

  std::string test_str = "#B3 L ";
  std::string expected = ds_util::int_to_hex<uint16_t>(0x24);
  std::string result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("kitchen sink: " << result << " ends with: " << expected);

  test_str = "#B3 B 00000000000000 ";
  expected = ds_util::int_to_hex<uint16_t>(0xDA);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("zero shunts: " << result << " ends with: " << expected);

  test_str = "#B3 ? ";
  expected = ds_util::int_to_hex<uint16_t>(0x17);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("help menu: " << result << " ends with: " << expected);

  test_str = "#B3 X ";
  expected = ds_util::int_to_hex<uint16_t>(0x30);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("turn off: \n" << result << " ends with: " << expected);

  test_str = "#B3 D ";
  expected = ds_util::int_to_hex<uint16_t>(0x1C);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("discharge: \n" << result << " ends with: " << expected);

  test_str = "#B3 C ";
  expected = ds_util::int_to_hex<uint16_t>(0x1B);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("charge: \n" << result << " ends with: " << expected);

  test_str = "#B3 N ";
  expected = ds_util::int_to_hex<uint16_t>(0x26);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("charge count: \n" << result << " ends with: " << expected);

  test_str = "#B3 n ";
  expected = ds_util::int_to_hex<uint16_t>(0x46);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("capacity and charge state: \n" << result << " ends with: " << expected);

  test_str = "#B3 V ";
  expected = ds_util::int_to_hex<uint16_t>(0x2E);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("cell voltage: \n" << result << " ends with: " << expected);

  test_str = "#B3 v ";
  expected = ds_util::int_to_hex<uint16_t>(0x4E);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("string voltage: \n" << result << " ends with: " << expected);

  test_str = "#B3 T ";
  expected = ds_util::int_to_hex<uint16_t>(0x2C);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("temperatures: \n" << result << " ends with: " << expected);

  test_str = "#B3 S ";
  expected = ds_util::int_to_hex<uint16_t>(0x2B);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("status string: \n" << result << " ends with: " << expected);

  test_str = "#B3 B ";
  expected = ds_util::int_to_hex<uint16_t>(0x1A);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("read balances: \n" << result << " ends with: " << expected);

  test_str = "#B3 P ";
  expected = ds_util::int_to_hex<uint16_t>(0x28);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("explain problem: \n" << result << " ends with: " << expected);

  test_str = "#B3 r ";
  expected = ds_util::int_to_hex<uint16_t>(0x4A);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("reset flags: \n" << result << " ends with: " << expected);

  test_str = "#B3 p ";
  expected = ds_util::int_to_hex<uint16_t>(0x48);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("view param: \n" << result << " ends with: " << expected);

  test_str = "#B3 s ";
  expected = ds_util::int_to_hex<uint16_t>(0x4B);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("set param: \n" << result << " ends with: " << expected);

  test_str = "#B3 R ";
  expected = ds_util::int_to_hex<uint16_t>(0x2A);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("reboot: \n" << result << " ends with: " << expected);

  test_str = "#B3 e ";
  expected = ds_util::int_to_hex<uint16_t>(0x3D);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("firmware version: \n" << result << " ends with: " << expected);

  test_str = "#B3 u ";
  expected = ds_util::int_to_hex<uint16_t>(0x4D);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("uptime: \n" << result << " ends with: " << expected);

  test_str = "#B3 a ";
  expected = ds_util::int_to_hex<uint16_t>(0x39);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("passthrough mode: \n" << result << " ends with: " << expected);

  test_str = "#B3 F ";
  expected = ds_util::int_to_hex<uint16_t>(0x1E);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("record full: \n" << result << " ends with: " << expected);

  test_str = "#B3 E ";
  expected = ds_util::int_to_hex<uint16_t>(0x1D);
  result = this_bat.with_checksum_text(test_str);
  EXPECT_EQ((int)strtol(expected.c_str(), NULL, 16), (int)strtol(result.substr(result.size() - 3).c_str(), NULL, 16));
  // ROS_ERROR_STREAM("record empty: \n" << result << " ends with: " << expected);
}

TEST_F(BatTest, B1_report_zero)
{
  // Dive 470, Bat 1
  sail_bat::Battery this_bat_1("B1");
  ASSERT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3864 3864 3869 3864 3869 3869 3869 3864 3869 3864 3869 3864 "
                                       "3864 3864 54220 0009 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 "
                                       "0008 0008 0010 0010 0010 0010 0008 0008 0009 0009 0010 0010 0009 0009 0011 "
                                       "0011 0008 0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0011 "
                                       "0009 0009 0010 0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 "
                                       "FFFFBC2E 002007B7 FFF80905 10 0 01 21077 0 114 1 30706851 00 0  69\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_1.get_cell_v(), 3.864);
  EXPECT_EQ(this_bat_1.get_cell_t(), 9);

  ASSERT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3858 3864 3864 3864 3858 3864 3864 3852 3864 3858 3864 3858 "
                                       "3864 3864 54150 0008 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 "
                                       "0008 0008 0010 0010 0010 0010 0008 0008 0009 0009 0010 0010 0009 0009 0011 "
                                       "0011 0008 0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0011 "
                                       "0009 0009 0010 0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 "
                                       "FFFFBC2E 002007B7 FFF7F330 10 0 01 21161 0 114 1 30707157 00 0  5C\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_1.get_cell_v(), 3.858);
  EXPECT_EQ(this_bat_1.get_cell_t(), 8);

  ASSERT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3858 3858 3858 3864 3864 3864 3858 3864 3864 3864 3864 3852 "
                                       "3858 3852 54220 0008 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 "
                                       "0008 0008 0010 0010 0010 0010 0008 0008 0009 0009 0010 0010 0009 0009 0011 "
                                       "0011 0008 0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0011 "
                                       "0009 0009 0010 0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 "
                                       "FFFFBC2E 002007B7 FFF7F231 10 0 01 21165 0 114 1 30707171 00 0  5A\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_1.get_cell_v(), 3.858);
  EXPECT_EQ(this_bat_1.get_cell_t(), 8);

  ASSERT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3858 3864 3864 3864 3864 3864 3864 3864 3858 3864 3864 3864 "
                                       "3858 3864 54150 0008 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 "
                                       "0008 0008 0010 0010 0010 0010 0008 0008 0009 0009 0010 0010 0009 0009 0010 "
                                       "0011 0008 0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0010 "
                                       "0009 0009 0010 0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 "
                                       "FFFFBC2E 002007B7 FFF7ED57 10 0 01 21184 0 114 1 30707240 00 0  71\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_1.get_cell_v(), 3.858);
  EXPECT_EQ(this_bat_1.get_cell_t(), 8);

  ASSERT_TRUE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3858 3858 3858 3858 3864 3864 3864 3858 3864 3858 3864 3852 "
                                       "3858 3852 54080 0008 0009 0008 0008 0008 0008 0008 0008 0010 0010 0009 0009 "
                                       "0008 0008 0010 0010 0010 0010 0008 0008 0009 0008 0010 0010 0009 0009 0010 "
                                       "0011 0008 0008 0010 0010 0010 0010 0008 0008 0011 0011 0009 0009 0010 0010 "
                                       "0009 0009 0010 0010 0008 0008 0008 0008 0008 0008 0009 0009 0010 0010 0011 "
                                       "FFFFBC2E 002007B7 FFF7E088 10 0 01 21234 0 114 1 30707420 00 0  64\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_1.get_cell_v(), 3.858);
  EXPECT_EQ(this_bat_1.get_cell_t(), 8);

  // Dive 469, Bat 2
  sail_bat::Battery this_bat_2("B2");
  ASSERT_TRUE(this_bat_2.handleMessage("#B2 L 23\r!B2 B2 3840 3840 3840 3835 3840 3835 3840 3840 3846 3840 3840 3840 "
                                       "3840 3840 53740 0011 0011 0011 0011 0013 0013 0011 0011 0012 0012 0014 0014 "
                                       "0011 0012 0014 0014 0014 0014 0011 0011 0013 0013 0014 0014 0015 0015 0012 "
                                       "0012 0012 0011 0012 0012 0015 0015 0014 0014 0015 0015 0013 0013 0013 0013 "
                                       "0014 0015 0012 0012 0014 0014 0013 0014 0012 0012 0013 0013 0012 0012 0014 "
                                       "00219622 00200A3D 0018C48D 10 0 11 58281 0 0 0 30653017 00 0  BB\n\r\x03"));
  EXPECT_FLOAT_EQ(this_bat_2.get_cell_v(), 3.840);
  EXPECT_EQ(this_bat_2.get_cell_t(), 11);

  // sail_bat::Battery this_bat_("B");
  // ASSERT_FALSE(this_bat_.handleMessage("Blah"));
  // EXPECT_FLOAT_EQ(this_bat_.get_cell_v(), 0);
  // EXPECT_EQ(this_bat_.get_cell_t(), 0);
}

TEST_F(BatTest, doesntHandleWrongString)
{
  sail_bat::Battery this_bat_1("B1");

  ASSERT_FALSE(this_bat_1.handleMessage(
      "#B6 L 27\r#B4 L 25\r!B4 B4 3852 3858 3852 3858 3852 3858 3858 3858 3852 3852 3858 3858 3858 3852 54010 0008 "
      "0008 0008 0009 0009 0010 0008 0008 0009 0009 0010 0011 0010 0011 0008 0008 0008 0008 0010 0010 0011 0011 0011 "
      "0011 0010 0010 0009 0009 0008 0008 0009 0009 0011 0011 0010 0011 0011 0011 0010 0010 0010 0010 0011 0011 0011 "
      "0011 0009 0009 0010 0010 0009 0009 0010 0010 0009 0010 0011 002BF5B1 001FBF16 00240941 10 0 01 47927 0 0 0 "
      "1623699 00 0  20\n\r\x03"));
  ASSERT_FALSE(this_bat_1.handleMessage("#B1 L 22\r!B1 B1 3858 3858 3858 3858 3864 3864 3864 24 \n\r\03"));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
