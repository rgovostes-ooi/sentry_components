Author: J Vaccaro
Updated: 2/27/18

BatMan : DsProcess

** include/sail_bat/batman.h
** src/sail_bat/batman.cpp

As the name suggests, this process manages battery operations. There are several objects that this node
instantiates, and several actions that can be taken.

OBJECTS
Instantiated when node spins up:
Bat: subscribes to and tracks latest bat information
Chg: subscribes to and maintains latest chg information from a single charger
Pwr: holds location and latest information from shore power

Instantiated during run:
MultiChargeSequence: pairs bats and chgs. Even if hooked up incorrectly, this stage should catch any mismatch.
ChargeSequence: For a single Bat/Chg pair, tracks the state of the charge.
    - ticks (updates) after a set period of time.
    - Adjusts the charger V/C limits in response to the phase of the charge
    - prompts the Bat to rebalance its cells.

ACTIONS
Start Charge:
Creates a MultiChargeSequence object, which creates a new ChargeSequence for every Bat/Chg pair it finds.
Manages the ticking at regular intervals.

Stop Charge:
Sets each Battery to off, destroys any MultiChargeSequence objects.

Shore On, Shore Off:
Sends the appropriate command to Shore Power Supply.

Launch prefers
    - ds_bus
    - battery
    - ds_lambda_ps

Service servers
    - ChargeCmd.srv : Charge start/stop action command
      ~nodename/chg_cmd
    uint8 CHARGE_CMD_OFF=1
    uint8 CHARGE_CMD_CHARGE=2
    - PowerCmd.srv : Power on/off action command (shore on/off)
      ~nodename/pwr_cmd
    uint8 POWER_CMD_OFF=1
    uint8 POWER_CMD_ON=2
    
Charge Sequence Logic: Every 40 seconds, resend the charge command and evaluate the state for any changes.

  State RAMP
   + command current increase by RAMP_INCR_C
   + if voltage exceeds VLIMIT, then enter CHARGE
  
  State CHARGE
   + if voltage exceeds VLIMIT, then command current decrease by INCR_C
   + trigger check_balance (do not evaluate)
   + if measured or commanded current falls beneath SHUTOFF_C, then enter BALANCE
   
  State BALANCE
   + evaluate check_balance.
   + if true, then end the charge and reset full. Enter DONE.
   
  State DONE
   + Do nothing, wait to be erased by MultiChargeSequence
  
**Fxn check_balance
   *return default is false*
   + if state==BALANCE_NOT_STARTED
     - if current is below BalanceMaxCurrent:
       set Battery Balance Enable on
       set BalanceStartTime and BalancerEndTime
       set state to BALANCER_STARTED
   + if state==BALANCER_STARTED
     - if balanceStartTime is not set:
       set balanceStartTime to the current time
       set balanceEndTime to an hour in the future
     - if now is after balanceEndTime, or battery balance is done:
       set state BALANCER_DONE
       return true
     - if current goes above the BalanceMaxCurrent:
       set state BALANCE_NOT_STARTED
   + if state==BALANCER_DONE
     return true   

Subscriptions
    -

Publishers
    - BatMan.msg
      ~nodename