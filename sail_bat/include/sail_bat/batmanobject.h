/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/6/18.
//

#ifndef PROJECT_BATMANOBJECT_H
#define PROJECT_BATMANOBJECT_H

#include "ds_asio/ds_asio.h"

#include "ds_hotel_msgs/PowerSupply.h"
#include "ds_hotel_msgs/Battery.h"
#include "ds_hotel_msgs/PowerSupplyCommand.h"
#include "sentry_msgs/BatteryCmd.h"

namespace sail_bat
{
class BatManObject
{
public:
  BatManObject(std::string _addr, int _id);

  virtual ~BatManObject();

  virtual int get_time_since()
  {
    return 0;
  }

  virtual bool get_errors()
  {
    return true;
  }

  virtual int get_id()
  {
    return id;
  }

  virtual std::string get_addr()
  {
    return addr;
  }

  virtual void set_client(ros::ServiceClient _client)
  {
    client = _client;
    client_set = true;
  }

protected:
  bool client_set = false;
  int id;
  std::string addr;
  ros::ServiceClient client;
  ros::Time last_time_arrived;
};

class Bat : public BatManObject
{
public:
  Bat(std::string _addr, int _id);

  ~Bat() override;

  void setOn();

  void setOff();

  void setCharge();

  void setFull();

  void setEnableBalance();

  void setDisableBalance();

  void set_client(ros::ServiceClient _client) override
  {
    client = _client;
    client_set = true;
  }

  bool get_errors() override;

  int get_time_since() override
  {
    return 0;
  }  // ros::Time::now().sec - last_time_arrived.sec; }

  bool get_balance_enable()
  {
    return latest.balanceEnable;
  }

  bool get_is_charging()
  {
    return latest.charging;
  }

  int get_id() override
  {
    return id;
  }

  ds_hotel_msgs::Battery get_latest()
  {
    return latest;
  }

  std::string get_addr() override
  {
    return addr;
  }

  void updateLatest(ds_hotel_msgs::Battery in);

private:
  ds_hotel_msgs::Battery latest;
  sentry_msgs::BatteryCmd cmd;
};

class Pwr : public BatManObject
{
public:
  Pwr(std::string _addr, int _id);

  ~Pwr() override;

  void setVC(double V, double C);

  void setOff();

  void set_client(ros::ServiceClient _client) override
  {
    client = _client;
    client_set = true;
  }

  bool get_errors() override;

  int get_id() override
  {
    return id;
  }

  virtual std::string get_addr() override
  {
    return addr;
  }

  int get_time_since() override
  {
    return 0;
  }  // ros::Time::now().sec - last_time_arrived.sec; }

  bool get_is_const_voltage()
  {
    return latest.constant_voltage;
  }

  bool get_is_const_current()
  {
    return latest.constant_current;
  }

  float get_meas_volts()
  {
    return latest.meas_volts;
  }

  float get_meas_amps()
  {
    return latest.meas_amps;
  }

  ds_hotel_msgs::PowerSupply get_latest()
  {
    return latest;
  }

  void updateLatest(ds_hotel_msgs::PowerSupply in);

private:
  ds_hotel_msgs::PowerSupply latest;
  ds_hotel_msgs::PowerSupplyCommand cmd;
};

///////////////////////////////////////////////////////////////////////////////
//    Charge config
class ChargeConfig
{
public:
  ChargeConfig();

  int getN_CELLS() const
  {
    return N_CELLS;
  }
  double getINCR_C() const
  {
    return INCR_C;
  }
  double getINITIAL_C() const
  {
    return INITIAL_C;
  }
  double getRAMP_INCR_C() const
  {
    return RAMP_INCR_C;
  }
  double getMAX_C() const
  {
    return MAX_C;
  }
  double getMAX_V() const
  {
    return MAX_V;
  }
  double getTARGET_CELL_V() const
  {
    return TARGET_CELL_V;
  }
  double getTARGET_V() const
  {
    return TARGET_CELL_V * N_CELLS + DIODE_V;
  }
  double getCABLE_R() const
  {
    return CABLE_R;
  }
  double getDIODE_V() const
  {
    return DIODE_V;
  }
  double getSHUTOFF_C() const
  {
    return SHUTOFF_C;
  }

  // comms timeout params
  double getBatteryTimeout() const
  {
    return batteryTimeout;
  };
  double getChargerTimeout() const
  {
    return chargerTimeout;
  };

  // balancing
  double getBalanceGain() const
  {
    return balanceGain;
  };
  double getBalanceOnsetVoltage() const
  {
    return balanceOnsetVoltage;
  };
  double getBalanceMaxCurrent() const
  {
    return balanceMaxCurrent;
  };

protected:
  // number of cells
  int N_CELLS;

  // current increment to use when controlling charging (Amps)
  double INCR_C;

  // current initial step size
  double INITIAL_C;

  // current increment to use when ramping up (Amps)
  double RAMP_INCR_C;

  // max current for system (Amps)
  double MAX_C;

  // max voltage for system (Volts)
  double MAX_V;

  // Charge target voltage (Volts)
  double TARGET_CELL_V;

  // Cable resistance (Ohms)
  double CABLE_R;

  // Diode drop in charge circuit
  double DIODE_V;

  // Charge shutoff current (Amps)
  double SHUTOFF_C;

  // Timeout for charger and battery--
  // after this number of seconds, an error is
  // flagged and the charge is aborted
  double batteryTimeout;
  double chargerTimeout;

  // balancer config
  double balanceGain;
  double balanceOnsetVoltage;
  double balanceMaxCurrent;
};

enum ChargeState
{
  DONE,
  RAMP,
  CHARGE,
  BALANCE
};

enum BalancerState
{
  BALANCER_DONE,
  BALANCER_NOT_STARTED,
  BALANCER_STARTED
};

}  // NAMESPACE

#endif  // PROJECT_BATMANOBJECT_H
