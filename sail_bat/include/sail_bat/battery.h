/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/24/18.
//

#ifndef PROJECT_BATTERY_H
#define PROJECT_BATTERY_H

#include "ds_base/ds_bus_device.h"
#include "ds_hotel_msgs/Battery.h"
#include "sentry_msgs/BatteryCmd.h"
#include "ds_core_msgs/IoSMcommand.h"
#include <sensor_msgs/BatteryState.h>
#include <string>
#include <memory>
#include <ds_base/util.h>
#include <ds_util/ds_util.h>

namespace sail_bat
{
/// \brief battery configuration values
typedef struct BatteryConfig
{
  // Set sensible defaults
  float countToCoulomb = 0.125;
  float cellUnderVoltage = 3.2;
  float cellShutoffBalance = 3.3;
  float reportTimeout = 300.0;
  int nominalBusVoltage = 50;
  int balanceToleranceMV = 50;
} BatteryConfig_t;

class Battery : public ds_base::DsBusDevice
{
public:
  explicit Battery();
  Battery(int argc, char* argv[], const std::string& name);
  // Only used for testing known bad parsing cases
  Battery(std::string address);
  ~Battery() override;
  DS_DISABLE_COPY(Battery)

protected:
  /// \function overrides from DsBusDevice
  void setupParameters() override;
  void setupIoSM() override;
  void setupConnections() override;
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;

private:
  /// \brief Loaded from param server
  std::string address_;

  /// \brief a cache of regex's
  boost::regex accept_regex;
  boost::regex start_regex;
  boost::regex end_regex;

  /// \brief function for updating regex values if address changes
  void regex_set();

  /// \brief published messages.
  ///
  /// The switching on Battery/State is intentional but frustrating,
  /// given that "state" is the full message and we can't change
  /// the preset sensor_msgs name, this is how it is.
  ds_hotel_msgs::Battery state;
  sensor_msgs::BatteryState bat;

  /// \brief to these publishers
  ros::Publisher bat_state_pub_;
  ros::Publisher bat_pub_;

  ros::ServiceServer bat_ctrl_cmd_;

  /// \brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  /// \brief struct with some important settings, same accross all batteries
  BatteryConfig_t config;
  /// \brief present command table
  const static std::map<std::string, std::string> commandTable;
  const static std::vector<std::string> SAFETY_CODES;

  /// \brief interprets error messages from incoming parsed status
  void updateStatus();

  /// \brief interprets balancing needs, generates and sends battery shunt command
  void updateBalance();

  /// \brief generates and sends "zeroing" battery shunt command
  void zeroShunts();

  /// \brief interprets, generates and sends rosservice commands
  bool _bat_cmd(const sentry_msgs::BatteryCmd::Request& req, const sentry_msgs::BatteryCmd::Response& resp);

  /// \brief utility for sending a given command to this battery
  void _send_preempt(const std::string& cmdstr);

  void _send_wakeup_cmd();

public:
  static std::string with_checksum_text(std::string cmd);
  /// \brief parsing functions are public.
  bool acceptsMessage(const std::string& msg);
  bool handleMessage(const std::string& msg);
  float get_cell_v()
  {
    return state.voltages[0];
  }
  int get_cell_t()
  {
    return state.temperatures[0];
  }
  float get_v()
  {
    return state.totalVoltage;
  }
  int get_t()
  {
    return state.switchTemp;
  }
  uint32_t get_countCapacity()
  {
    return state.countCapacity;
  }
  uint32_t get_countUsed()
  {
    return state.countUsed;
  }
  uint32_t get_countAvailable()
  {
    return state.countAvailable;
  }
};

/// \brief Battery Error states bitmask
typedef enum BatteryErrorStates {
  BATT_NOT_CONNECTED = 0x0001,           /* 000...000000001 */
  BATT_OFFLINE = 0x0002,                 /* 000...000000010 */
  BATT_OVERTEMP = 0x0004,                /* 000...000000100 */
  BATT_UNDERVOLTAGE = 0x0008,            /* 000...000001000 */
  BATT_OVERVOLTAGE = 0x0010,             /* 000...000010000 */
  BATT_SAFETY_PROBLEM_CODE_SET = 0x0020, /* 000...000100000 */
  BATT_DISCHARGE_SWITCH_OFF = 0x0040,    /* 000...001000000 */
  BATT_CHARGE_SWITCH_ON = 0x0100,        /* 000...010000000 */
  BATT_BALANCING = 0x0200,               /* 000...100000000 */
} BatteryErrorStates_t;
}

#endif  // PROJECT_BATTERY_H
