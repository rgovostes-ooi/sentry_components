/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/9/18.
//

#ifndef PROJECT_BALANCER_H
#define PROJECT_BALANCER_H

#include "ds_base/ds_process.h"
#include <ds_hotel_msgs/Battery.h>
#include <sentry_msgs/BatteryCmd.h>
#include <sentry_msgs/PowerCmd.h>

#include <string>
#include <memory>

namespace sail_bat
{
class Balancer : public ds_base::DsProcess
{
public:
  explicit Balancer();
  Balancer(int argc, char* argv[], const std::string& name);
  ~Balancer() override;

  DS_DISABLE_COPY(Balancer)

protected:
  void setupParameters() override;
  void setupConnections() override;
  void sub_callback(ds_hotel_msgs::Battery msg);
  bool cmd_callback(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp);
  void timer_callback(const ros::TimerEvent&);
  void balance_on();
  void balance_off();

private:
  std::string bat_path;
  bool is_balancing;
  ros::Subscriber bat_sub;
  ros::ServiceClient bat_client;
  ros::ServiceServer balance_server;
  ros::Timer timer;
};
}

#endif  // PROJECT_BALANCER_H
