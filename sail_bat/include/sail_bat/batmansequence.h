/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/6/18.
//

#ifndef PROJECT_BATMANSEQUENCE_H
#define PROJECT_BATMANSEQUENCE_H

#include <ros/time.h>
#include "batmanobject.h"

///////////////////////////////////////////////////////////////////////////////
// A charge sequence is responsible for a single battery / power
// supply pair and manages the charge sequence.
namespace sail_bat
{
class ChargeSequence
{
public:
  ChargeSequence(const ChargeConfig& _c, Pwr* _charger, Bat* _battery);
  virtual ~ChargeSequence();

  void tick();
  void stopCharge();
  void startCharge();

  bool isDone() const
  {
    return state == DONE;
  };

protected:
  ChargeConfig config;
  Bat* battery;
  Pwr* charger;

  ros::WallTime balanceStartTime, balanceEndTime;

  double batteryCommsTimeout;
  double chargerCommsTimeout;

  double current;
  double voltage;
  double terminalVmax;
  enum ChargeState state;
  enum BalancerState balancerState;

  bool charge_notOk();
  void tick_ramp();
  void tick_charge();
  void tick_balance();

  // update the balancer status-- possibly enable it, etc
  bool check_balancer();
};

///////////////////////////////////////////////////////////////////////////////
// A MultichargeSequence manages multiple charge sequences.
// In particular, it helps ensure the chargers start up one at a time
// and that each given charger is correctly associated with its
// battery module
class MultichargeSequence
{
public:
  MultichargeSequence(std::vector<Bat*> _batteries, std::vector<Pwr*> _chargers, ros::Timer _timer);
  virtual ~MultichargeSequence();

  void startCharge();
  void stopCharge();
  void tick();

protected:
  int num_bats;
  //    Bat* battery;
  std::vector<Bat*> batteries;
  std::vector<Pwr*> chargers;
  ros::Timer timer;
  ChargeConfig config;
  std::list<ChargeSequence*> chargeSeqs;
  int batteryId;
  int chargerId;
  bool starting;

  void startupTick();
};
}

#endif  // PROJECT_BATMANSEQUENCE_H
